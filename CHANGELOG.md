# VeriPB Changelog

This changelog contains the most important changes to VeriPB from version to version. Due to being a prototype implementation and the active development of the proof system, there will be breaking changes. If there are breaking changes, they will be mentioned at the top for of each version.

## Version 2.3.0
Commit hash: ***not released***

### New
- Added optional constraint labels to refer to constraints in the proof instead of constraint IDs. Labels have to start with the `@` character and can be assigned to any constraint in the input OPB file or the proof.
- Added printing constraint label definition to trace.

### Changes
- Changed autoproving trace to mention that the database has been substituted for the implication check.
- Removed checked deletion flag check for checking the upper bound at the end of the proof.

### Fixes
- Fixed annotated RUP for using relative constraint IDs as hints.
- Fixed using relative constraint IDs for `deld`.
- Fixed bug not deleting duplicate constraints when using `deld`.


## Version 2.2.2 (PB competition 2024)
Commit hash: 765fa91e60f120d51c079a5fa83f664ff69d7d17

### New
- Added user-defined timers for custom statistics on tracking the time requires checking certain proof sections.
- Added elaboration for substituted database implication autoproving check.

### Bug Fixes
- Fixed circular import bug and provide instructions to avoid this bug.
- Fixed elaboration of autoproving by implication.


## Version 2.2.1
Commit hash: 62f78fd47b41ad28fd47df626a14491a43b14cc1

### New
- Added printing conclusion and output verification statements at the end of checking.

### Changes
- Changed to implicitly add negated constraints to annotated RUP hints.
- Removed legacy code setting unassigned variables after propagation to true.
- Improved `f` rule error message.
- Improved error messages for lower bound check.
- Changed to print `Verification failed` error message for more errors.

### Bug Fixes
- Fixed parsing bug when there is no space between semicolon and last witness mapping.
- Fixed erroneous on/off in README.md.
- Fixed elaboration of checked deletion using RUP with duplicate constraints.
- Fixed elaboration of RUP autoproving when using negated constraint.
- Fixed elaboration bug reusing constraint ID of negated constraint.
- Fixed elaboration bug when unloading order while no order is loaded.
- Fixed elaboration of implication to exactly derived the desired constraint.
- Fixed to check solution only for variables still occurring in the database.
- Fixed `conclusion BOUNDS` check when claiming solution and infeasible at the same time.
- Fixed elaboration for checked deletion RUP check.
- Fixed default VeriPB version when compiling outside repository.


## Version 2.2.0 (SAT competition 2024)
Commit hash: aa8fe7380af9e2ec7776b707f708db048e8fb3dd

### Breaking Changes
- The rules `e`, `ea`, `i`, and `ia` have a changed syntax. E.g., for the `e` rule the new syntax is
```
e <constraint in OPB syntax> ; [<constraint ID>]
```

### New
- Annotated RUP: The `rup` rule can now take a list of constraints as hints. The propagation is then only performed on that list. The negated constraint is denoted by `~`. The syntax for this rule is
```
rup <constraint in OPB syntax> ; [<list of IDs of propagated constraint>]
```
import unittest

from pathlib import Path

from veripb import run

from veripb.utils import Settings as MiscSettings
from veripb.verifier import Verifier

import shutil
import subprocess


class TestSelfVerification(unittest.TestCase):
    def run_single(self, formulaPath):
        proofPath = formulaPath.with_suffix(".pbp")
        proofLog = Path("out.pbp")

        if shutil.which("cake_pb"):
            print(
                "veripb %s %s --proofOutput %s" % (
                    formulaPath, proofPath, proofLog),
                "&&",
                "cake_pb %s %s" % (formulaPath, proofLog),
                "&&",
                "veripb %s %s" % (formulaPath, proofLog)
            )

            with open(proofLog, "w+") as outfile:
                miscSettingsLogging = MiscSettings(
                    {"proofFile": outfile})
                miscSettingsChecking = MiscSettings(
                    {"proofFile": None})
                if formulaPath.suffix == ".wcnf":
                    miscSettingsLogging.setPreset({"wcnf": True})
                    miscSettingsChecking.setPreset({"wcnf": True})
                elif formulaPath.suffix == ".cnf":
                    miscSettingsLogging.setPreset({"cnf": True})
                    miscSettingsChecking.setPreset({"cnf": True})

                verifierSettings = Verifier.Settings(
                    {"isCheckDeletionOn": True, "forceCheckDeletion": True})

                with formulaPath.open() as formula:
                    with proofPath.open() as proof:
                        run(formula, proof, verifierSettings=verifierSettings,
                            miscSettings=miscSettingsLogging)
                        outfile.seek(0)
                        if formulaPath.suffix == ".wcnf":
                            if shutil.which("cake_pb_wcnf"):
                                result = subprocess.check_output(
                                    ["cake_pb_wcnf", str(formulaPath), "out.pbp"], stderr=subprocess.STDOUT).decode('ascii')
                            else:
                                raise Exception(
                                    "Could not find cake_pb_wcnf for WCNF test.")
                        else:
                            result = subprocess.check_output(
                                ["cake_pb", str(formulaPath), "out.pbp"], stderr=subprocess.STDOUT).decode('ascii')
                        if result[0] == "c":
                            raise Exception("CakePB🍰 Error: " + str(result))
                        run(formula, outfile, verifierSettings=verifierSettings,
                            miscSettings=miscSettingsChecking)

        else:
            print(
                "veripb %s %s --proofOutput %s" % (
                    formulaPath, proofPath, proofLog),
                "&&",
                "veripb %s %s" % (formulaPath, proofLog)
            )

            with open(proofLog, "w+") as outfile:
                miscSettingsLogging = MiscSettings(
                    {"proofFile": outfile})
                miscSettingsChecking = MiscSettings(
                    {"proofFile": None})
                if formulaPath.suffix == ".wcnf":
                    miscSettingsLogging.setPreset({"wcnf": True})
                    miscSettingsChecking.setPreset({"wcnf": True})
                elif formulaPath.suffix == ".cnf":
                    miscSettingsLogging.setPreset({"cnf": True})
                    miscSettingsChecking.setPreset({"cnf": True})

                verifierSettings = Verifier.Settings(
                    {"isCheckDeletionOn": True, "forceCheckDeletion": True})

                with formulaPath.open() as formula:
                    with proofPath.open() as proof:
                        run(formula, proof, verifierSettings=verifierSettings,
                            miscSettings=miscSettingsLogging)
                        outfile.seek(0)
                        run(formula, outfile, verifierSettings=verifierSettings,
                            miscSettings=miscSettingsChecking)

    def correct_proof(self, formulaPath):
        self.run_single(formulaPath)


def create(formulaPath, helper):
    def fun(self):
        helper(self, formulaPath)
    return fun


def findProblems(globExpression):
    current = Path(__file__).parent
    files = current.glob(globExpression)
    files = [f for f in files if f.suffix in [".cnf", ".opb"] and f.is_file()]
    return files


# correct = findProblems("integration_tests/correct/**/*.*")
correct = list(map(Path, [
    # Basic tests
    "tests/integration_tests/correct/read_formula_1_version2.opb",
    "tests/integration_tests/correct/rup_php65_version2.opb",
    "tests/integration_tests/correct/rup_version2.opb",
    "tests/integration_tests/correct/literal_rule_1_version2.opb",
    "tests/integration_tests/correct/optimization_3_version2.opb",
    "tests/integration_tests/correct/core_deletion_with_move_version2.opb",
    "tests/integration_tests/correct/large_coeffs_version2.opb",
    "tests/integration_tests/correct/saturation_1_version2.opb",
    "tests/integration_tests/correct/double_variable_2_version2.opb",
    "tests/integration_tests/correct/solution_version2.opb",
    "tests/integration_tests/correct/zero_saturation_version2.opb",
    "tests/integration_tests/correct/negated_literal_1_version2.opb",
    "tests/integration_tests/correct/weakening_version2.opb",
    "tests/integration_tests/correct/miniProof_polishnotation_1_with_comments_version2.opb",
    "tests/integration_tests/correct/delete_core_version2.opb",
    "tests/integration_tests/correct/test_deletion_version2.opb",
    "tests/integration_tests/correct/implication_version2.opb",
    "tests/integration_tests/correct/implication_version2_without_id.opb",
    "tests/integration_tests/correct/implication_with_weakening_without_id.opb",
    "tests/integration_tests/correct/implication_with_weakening.opb",
    "tests/integration_tests/correct/implication_version2_weaker.opb",
    "tests/integration_tests/correct/implication_version2_non_saturated.opb",
    "tests/integration_tests/correct/rup_objective_version2.opb",
    "tests/integration_tests/correct/rup_php65_objective_version2.opb",
    "tests/integration_tests/correct/duplicate_constraints.opb",
    "tests/integration_tests/correct/rup_php65_version2_optional_end_id.opb",
    "tests/integration_tests/correct/optimization_propagate_version2.opb",

    # General variable names tests
    "tests/integration_tests/correct/freeNames_version2.opb",
    "tests/integration_tests/correct/all_diff_version2.opb",
    "tests/integration_tests/correct/multiple_contradiction_rups_version2.opb",

    "tests/integration_tests/correct/miniProof_polishnotation_2_version2.opb",
    "tests/integration_tests/correct/set_level_withdraw_version2.opb",
    "tests/integration_tests/correct/double_variable_version2.opb",
    # Axioms are not supported in CakePB
    # "tests/integration_tests/correct/axiom_rule_version2.opb",
    "tests/integration_tests/correct/optimization_version2.opb",
    "tests/integration_tests/correct/read_formula_2_version2.opb",
    "tests/integration_tests/correct/relative_rpn_version2.opb",
    "tests/integration_tests/correct/optimization_2_version2.opb",
    "tests/integration_tests/correct/miniProof_polishnotation_1_version2.opb",
    "tests/integration_tests/correct/division_1_version2.opb",

    # Redundancy tests
    "tests/integration_tests/correct/redundancy/and_opb_version2.opb",
    "tests/integration_tests/correct/redundancy/and_version2.opb",
    "tests/integration_tests/correct/redundancy/prefix_only_version2.opb",
    "tests/integration_tests/correct/redundancy/subproof_version2.opb",
    "tests/integration_tests/correct/redundancy/definition_version2.opb",
    "tests/integration_tests/correct/redundancy/increaseNumVars_version2.opb",
    "tests/integration_tests/correct/redundancy/rupsymetry_version2.opb",
    "tests/integration_tests/correct/redundancy/rupsymetry_objective_version2.opb",
    "tests/integration_tests/correct/redundancy/prefix_only_objective_version2.opb",
    "tests/integration_tests/correct/redundancy/prefix_only_duplicate_version2.opb",

    # Dominance tests
    "tests/integration_tests/correct/dominance/example_version2.opb",
    "tests/integration_tests/correct/dominance/php_5_version2.opb",
    "tests/integration_tests/correct/dominance/hole002_version2.opb",
    "tests/integration_tests/correct/dominance/dbg_version2.opb",
    # The following test case requires advanced selflogging of implication or is broken
    "tests/integration_tests/correct/dominance/autoproof_version2.opb",
    "tests/integration_tests/correct/dominance/optimization_version2.opb",
    "tests/integration_tests/correct/dominance/php_5_no_f_version2.opb",
    "tests/integration_tests/correct/dominance/mini_version2.opb",
    "tests/integration_tests/correct/dominance/hole002_version2_reflexivity.opb",
    "tests/integration_tests/correct/dominance/example_version2_reflexivity.opb",
    "tests/integration_tests/correct/dominance/multiple_constraints_order.opb",

    # Version 2.0
    "tests/integration_tests/correct/version2/decision_sat_none.opb",
    "tests/integration_tests/correct/version2/decision_sat.opb",
    "tests/integration_tests/correct/version2/decision_unsat_none.opb",
    "tests/integration_tests/correct/version2/decision_unsat.opb",
    "tests/integration_tests/correct/version2/optimization_bounds_no_lb.opb",
    "tests/integration_tests/correct/version2/optimization_bounds_no_ub.opb",
    "tests/integration_tests/correct/version2/optimization_bounds.opb",
    "tests/integration_tests/correct/version2/optimization_infeasible.opb",
    "tests/integration_tests/correct/version2/optimization_none.opb",
    "tests/integration_tests/correct/rup_objective_version2_constant_term.opb",
    "tests/integration_tests/correct/rup_objective_version2_no_space_obj_constant_term.opb",
    "tests/integration_tests/correct/rup_objective_version2_no_space_obj.opb",

    # Checked deletion
    "tests/integration_tests/correct/core_deletion_with_move_version2_subproof.opb",
    "tests/integration_tests/correct/core_deletion_with_double_move_version2.opb",
    "tests/integration_tests/correct/multiple_contradiction_rups_version2_range.opb",
    "tests/integration_tests/correct/test_deletion_multiple_core.opb",
    "tests/integration_tests/correct/core_deletion_unsaturated_constraint.opb",
    "tests/integration_tests/correct/core_deletion_weaker_constraint.opb",
    "tests/integration_tests/correct/core_deletion_derived_duplicates.opb",

    # Applied instances
    "tests/integration_tests/correct/g3-g5.opb",
    "tests/integration_tests/correct/g3-g5_without_id.opb",
    "tests/integration_tests/correct/g3-g5_without_id_equal.opb",
    "tests/integration_tests/correct/g4-g5.opb",
    "tests/integration_tests/correct/g4-g5_without_id.opb",
    "tests/integration_tests/correct/g4-g5_without_id_equal.opb",
    "tests/integration_tests/correct/g4-g5_time.opb",
    # "tests/integration_tests/correct/skyscrapers.opb",

    # Objective update
    "tests/integration_tests/correct/optimization_version2_objective_update.opb",
    "tests/integration_tests/correct/optimization_version2_objective_update_diff.opb",
    "tests/integration_tests/correct/optimization_version2_objective_update_diff_check.opb",

    # Strengthening to core
    "tests/integration_tests/correct/redundancy/strengthening_to_core.opb",
    "tests/integration_tests/correct/redundancy/and_opb_version2_to_core.opb",
    "tests/integration_tests/correct/redundancy/definition_version2_to_core.opb",
    "tests/integration_tests/correct/redundancy/strengthening_to_core_proof_by_contradiction.opb",

    # MaxSAT instances
    "tests/integration_tests/correct/test_wcnf_allconstraintscore.wcnf",
    "tests/integration_tests/correct/maxsat_new_format.wcnf",
]))
for file in correct:
    method = create(file, TestSelfVerification.correct_proof)
    method.__name__ = "test_correct_%s" % (file.stem)
    setattr(TestSelfVerification, method.__name__, method)

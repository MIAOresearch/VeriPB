import argparse
import logging

# import pyximport; pyximport.install(
#     language_level=3,
#     build_dir=os.path.join(os.path.dirname(os.path.abspath(__file__)), "__pycache__/pyximport")
# )

from veripb import InvalidProof, ParseError
from veripb.verifier import Verifier, Context
from veripb.drat import DRATParser
from veripb.rules_register import get_registered_rules
from veripb.timed_function import TimedFunction
from veripb.parser import RuleParser, loadFormula
from veripb.exceptions import ParseError
from veripb.optimized.constraints import PropEngine as CppPropEngine
from veripb.optimized.parsing import parseOpb, parseCnf, parseWcnf
from veripb.constraints import CppIneqFactory
from veripb.prooflogging import Proof
from time import perf_counter
import importlib.metadata

from veripb.rules_dominance import stats as dominance_stats

profile = False

if profile:
    import cProfile
    from pyprof2calltree import convert as convert2kcachegrind
    from guppy import hpy


class Settings():
    def __init__(self, preset=None):
        self.setPreset(type(self).defaults(), unchecked=True)

        if preset is not None:
            self.setPreset(preset)

    def setPreset(self, preset, unchecked=False):
        for key, value in preset.items():
            if unchecked or hasattr(self, key):
                setattr(self, key, value)
            else:
                raise ValueError("Got unknown setting %s." % key)

    @staticmethod
    def defaults():
        return {
            "drat": False,
            "cnf": False,
            "wcnf": False,
            "arbitraryPrecision": False,
            "enableFreeNames": True,
            "printStats": False,
            "proofFile": None
        }

    def computeNumUse(self):
        return self.lazy or not self.disableDeletion

    @classmethod
    def addArgParser(cls, parser, name="misc"):
        defaults = cls.defaults()
        group = parser.add_argument_group(name)

        group.add_argument(
            '--drat',
            help="Process CNF with DRAT proof.",
            action="store_true", dest=name+".drat", default=False
        )

        group.add_argument(
            '--cnf',
            help="Process CNF with PB proof.",
            action="store_true", dest=name+".cnf", default=False
        )

        group.add_argument(
            '--wcnf',
            help="Process WCNF with PB proof.",
            action="store_true", dest=name+".wcnf", default=False
        )

        group.add_argument("--arbitraryPrecision",
                           action="store_true",
                           default=defaults["arbitraryPrecision"],
                           help="(deprecated) Has no effect, arbitraryPrecision is always used.",
                           dest=name+".arbitraryPrecision")
        group.add_argument("--no-arbitraryPrecision",
                           action="store_false",
                           help="(deprecated) Has no effect, arbitraryPrecision is always used.",
                           dest=name+".arbitraryPrecision")

        group.add_argument("--freeNames",
                           action="store_true",
                           default=defaults["enableFreeNames"],
                           help="Enable use of arbitrary variable names.",
                           dest=name+".enableFreeNames")
        group.add_argument("--no-freeNames",
                           action="store_false",
                           help="Disable use of arbitrary variable names.",
                           dest=name+".enableFreeNames")

        group.add_argument("--checkOutputConstraints",
                           action="store_true",
                           help="Enable writing an `e` rule every time a constraint was derived when `--proofOutput` is given.",
                           dest=name+".checkOutputConstraints")

        group.add_argument("--stats",
                           action="store_true",
                           default=defaults["printStats"],
                           help="Print statistics on terminations.",
                           dest=name+".printStats")
        group.add_argument("--no-stats",
                           action="store_false",
                           help="Disable printing of statistics on terminations.",
                           dest=name+".printStats")

        group.add_argument('--proofOutput', type=argparse.FileType('w'),
                           help="Produce a simplified proof file.",
                           dest=name+".proofFile")
        group.add_argument('--elaborate', type=argparse.FileType('w'),
                           help="Alias for '--proofOutput'.",
                           dest=name+".proofFile")

    @classmethod
    def extract(cls, result, name="misc"):
        preset = dict()
        defaults = cls.defaults()
        for key in defaults:
            try:
                preset[key] = getattr(result, name + "." + key)
            except AttributeError:
                pass
        return cls(preset)

    def __repr__(self):
        return type(self).__name__ + repr(vars(self))


def run(formulaFile, rulesFile, outputFormulaFile=None, verifierSettings=None, miscSettings=Settings()):
    if profile:
        pr = cProfile.Profile()
        pr.enable()

    if verifierSettings == None:
        verifierSettings = Verifier.Settings()

    TimedFunction.startTotalTimer()

    rules = list(get_registered_rules())

    context = Context()

    def newIneqFactory():
        return CppIneqFactory(miscSettings.enableFreeNames)
    context.ineqFactory = newIneqFactory()
    context.newIneqFactory = newIneqFactory

    # Add output formula file to context, so that output check rule knows about it.
    context.outputFormulaFile = outputFormulaFile
    context.outputCNF = miscSettings.cnf
    context.outputWCNF = miscSettings.wcnf

    if miscSettings.drat or miscSettings.cnf:
        parser = parseCnf
    elif miscSettings.wcnf:
        parser = parseWcnf
    else:
        parser = parseOpb

    try:
        formula = loadFormula(formulaFile.name, parser,
                              context.ineqFactory.varNameMgr)
    except ParseError as e:
        e.fileName = formulaFile.name
        raise e

    context.formula = formula["constraints"]
    context.objective = formula["objective"]
    context.originalObjectiveTerms = formula["objective"]
    if context.originalObjectiveTerms is not None:
        context.originalObjectiveTerms = context.originalObjectiveTerms.copy()
    context.objectiveConstant = formula["objectiveConstant"]
    context.originalObjectiveConstant = formula["objectiveConstant"]
    context.aliases = formula["alias_to_id"]
    context.formula_id_to_alias = formula["id_to_alias"]

    def newPropEngine(initFormulaSize=False):
        if initFormulaSize:
            return CppPropEngine(formula["numVariables"])
        else:
            return CppPropEngine(0)

    context.propEngine = newPropEngine(True)
    context.newPropEngine = newPropEngine

    context.proof = Proof(miscSettings.proofFile, None,
                          formula["constraints"], context.ineqFactory.varNameMgr)

    verify = Verifier(
        context=context,
        settings=verifierSettings)

    try:
        if not miscSettings.drat:
            ruleParser = RuleParser(context)
            if verifierSettings.progressBar:
                context.ruleCount = ruleParser.numRules(rulesFile)

            rules = ruleParser.parse(
                rules, rulesFile, dumpLine=verifierSettings.trace)
        else:
            ruleParser = DRATParser(context)
            rules = ruleParser.parse(rulesFile)

        return verify(rules)
    except ParseError as e:
        e.fileName = rulesFile.name
        raise e
    finally:
        if miscSettings.printStats:
            print()
            TimedFunction.print_stats()
            context.propEngine.printStats()
            dominance_stats.print_stats()
            verify.print_stats()
            print_custom_timers(context)

        if profile:
            heap = hpy()
            print(heap.heap())
            pr.disable()
            convert2kcachegrind(pr.getstats(), 'callgrind.out.py.profile')

        if miscSettings.proofFile:
            miscSettings.proofFile.flush()


def print_custom_timers(context):
    for (name, time) in context.total_time_custom.items():
        print("c statistic: custom timer: {}: {:.4f}s".format(name, time))

    for name in context.running_timer_custom.keys():
        logging.warning(
            "The timer with name '{}' is still running!".format(name))


def runUI(*args, **kwargs):
    try:
        result = run(*args, **kwargs)
        result.print()

    except KeyboardInterrupt as e:
        print("Interrupted by user.")
        return 100

    except InvalidProof as e:
        print("Verification failed.")
        line = getattr(e, "lineInFile", None)
        where = ""
        if line is not None:
            print("Failed in proof file line %i." % (line))
        hint = str(e)

        if len(hint) > 0:
            print("Hint: %s" % (str(e)))
        return 5

    except ParseError as e:
        print("Verification failed.")
        logging.error(e)
        return 4

    except MemoryError as e:
        try:
            logging.error("MemoryError, probably out of memory.")

        except MemoryError:
            # it could be we do not have enough memory for reporting
            # out of memory.
            pass

        return 3

    except NotImplementedError as e:
        logging.error("Not Implemented: %s" % str(e))
        return 2

    except ValueError as e:
        print("Verification failed.")
        logging.error(e)
        return 6

    except Exception as e:
        print("Verification failed.")
        if logging.getLogger().getEffectiveLevel() != logging.DEBUG:
            logging.error("Sorry, there was an internal error. Please rerun with debugging "
                          "and make a bug report.")
        else:
            print("Sorry, there was an internal error. Because you are running in "
                  "debug mode I will give you the exception.")
            raise e

    return 0


def print_VeriPB_header():
    print("Running VeriPB version", importlib.metadata.version('veripb'))


def run_cmd_main():
    print_VeriPB_header()
    p = argparse.ArgumentParser(
        prog="VeriPB",
        description="""Command line tool to verify derivation
            graphs. See README.md for a description of the file
            format.""")
    p.add_argument("formula", help="Formula containing axioms.",
                   type=argparse.FileType('r'))
    p.add_argument("derivation", help="Refutation / Proof Log.",
                   type=argparse.FileType('r'))
    p.add_argument(
        '-d', '--debug',
        help="Print lots of debugging statements.",
        action="store_const", dest="loglevel", const=logging.DEBUG,
        default=logging.INFO,
    )
    p.add_argument(
        '-v', '--verbose',
        help="Be verbose",
        action="store_const", dest="loglevel", const=logging.INFO,
    )
    p.add_argument("output_formula", help="Output formula for output type 'FILE'.", nargs='?',
                   type=argparse.FileType('r'), default=None)

    Verifier.Settings.addArgParser(p)
    Settings.addArgParser(p)

    args = p.parse_args()

    verifyerSettings = Verifier.Settings.extract(args)
    miscSettings = Settings.extract(args)

    logging.basicConfig(level=args.loglevel)

    return runUI(args.formula, args.derivation, args.output_formula, verifyerSettings, miscSettings)

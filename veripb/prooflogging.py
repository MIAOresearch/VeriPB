from veripb.optimized.constraints import ProofContext
import tempfile


class Proof():
    def __init__(self, file, tmp_buffer, formula, varNameManager, subproof=False):
        self.numConstraints = len(formula)
        self.file = file
        self.subproof = subproof
        self.tmp_buffer = tmp_buffer
        if not self.subproof:
            self.print("pseudo-Boolean proof version 2.0")
            self.print("f %i" % (self.numConstraints))
        if file:
            self.proofContext = ProofContext(self.numConstraints, True)
        else:
            self.proofContext = ProofContext(self.numConstraints, False)
        self.proofContext.setVariableNameManager(varNameManager)

    def nextId(self):
        self.numConstraints += 1
        return self.numConstraints

    def lastId(self):
        return self.numConstraints

    def nextIdNoInc(self):
        return self.numConstraints + 1

    def setId(self, numId):
        self.numConstraints = numId

    def print(self, *args, **kwargs):
        if self.file is not None:
            if self.tmp_buffer:
                kwargs["file"] = self.tmp_buffer
            else:
                kwargs["file"] = self.file
            print(*args, **kwargs)

    def setTemporaryProofBuffer(self):
        self.tmp_buffer = tempfile.NamedTemporaryFile(mode="w")

    def writeTemporaryProofBuffer(self):
        with open(self.tmp_buffer.name) as f:
            self.tmp_buffer = None
            for line in f:
                self.print(line, end='')

    def forgetTemporaryProofBuffer(self):
        self.tmp_buffer = None

    def getProofContext(self, startId=None):
        if startId is None:
            startId = self.numConstraints
        self.proofContext.lastId = startId
        self.proofContext.resetBuffer()
        return self.proofContext

    def getBuffer(self, updateId=False):
        if updateId:
            self.numConstraints = self.proofContext.lastId
        return self.proofContext.getBuffer()

    def resetBuffer(self):
        self.proofContext.resetBuffer()

    def __bool__(self):
        return bool(self.file)

from veripb import InvalidProof
from veripb.substitution import *

from veripb.timed_function import TimedFunction

from collections import deque

from veripb.printing import printRUPTrace


def objectiveToDict(formula):
    if (formula.hasObjective):
        coeffs = formula.objectiveCoeffs
        literals = formula.objectiveLits
        return {lit: coeff for (coeff, lit) in zip(coeffs, literals)}
    else:
        return None


class TemporaryAttach():
    def __init__(self, propEngine):
        self.propEngine = propEngine
        self.attached = []

    def attach(self, constraint, output_constraint_id=None):
        self.attached.append(self.propEngine.attach(
            constraint, 0))
        self.propEngine.moveToCore(self.attached[-1], 0)
        if output_constraint_id is not None:
            self.attached[-1].setOutId(0, output_constraint_id)

    def detachAll(self):
        for c in self.attached:
            self.propEngine.detach(c, 0)

    def __enter__(self):
        return self

    def __exit__(self, exec_type, exec_value, exec_traceback):
        self.detachAll()


class Autoprover():
    # @TimedFunction.time("Autoprover::setup")
    def __init__(self, context, db, subgoals):
        self.context = context
        self.proof = context.proof
        self.subgoals = subgoals
        self.verbose = context.verifierSettings.trace
        self.traceFailed = context.verifierSettings.traceFailed
        self.context.propEngine.increaseNumVarsTo(
            context.ineqFactory.numVars())
        self.propEngine = context.propEngine
        self.db = db
        self.dbSubstituted = None
        self.dbSet = None
        self.assignment = None
        self.wasRUP = False
        self.triedRUP = False

    # @TimedFunction.time("Autoprover::propagate")
    def propagate(self, onlyCore=False):
        self.assignment = Substitution()
        self.assignment.addConstants(self.propEngine.propagatedLits(onlyCore))

    def getOutGoal(self, goalId, ineq):
        if isinstance(goalId, int):
            return ineq.getOutId(goalId)
        else:
            return goalId

    # @TimedFunction.time("Autoprover::inDB")
    def inDB(self, nxtGoalId, nxtGoal, onlyCore):
        constraint = self.propEngine.find(nxtGoal, onlyCore)
        if constraint:
            if self.context.proof:
                self.context.proof.print("\t* autoproven: found in database")
                self.context.proof.print(
                    "\tproofgoal", self.getOutGoal(nxtGoalId, nxtGoal))
                cId = self.context.proof.nextId()
                if onlyCore:
                    self.context.proof.print(
                        "\t\tpol", cId, constraint.getOutId(constraint.getSomeCoreId()), "+")
                else:
                    self.context.proof.print(
                        "\t\tpol", cId, constraint.getOutId(constraint.getSomeId()), "+")
                cId = self.context.proof.nextId()
                self.context.proof.print("\tend", cId)

            if self.verbose:
                print("    automatically proved %s by finding constraint in database" % (
                    str(nxtGoalId)))
            return True
        return False

    # @TimedFunction.time("Autoprover::dbImplication")
    def dbImplication(self, nxtGoalId, nxtGoal, onlyCore, max_coeff):
        success = False
        if self.dbSubstituted is None:
            asmnt = self.getPropagatedAssignment(onlyCore).get()
            self.dbSubstituted = list()
            for (Id, ineq) in self.db:
                if (not onlyCore) or ineq.isCoreId(Id):
                    ineq_max_coeff = ineq.get_max_coeff()
                    if ineq_max_coeff > max_coeff:
                        max_coeff = ineq_max_coeff
                    ineq_cp = ineq.copy().substitute(asmnt)
                    ineq_cp.setOutId(Id, ineq.getOutId(Id))
                    self.dbSubstituted.append((Id, ineq_cp))

        for ineqId, ineq in self.dbSubstituted:
            if ineq.implies_strong(nxtGoal, self.context.proof.getProofContext()):
                success = True
                if self.context.proof:
                    # Idea of elaboration:
                    # Derive one cardinality constraint containing all literals propagated at level 0 and degree being the number of literals in the propagation. Hence, this constraint unit propagated all literals to the right value. This constraint has slack 0 and multiplying the constraint preserves the slack 0. Thus, if we add the unit propagation constraint multiplied by a large enough integer, so that all cancellation happen completely, then the slack of the resulting constraints will never be worse and actually be reduced by the the total sum of the coefficients of the cancellations. Furthermore, as the variables in the unit propagation are completely cancelled, the implication proof will not talk about propagated variables. So we achieve the desired effect of plugging in the unit propagations and do not mess up the implication proof.
                    self.context.proof.print(
                        "\t* autoproven: implied from substituted database")
                    self.context.proof.print(
                        "\tproofgoal", self.getOutGoal(nxtGoalId, nxtGoal))
                    cId = self.context.proof.nextId()
                    weakening_steps = self.context.proof.getBuffer()

                    # Generate constraint from unit propagation.
                    self.context.proof.resetBuffer()
                    self.propEngine.get_unit_propagation_hints(
                        onlyCore, self.context.proof.getProofContext())
                    constants = self.getPropagatedAssignment(
                        onlyCore).constants
                    self.context.proof.print("\t\trup",
                                             " ".join(["1 " + self.context.ineqFactory.int2lit(lit) for lit in constants]), ">=", len(constants), ";", self.context.proof.getBuffer(), "~")
                    solution = self.context.proof.nextId()

                    # Add unit propagation constraint with large enough coefficient to each side of the implication.
                    self.context.proof.print(
                        "\t\tpol", cId, solution, max_coeff, "* +", ineq.getOutId(ineqId), solution, max_coeff, "* +", weakening_steps, "+")
                    cId = self.context.proof.nextId()
                    self.context.proof.print("\tend", cId)
                break

        if success:
            if self.verbose:
                print("    automatically proved %s by substituted implication from %i" %
                      (str(nxtGoalId), ineqId))
            return True
        return False

    # @TimedFunction.time("Autoprover::rupImplication")
    def rupImplication(self, nxtGoalId, nxtGoal, onlyCore, addConstraintID=True):
        success = nxtGoal.rupCheck(
            self.propEngine, onlyCore, self.context.proof.getProofContext(), self.context.verifierSettings.toAnnotatedRUP)
        if success:
            if self.context.proof:
                # If a proofgoal was proven by RUP
                if nxtGoalId is not None:
                    self.context.proof.print("\t* autoproven: implied by RUP")
                    self.context.proof.print(
                        "\tproofgoal", self.getOutGoal(nxtGoalId, nxtGoal))
                    if addConstraintID:
                        self.context.proof.nextId()

                if self.context.verifierSettings.toAnnotatedRUP:
                    # We already started the proof by contradiction with `proofgoal`, so we can du RUP of `>= 1`. But we need to replace ~ with the correct constraint ID of the `proofgoal`.
                    self.context.proof.print(
                        "\t\trup >= 1 ;", self.context.proof.getBuffer().replace("~", str(self.context.proof.nextIdNoInc() - 1)))
                else:
                    self.context.proof.print(
                        "\t\tpol", self.context.proof.getBuffer())

                # If a proofgoal was proven by RUP
                if nxtGoalId is not None:
                    self.context.proof.print(
                        "\tend", self.context.proof.nextId())

            if self.verbose:
                if nxtGoalId is not None:
                    print("    automatically proved %s by RUP check." %
                          (str(nxtGoalId)))
                    # self.getPropagatedAssignment()
                else:
                    print(
                        "    automatically proved constraint to be added by RUP check.")
                    # self.getPropagatedAssignment()
            return True
        return False

    def getPropagatedAssignment(self, onlyCore=False):
        if self.assignment is None:
            self.propagate(onlyCore)

        return self.assignment

    @TimedFunction.time("Autoprover")
    def __call__(self, onlyCore):
        while self.subgoals:
            nxtGoalId, nxtGoal = self.subgoals.popleft()

            # Implication from the negated constraint and if proofgoal is in DB is already checked when creating proofgoals, hence it is already skipped.

            asRhs = nxtGoal.getAsRightHand()
            if asRhs is None:
                asLhs = nxtGoal.getAsLeftHand()

                with TemporaryAttach(self.propEngine) as temporary:
                    for c in asLhs:
                        if c is not None:
                            temporary.attach(c)
                            if self.context.proof:
                                # Set output proof constraint ID for constraints introduced by proofgoal
                                c.setOutId(
                                    c.getSomeId(), self.context.proof.nextId())

                    if self.rupImplication(nxtGoalId, self.context.ineqFactory.fromTerms([], 1), onlyCore, False):
                        continue

            else:
                nxtGoal = asRhs
                if nxtGoal.isTrivial():
                    if self.context.proof:
                        self.context.proof.print(
                            "\t* autoproven: trivial constraint")
                        self.context.proof.print(
                            "\tproofgoal", self.getOutGoal(nxtGoalId, nxtGoal))
                        cId = self.context.proof.nextId()
                        self.context.proof.print("\tend", cId)

                    if self.verbose:
                        print("    automatically proved %s, constraint is trivial." % (
                            str(nxtGoalId)))
                    continue

                # If we have no other proofgoals left, then this first RUP check might be too costly if the constraint is implied by DB.
                if len(self.subgoals) > 0:
                    if not self.triedRUP and self.rupImplication(None, self.context.ineqFactory.fromTerms([], 1), onlyCore):
                        self.wasRUP = True
                        break

                    self.triedRUP = True

                # this is already checked when the effected constraints
                # are computed. However, due to caching it could be that
                # new constraints were added since then.
                if self.inDB(nxtGoalId, nxtGoal, onlyCore):
                    continue

                if self.rupImplication(nxtGoalId, nxtGoal, onlyCore):
                    if len(self.subgoals) == 0 and str(nxtGoalId) == "#1":
                        self.context.autoRUPstreak = getattr(
                            self.context, "autoRUPstreak", 5) + 1
                    continue

                # implication checks are stronger if we plug in propagated literals
                max_coeff = nxtGoal.get_max_coeff()
                nxtGoalSubstituted = nxtGoal.copy()
                asmnt = self.getPropagatedAssignment(onlyCore).get()
                nxtGoalSubstituted.substitute(asmnt)
                if isinstance(nxtGoalId, int):
                    nxtGoalSubstituted.copyId(nxtGoal)
                    nxtGoalSubstituted.setOutId(
                        nxtGoalId, nxtGoal.getOutId(nxtGoalId))

                if self.dbImplication(nxtGoalId, nxtGoalSubstituted, onlyCore, max_coeff):
                    continue

            if self.traceFailed:
                printRUPTrace(self.context, onlyCore)
            raise InvalidProof(
                "Could not prove proof goal %s automatically. Please add an explicit proof for this proof goal. (using 'proofgoal %s')" % (str(nxtGoalId), str(nxtGoalId)))


def autoProof(context, db, subgoals, onlyCore=False):
    goals = deque(((nxtGoalId, nxtGoal) for nxtGoalId,
                  nxtGoal in subgoals.items() if not nxtGoal.isProven))
    subgoals.clear()
    context.rupWithoutSubgoals = False

    if not goals:
        return

    prover = Autoprover(context, db, goals)
    prover(onlyCore)

    if prover.wasRUP:
        context.autoRUPstreak = getattr(context, "autoRUPstreak", 5) + 1
    else:
        context.autoRUPstreak = 0

    context.rupWithoutSubgoals = prover.wasRUP

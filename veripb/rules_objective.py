from veripb.rules_multigoal import *
from veripb.timed_function import TimedFunction

from veripb.printing import print_pretty

NEW_OBJECTIVE = "new"
OBJECTIVE_DIFF = "diff"


def checkObjective(context, wantedObjective, wantedObjectiveConstant):
    if context.objective is None and wantedObjective is not None:
        raise InvalidProof(
            "Original problem has no objective, while output problem has objective.")
    if context.objective is not None and wantedObjective is None:
        raise InvalidProof(
            "Original problem has objective, while output problem has no objective.")

    # Check if actual objective is matching if it exists.
    if context.objective is not None:
        # Check if constants are matching up.
        if context.objectiveConstant != wantedObjectiveConstant:
            raise InvalidProof("Objective constants are not matching up. Current objective: {}, Wanted objective: {}".format(
                context.objectiveConstant, wantedObjectiveConstant))

        # Check if terms are matching up.
        for lit, coeff in context.objective.items():
            if lit not in wantedObjective:
                raise InvalidProof("Term from current objective not in wanted objective: {} {}".format(
                    coeff, context.ineqFactory.int2lit(lit)))
            if wantedObjective[lit] != coeff:
                raise InvalidProof("Coefficient for objective literal {} is mismatching. Current objective: {}, Wanted objective: {}".format(
                    context.ineqFactory.int2lit(lit), coeff, wantedObjective[lit]))
            del wantedObjective[lit]
        if wantedObjective:
            raise InvalidProof("The following terms are in the wanted objective but not in the current objective: " +
                               ", ".join([str(coeff) + " " + context.ineqFactory.int2lit(lit) for lit, coeff in wantedObjective.items()]))


@register_rule
class UpdateObjective(MultiGoalRule):
    Ids = ["obju"]

    @classmethod
    def parse(cls, words, context):
        if context.objective is None:
            raise InvalidProof(
                "The rule 'obju' can only be used for optimization problems, but there was no objective specified in formula file.")

        update_type = next(words, None)
        if update_type is None or update_type not in [NEW_OBJECTIVE, OBJECTIVE_DIFF]:
            raise InvalidProof(
                "Expected objective update type 'new' or 'diff' as first parameter for objective update rule.")

        cppWordIter = words.wordIter.getNative()
        function_terms, function_constant = context.ineqFactory.parseObjective(
            cppWordIter)

        # We need to consider that the maybe is a ";" as separator or not.
        maybe_separator = next(words, None)
        if maybe_separator is None:
            autoProveAll = True
        elif maybe_separator == cls.subProofBegin:
            autoProveAll = False
        elif maybe_separator == ";":
            autoProveAll = not cls.parseHasExplicitSubproof(words)
        else:
            ValueError("Unexpected word, expected 'begin'")

        context.propEngine.increaseNumVarsTo(context.ineqFactory.numVars())

        return cls(context, update_type, function_terms, function_constant, autoProveAll)

    def __init__(self, context, update_type, function_terms, function_constant, autoProveAll):
        super().__init__(context)

        self.function_terms = function_terms
        self.function_constant = function_constant
        self.autoProveAll = autoProveAll
        self.update_type = update_type

    def antecedentIDs(self):
        return "all"

    @TimedFunction.time("ObjectiveUpdate")
    def compute(self, antecedents, context):
        # The function_terms and function_constant describe the new objective function.
        if self.update_type == NEW_OBJECTIVE:
            # Calculate constraint f_new >= f_old.
            terms = list()
            degree = context.objectiveConstant - self.function_constant
            for lit, coeff in self.function_terms.items():
                terms.append((coeff, lit))
            for lit, coeff in context.objective.items():
                terms.append((-coeff, lit))
            geq_ineq = context.ineqFactory.fromTerms(terms, degree)

            # Calculate constraint f_old >= f_new.
            for idx in range(len(terms)):
                terms[idx] = (-terms[idx][0], terms[idx][1])
            leq_ineq = context.ineqFactory.fromTerms(terms, -degree)

        # The function_terms and function_constant describe f_new - f_old.
        elif self.update_type == OBJECTIVE_DIFF:
            # The proofgoal f_new >= f_old is just the function_terms >= -function_constant.
            terms = [(coeff, lit)
                     for lit, coeff in self.function_terms.items()]
            geq_ineq = context.ineqFactory.fromTerms(
                terms, -self.function_constant)

            # The proofgoal f_old >= f_new is just the -function_terms >= function_constant.
            terms = [(-coeff, lit)
                     for lit, coeff in self.function_terms.items()]
            leq_ineq = context.ineqFactory.fromTerms(
                terms, self.function_constant)

        # Print output proof here, as subgoals might be autoproven immediately.
        if context.proof:
            context.proof.print(
                "obju", self.update_type, " ".join(str(coeff) + " " + context.ineqFactory.int2lit(lit) for (lit, coeff) in self.function_terms.items()), self.function_constant, "; begin")

        # Add proofgoals.
        if context.verifierSettings.trace:
            print("  ** proofgoal from new objective >= old objective **")
        geq_goal = SubGoal(geq_ineq)
        self.addSubgoal(geq_goal)
        if context.verifierSettings.trace:
            print("  ** proofgoal from old objective >= new objective **")
        leq_goal = SubGoal(leq_ineq)
        self.addSubgoal(leq_goal)

        # Compute new objective.
        if self.update_type == NEW_OBJECTIVE:
            new_objective_terms = self.function_terms
            new_objective_constant = self.function_constant
        elif self.update_type == OBJECTIVE_DIFF:
            new_objective_terms = context.objective
            new_objective_constant = context.objectiveConstant + self.function_constant
            # Go through objective diff and update objective accordingly.
            for lit, coeff in self.function_terms.items():
                if coeff == 0:
                    continue
                if lit in new_objective_terms:
                    # Literal has same sign.
                    new_objective_terms[lit] += coeff
                elif -lit in new_objective_terms:
                    # This case is tricky, as we have to pay attention to cancellations.
                    if new_objective_terms[-lit] > coeff:
                        # Old coefficient is bigger.
                        new_objective_terms[-lit] -= coeff
                        new_objective_constant += coeff
                    else:
                        if new_objective_terms[-lit] < coeff:
                            # New coefficient is bigger.
                            new_objective_terms[lit] = coeff - \
                                new_objective_terms[-lit]
                        new_objective_constant += new_objective_terms[-lit]
                        del new_objective_terms[-lit]

                else:
                    new_objective_terms[lit] = coeff

        if self.autoProveAll:
            self.autoProof(context, antecedents, True)
            context.objective = new_objective_terms
            context.objectiveConstant = new_objective_constant

            # Print new objective to trace
            if context.verifierSettings.trace:
                print_pretty(context.verifierSettings.useColor, "  ${cid}Objective${reset} update to: ${obj}min", " ".join(
                    [str(coeff) + " " + context.ineqFactory.int2lit(lit) for (lit, coeff) in context.objective.items()]), str(context.objectiveConstant), "${reset}")

            if context.proof:
                context.proof.print("end")
        else:  # There is an explicit subproof
            if context.only_core_subproof:
                raise NotImplementedError(
                    "Nesting of only core subproofs has not been implemented, yet. Only core subproofs are required for checked deletion and objective update.")
            context.only_core_subproof = True
            self.subContext.set_only_core_subproof = True
            self.addObjective(new_objective_terms,
                              new_objective_constant)

        return super().compute(antecedents, context)


@register_rule
class EqualObjective(EmptyRule):
    Ids = ["eobj"]

    @classmethod
    def parse(cls, words, context):
        if context.objective is None:
            raise InvalidProof(
                "The rule 'eobj' can only be used for optimization problems, but there was no objective specified in formula file.")

        cppWordIter = words.wordIter.getNative()
        function_terms, function_constant = context.ineqFactory.parseObjective(
            cppWordIter)

        if function_terms is None:
            raise InvalidProof("No objective to check has been specified.")

        context.propEngine.increaseNumVarsTo(context.ineqFactory.numVars())

        return cls(context, function_terms, function_constant)

    def __init__(self, context, function_terms, function_constant):
        self.function_terms = function_terms
        self.function_constant = function_constant

    def antecedentIDs(self):
        return []

    def compute(self, _, context):
        checkObjective(context, self.function_terms, self.function_constant)

        return []

    def justify(self, _antecedents, _derived, context):
        context.proof.print("eobj", " ".join(str(coeff) + " " + context.ineqFactory.int2lit(lit)
                            for (lit, coeff) in context.objective.items()), context.objectiveConstant, ";")

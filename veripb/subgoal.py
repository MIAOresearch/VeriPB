class SubGoal:
    def __init__(self, constraint):
        self.constraint = constraint
        self.isProven = False

    def getAsLeftHand(self):
        return [self.constraint.copy().negated()]

    def getAsRightHand(self):
        return self.constraint

    def toString(self, ineqFactory):
        return ineqFactory.toString(self.constraint)


class NegatedSubGoals:
    def __init__(self, constraints):
        self.constraints = constraints
        self.isProven = False

    def getAsLeftHand(self):
        return self.constraints

    def getAsRightHand(self):
        return None

    def toString(self, ineqFactory):
        constraintsString = " ".join([ineqFactory.toString(
            constraint) for constraint in self.constraints])
        return "not [%s]" % constraintsString

    def isProven():
        return False
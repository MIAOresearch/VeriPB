#pragma once

#include <algorithm>
#include <chrono>
#include <cstddef>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <memory>
#include <numeric>
#include <sstream>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "BigInt.hpp"
#include "Logging.hpp"

#ifndef NDEBUG
#include <execinfo.h>
#include <unistd.h>
#endif

#define LEVEL_ZERO_PROPAGATION_INTERVAL 10
#define DEFAULT_CONSTRAINT_ID std::numeric_limits<uint64_t>::max()
#define NO_OUTPUT_ID_FOUND 0

using CoefType = BigInt;
// use one of the following lines instead to increase precision.
// using CoefType = long long;
// using CoefType = BigInt;

// #undef NDEBUG

#undef assert
#ifdef NDEBUG
#define assert(x) ((void)sizeof(x))
#else
#define assert(x) ((void)(!(x) && assert_handler(#x, __FILE__, __LINE__)))
#endif

// assert that is also triggered in release mode, i.e., when NDEBUG is set.
#define _assert_(x) ((void)(!(x) && assert_handler(#x, __FILE__, __LINE__)))

#define unreachible(x) \
  ((void)(assert_handler(#x, __FILE__, __LINE__), throw ""))

class assertion_error : public std::runtime_error
{
public:
  assertion_error(const std::string &what_arg) : std::runtime_error(what_arg) {}
};

inline bool assert_handler(std::string condition, std::string file, int line)
{
  std::stringstream s;
  s << std::endl
    << file << ":" << line << " Assertion failed: '" << condition << "'";

#ifndef NDEBUG
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  backtrace_symbols_fd(array, size, STDERR_FILENO);
#endif

  throw assertion_error(s.str());
  return false;
}

class Timer
{
private:
  std::chrono::high_resolution_clock::time_point start;
  std::chrono::duration<double> &targetTimer;

public:
  Timer(std::chrono::duration<double> &targetTimer_)
      : targetTimer(targetTimer_)
  {
    start = std::chrono::high_resolution_clock::now();
  }

  ~Timer()
  {
    auto stop = std::chrono::high_resolution_clock::now();
    targetTimer += (stop - start);
  }
};

extern int hashColision;

class IJunkyard
{
public:
  virtual void clear() = 0;
};

class MasterJunkyard
{
public:
  std::vector<IJunkyard *> yards;

  static MasterJunkyard &get()
  {
    static MasterJunkyard junkyard;
    return junkyard;
  }

  void clearAll()
  {
    for (IJunkyard *yard : yards)
    {
      yard->clear();
    }
  }
};

template <typename T>
class Junkyard : public IJunkyard
{
private:
  std::vector<T> junk;

public:
  Junkyard(MasterJunkyard &master) { master.yards.push_back(this); }

  static Junkyard<T> &get()
  {
    static Junkyard<T> junkyard(MasterJunkyard::get());
    return junkyard;
  }

  void add(T &&value) { junk.push_back(std::move(value)); }

  void clear() { junk.clear(); }
};

template <typename T>
struct PointedEq
{
  bool operator()(T const *lhs, T const *rhs) const
  {
    bool result = (*lhs == *rhs);
    if (!result)
    {
      hashColision += 1;
    }
    return result;
  }
};

template <typename T>
struct PointedHash
{
  std::size_t operator()(T const *element) const
  {
    return std::hash<T>()(*element);
  }
};

template <typename TVal>
TVal divideAndRoundUp(TVal value, TVal divisor)
{
  return (value + divisor - 1) / divisor;
}

long divideAndRoundUp(long value, BigInt divisor);

typedef uint32_t LitData;

class Var
{
public:
  static const LitData LIMIT = std::numeric_limits<LitData>::max() >> 1;

  LitData value;

  explicit Var(LitData value_) : value(value_) {}

  operator size_t() const { return value; }
};

/* lexicographic order respecting numeric values, i.e. "x2" < "x10" */
struct numLexOrder
{
  bool operator()(std::string &a, std::string &b)
  {
    auto itA = a.begin();
    auto itB = b.begin();

    bool numMode = false;
    size_t nZeroA = 0;
    size_t nZeroB = 0;
    while (itA != a.end() && itB != b.end())
    {
      if (std::isdigit(*itA) && std::isdigit(*itB))
      {
        if (!numMode)
        {
          nZeroA = 0;
          while (itA != a.end() && *itA == '0')
          {
            ++itA;
            ++nZeroA;
          };
          nZeroB = 0;
          while (itB != b.end() && *itB == '0')
          {
            ++itB;
            ++nZeroB;
          };

          // both are zero compare number of zeros
          if ((itA == a.end() || !std::isdigit(*itA)) &&
              (itB == b.end() || !std::isdigit(*itB)) && nZeroA != nZeroB)
          {
            return nZeroA < nZeroB;
          }

          size_t posA = std::distance(a.begin(), itA);
          size_t numEndA = a.find_first_not_of("0123456789", posA);
          numEndA = (numEndA == std::string::npos) ? a.size() : numEndA;
          size_t posB = std::distance(b.begin(), itB);
          size_t numEndB = b.find_first_not_of("0123456789", posB);
          numEndB = (numEndB == std::string::npos) ? b.size() : numEndB;

          if (numEndA - posA < numEndB - posB)
          {
            return true;
          }
          else if (numEndA - posA > numEndB - posB)
          {
            return false;
          }
          numMode = true;
          continue;
        }
      }
      else if (numMode)
      {
        if (nZeroA > nZeroB)
        {
          return true;
        }
        else if (nZeroA < nZeroB)
        {
          return false;
        }
        numMode = false;
      }

      if (*itA < *itB)
      {
        return true;
      }
      else if (*itA > *itB)
      {
        return false;
      }

      numMode = std::isdigit(*itA);
      ++itA;
      ++itB;
    }

    if (nZeroA > nZeroB)
    {
      return true;
    }
    else if (nZeroA < nZeroB)
    {
      return false;
    }

    if (itA == a.end() && itB == b.end())
    {
      return false;
    }
    else
    {
      return itA == a.end();
    }
  }
};

class Lit
{
private:
  LitData value;

  friend std::hash<Lit>;

public:
  Lit() {}

  explicit Lit(int t) : value((std::abs(t) << 1) + (t < 0 ? 1 : 0)) {}

  Lit(Var var, bool isNegated)
      : value((var.value << 1) + static_cast<LitData>(isNegated)) {}

  Var var() const { return Var(value >> 1); }

  bool isNegated() const { return value & 1; }

  Lit operator~() const
  {
    Lit res;
    res.value = value ^ 1;
    return res;
  }

  Lit negated() const
  {
    Lit res;
    res.value = value ^ 1;
    return res;
  }

  bool operator==(const Lit &other) const { return value == other.value; }

  bool operator!=(const Lit &other) const { return value != other.value; }

  bool operator>(const Lit &other) const { return value > other.value; }

  bool operator<(const Lit &other) const { return value < other.value; }

  explicit operator size_t() const { return value; }

  explicit operator int64_t() const
  {
    int result = var();
    assert(result != 0);
    if (isNegated())
    {
      result = -result;
    }
    return result;
  }

  static Lit Undef() { return Lit(0); };

  int toInt() const { return value & 1 ? -(static_cast<int>(value) >> 1) : static_cast<int>(value) >> 1; }
};

namespace std
{
  template <>
  struct hash<Lit>
  {
    std::size_t operator()(const Lit &lit) const
    {
      return hash<LitData>()(lit.value);
    }
  };
} // namespace std

inline std::ostream &operator<<(std::ostream &os, const Var &v)
{
  os << "x" << v.value;
  return os;
}

inline std::ostream &operator<<(std::ostream &os, const Lit &v)
{
  if (v.isNegated())
  {
    os << "~";
  }
  os << v.var();
  return os;
}

class VariableNameManager
{
  std::unordered_map<std::string, int> name2num;
  std::vector<std::string> num2name;
  size_t _maxVar = 0;

  bool allowArbitraryNames = false;

public:
  VariableNameManager(bool _allowArbitraryNames)
      : allowArbitraryNames(_allowArbitraryNames) {}

  size_t maxVar() { return std::max(_maxVar, num2name.size()); }

  std::string getName(Var num)
  {
    if (!allowArbitraryNames)
    {
      return "x" + std::to_string(num);
    }
    else
    {
      return num2name[num - 1];
    }
  }

  Var getVar(const std::string_view &name_const)
  {
    if (name_const.size() < 2)
    {
      throw std::invalid_argument("Expected variable, but name is too short.");
    }

    std::string_view name = name_const;
    if (name[name.size() - 1] == ';')
    {
      name.remove_suffix(1);
    }

    if (!allowArbitraryNames)
    {
      if (name[0] != 'x')
      {
        throw std::invalid_argument(
            "Expected variable, but did not start with 'x'.");
      }
      std::string var(name.substr(1));
      unsigned long varVal = std::stoul(var);
      if (varVal > Var::LIMIT)
      {
        throw std::invalid_argument(
            "Variable too large, can not be represented.");
      }
      _maxVar = std::max(_maxVar, varVal);
      return Var(varVal);
    }
    else
    {
      if (!std::isalpha(name[0]) && name[0] != '_')
      {
        throw std::invalid_argument(
            "Expected variable, but did not start with letter.");
      }
      auto result = name2num.insert(std::make_pair(name, num2name.size()));
      bool newName = result.second;
      if (newName)
      {
        num2name.emplace_back(name);
      }
      size_t varVal = result.first->second + 1;
      if (varVal > Var::LIMIT)
      {
        throw std::invalid_argument(
            "Too many variables, can not be represented.");
      }
      return Var(varVal);
    }
  }

  Lit getLit(const std::string_view &name)
  {
    if (name.size() < 1)
    {
      throw std::invalid_argument("Expected literal.");
    }

    bool isNegated = (name[0] == '~');
    int offset = isNegated ? 1 : 0;
    const std::string_view var = name.substr(offset);
    return Lit(this->getVar(var), isNegated);
  }
};

class ProofContext
{
public:
  // General variable names
  VariableNameManager *varNameManager = nullptr;

  std::stringstream buffer;
  uint64_t lastId;
  bool is_logging_proof;

  ProofContext(bool _is_logging_proof) : is_logging_proof(_is_logging_proof) { lastId = 0; }

  ProofContext(uint64_t _lastId, bool _is_logging_proof) : lastId(_lastId), is_logging_proof(_is_logging_proof) {}

  void setVariableNameManager(VariableNameManager &varNameManager)
  {
    this->varNameManager = &varNameManager;
  }

  VariableNameManager *getVariableNameManager()
  {
    return this->varNameManager;
  }

  uint64_t nextId() { return ++lastId; }

  uint64_t nextIdNoInc() { return lastId + 1; }

  std::string getBuffer() { return buffer.str(); }

  void resetBuffer()
  {
    buffer.str(""); // set to empty string
    buffer.clear(); // clear error state of stringstream
  }
};

class Substitution
{
public:
  static Lit zero()
  {
    // note that variable 0 is not a valid variable, hence we use
    // it to represent constants 0 as literal ~Var(0) and 1 as
    // literal Var(0). You may not swap the values of isNegated
    // passed to the constructor!

    return Lit(Var(0), true);
  }

  static Lit one() { return Lit(Var(0), false); }

  std::unordered_map<Lit, Lit> map;
  // var -> lit or 0 or 1

  Substitution(std::vector<int> &constants, std::vector<int> &from,
               std::vector<int> &to)
  {
    assert(from.size() == to.size());
    map.reserve(constants.size() + from.size());
    for (int intLit : constants)
    {
      Lit lit(intLit);
      map.emplace(lit, Substitution::one());
      map.emplace(~lit, Substitution::zero());
    }
    for (size_t i = 0; i < from.size(); i++)
    {
      Lit a(from[i]);
      Lit b(to[i]);
      map.emplace(a, b);
      map.emplace(~a, ~b);
    }
  }
};

/*
 * some syntactic sugar to prevent us from accidentally using Var to
 * go in to a Lit indexed vector and vice versa.
 */
template <typename T>
class LitIndexedVec : public std::vector<T>
{
public:
  using std::vector<T>::vector;
  // uncomment to allow indexing with integers
  // using std::vector<T>::operator[];
  T &operator[](Var idx) = delete;
  T &operator[](Lit idx)
  {
    size_t pos = static_cast<size_t>(idx);
    assert(pos < this->size());
    return this->std::vector<T>::operator[](pos);
  }
  const T &operator[](Lit idx) const
  {
    size_t pos = static_cast<size_t>(idx);
    assert(pos < this->size());
    return this->std::vector<T>::operator[](pos);
  }
};

/*
 * some syntactic sugar to prevent us from accidentally using Var to
 * go in to a Lit indexed vector and vice versa.
 */
template <typename T>
class VarIndexedVec : public std::vector<T>
{
public:
  using std::vector<T>::vector;
  // uncomment to allow indexing with integers
  // using std::vector<T>::operator[];
  T &operator[](Lit idx) = delete;
  T &operator[](Var idx)
  {
    size_t pos = static_cast<size_t>(idx);
    assert(pos < this->size());
    return this->std::vector<T>::operator[](pos);
  }
  const T &operator[](Var idx) const
  {
    size_t pos = static_cast<size_t>(idx);
    assert(pos < this->size());
    return this->std::vector<T>::operator[](pos);
  }
};

template <typename T>
T cpsign(T a, Lit b)
{
  using namespace std;
  if (b.isNegated())
  {
    return -abs(a);
  }
  else
  {
    return abs(a);
  }
}

template <typename T>
struct Term
{
  using coeff_type = T;

  T coeff;
  Lit lit;

  Term() = default;

  Term(T coeff_, Lit lit_) : coeff(coeff_), lit(lit_) {}

  template <typename TTermType>
  Term(const Term<TTermType> &other) : coeff(other.coeff), lit(other.lit) {}

  template <typename TTermType>
  Term<T> &operator=(const Term<TTermType> &other)
  {
    coeff = other.coeff;
    lit = other.lit;
    return *this;
  }

  template <typename TTerm>
  bool operator==(const TTerm &other) const
  {
    return (this->coeff == other.coeff) && (this->lit == other.lit);
  }

  static std::vector<Term<T>> toTerms(std::vector<T> &coeffs,
                                      std::vector<int> &lits)
  {
    assert(coeffs.size() == lits.size());
    size_t size = coeffs.size();
    std::vector<Term<T>> result;
    result.reserve(size);
    for (size_t i = 0; i < size; i++)
    {
      result.emplace_back(coeffs[i], Lit(lits[i]));
    }
    return result;
  }
};

template <>
struct Term<void>
{
  using coeff_type = int32_t;

  Lit lit;
  static constexpr int32_t coeff = 1;

  Term() = default;

  template <typename T>
  Term(T _coeff, Lit _lit) : lit(_lit)
  {
    assert(_coeff == 1);
  }

  template <typename TTermType>
  Term(const Term<TTermType> &other) : lit(other.lit)
  {
    assert(other.coeff == 1);
  }

  Term<void> &operator=(const Lit &_lit)
  {
    lit = _lit;
    return *this;
  }

  template <typename T>
  Term<void> &operator=(const Term<T> &other)
  {
    assert(other.coeff == 1);
    lit = other.lit;
    return *this;
  }

  template <typename TTerm>
  bool operator==(const TTerm &other) const
  {
    return (this->coeff == other.coeff) && (this->lit == other.lit);
  }
};

// While this is not required for correctness, we do want that clauses
// only take up space for terms and not for coefficients.
static_assert(sizeof(Term<void>) == sizeof(Lit));

template <typename T>
inline std::ostream &operator<<(std::ostream &os, const Term<T> &term)
{
  os << term.coeff << " " << term.lit;
  return os;
}

template <typename T>
bool orderByVar(T &a, T &b) { return b.lit > a.lit; }

template <typename T>
bool orderByCoeff(T &a, T &b)
{
  return a.coeff < b.coeff;
}

enum class State : uint8_t
{
  False = 2,
  True = 3,
  Unassigned = 0
};

inline State operator~(State s)
{
  uint8_t i = static_cast<uint8_t>(s);
  i = (i >> 1) ^ i;
  // in case of unassigned (0b00) i >> 1 = 0b00 so nothing is flipped
  // in case of false (0b10) or true (0b11) i >> 1 = 0b01 so the right
  // most bit is flipped which changes true to false and vice versa
  return static_cast<State>(i);
}

class Assignment
{
public:
  LitIndexedVec<State> value;

  Assignment(int nVars) : value(2 * (nVars + 1), State::Unassigned) {}

  Assignment(Assignment &other) : value(other.value) {}

  Assignment(std::vector<int> &assignment)
      : value(2 * (assignment.size() + 1), State::Unassigned)
  {
    for (int int_lit : assignment)
    {
      Lit lit(int_lit);
      assign(lit);
    }
  }

  Assignment(std::vector<int> &assignment, int nVars) : value(2 * (nVars + 1), State::Unassigned)
  {
    for (int int_lit : assignment)
    {
      Lit lit(int_lit);
      assign(lit);
    }
  }

  size_t get_mem_usage() { return sizeof(State) * value.capacity(); }

  void assign(Lit lit)
  {
    value[lit] = State::True;
    value[~lit] = State::False;
  }

  void unassign(Lit lit)
  {
    value[lit] = State::Unassigned;
    value[~lit] = State::Unassigned;
  }

  void resize(size_t nVars)
  {
    value.resize(2 * (nVars + 1), State::Unassigned);
  }

  State &operator[](Lit lit) { return value[lit]; }

  State operator[](Lit lit) const { return this->value[lit]; }
};

template <typename T>
class Inequality;

template <typename T>
using InequalityPtr = std::unique_ptr<Inequality<T>>;

template <typename T>
class FixedSizeInequality;

class Clause;

template <typename T>
class ConstraintHandler;

template <typename T>
using FixedSizeInequalityHandler = ConstraintHandler<FixedSizeInequality<T>>;

using ClauseHandler = ConstraintHandler<Clause>;

template <typename T>
class PropEngine;

struct PropState
{
  // qhead is the point in the trail until which propagations were
  // performed exhaustively
  size_t qhead = 0;
  size_t trailSize = 0;
  bool conflict = false;
};

template <typename T>
struct WatchInfo
{
  T *ineq;
  Lit other;

  WatchInfo() : ineq(nullptr), other(Lit::Undef()) {}
};

template <typename Element>
class InlineVec
{
private:
  uint32_t _size;
  Element data[];

  // warning requires custom allocation, if you know what you are
  // doing add a friend
  InlineVec(size_t size) : _size(size) {};

  template <typename T>
  friend class FixedSizeInequality;
  friend class Clause;

public:
  ~InlineVec() { this->erase(this->begin(), this->end()); }

  void erase(Element *begin, Element *end)
  {
    size_t removed = end - begin;
    assert(removed >= 0);
    for (; begin != end; begin++)
    {
      begin->~Element();
    }
    _size = size() - removed;
  }

  size_t size() const { return _size; }

  Element &operator[](size_t index) { return data[index]; }

  const Element &operator[](size_t index) const { return data[index]; }

  const Element *begin() const { return data; }

  const Element *end() const { return data + size(); }

  Element *begin() { return data; }

  Element *end() { return data + size(); }
};

template <typename TVec, typename TElement>
class extra_size_requirement
{
public:
  static size_t value();
};

template <typename TElement>
class extra_size_requirement<std::vector<TElement>, TElement>
{
public:
  static size_t value(size_t numElements) { return 0; };
};

template <typename TElement>
class extra_size_requirement<InlineVec<TElement>, TElement>
{
public:
  static size_t value(size_t numElements)
  {
    return numElements * sizeof(TElement);
  };
};

struct DBConstraintHeader
{
  bool isMarkedForDeletion = false;
  bool isReason = false;
  std::unordered_map<uint64_t, uint64_t> in_to_out_id;
  uint64_t someId = DEFAULT_CONSTRAINT_ID;
};

class Reason
{
public:
  virtual void rePropagate() = 0;
  virtual bool isMarkedForDeletion() = 0;
  virtual void setIsReason() = 0;
  virtual void unsetIsReason() = 0;
  virtual void print(std::ostream &) = 0;
  virtual std::unordered_map<Lit, BigInt> getTerms() = 0;
  virtual std::vector<Lit> getLits() = 0;
  virtual BigInt getDegree() = 0;
  virtual void logConstraintID(ProofContext &, bool) = 0;
  virtual uint64_t getOutputConstraintID(bool) = 0;
  virtual void resolveInto(std::unordered_map<Lit, BigInt> &, Lit, BigInt, ProofContext &, Assignment &, bool) = 0;
  virtual void *getConstraintPtr() = 0;
  virtual ~Reason() {};
};

using ReasonPtr = std::unique_ptr<Reason, std::function<void(Reason *)>>;

template <typename TConstraint, typename TPropagator>
class GenericDBReason : public Reason
{
  TConstraint *constraint;
  TPropagator *propagator;

public:
  GenericDBReason(TConstraint &_constraint, TPropagator &_propagator)
      : constraint(&_constraint), propagator(&_propagator) {}

  virtual void rePropagate()
  {
    // todo: this may causing duplicate entries in the watch list
    // as init will always add an entry to the watch list no
    // matter if there was an existing entry before, maybe we
    // should have something like initWatch, updateAllWatches,
    // updateSingleWatch.
    constraint->updateWatch(*propagator);
  }

  virtual bool isMarkedForDeletion()
  {
    return constraint->header.isMarkedForDeletion;
  }

  virtual void setIsReason() { constraint->header.isReason = true; }

  virtual void unsetIsReason() { constraint->header.isReason = false; }

  virtual void print(std::ostream &out) { out << *constraint; }

  static ReasonPtr aquire(TConstraint &_constraint, TPropagator &_propagator)
  {
    static std::vector<std::unique_ptr<GenericDBReason>> pool;
    GenericDBReason *result;
    if (!pool.empty())
    {
      result = pool.back().release();
      pool.pop_back();
      GenericDBReason tmp(_constraint, _propagator);
      *result = tmp;
    }
    else
    {
      result = new GenericDBReason(_constraint, _propagator);
    }
    return ReasonPtr(result, [](Reason *ptr)
                     { pool.emplace_back(static_cast<GenericDBReason *>(ptr)); });
  }

  virtual std::unordered_map<Lit, BigInt> getTerms()
  {
    std::unordered_map<Lit, BigInt> terms_map;
    for (auto term : constraint->terms)
    {
      terms_map[term.lit] = term.coeff;
    }
    return terms_map;
  }

  virtual std::vector<Lit> getLits()
  {
    std::vector<Lit> lits;
    for (auto term : constraint->terms)
    {
      lits.push_back(term.lit);
    }
    return lits;
  }

  virtual BigInt getDegree()
  {
    return constraint->degree;
  }

  /// @brief Log the output constraint ID to the proof buffer, which can be used for elaborating RUP to cutting planes.
  /// @param proof Object to access the proof logging.
  virtual inline void logConstraintID(ProofContext &proof, bool onlyCore)
  {
    if (onlyCore)
    {
      proof.buffer << " " << constraint->header.in_to_out_id[constraint->getSomeId()];
    }
    else
    {
      proof.buffer << " " << constraint->header.in_to_out_id.begin()->second;
    }
  }

  /// @brief Log the output constraint ID to the proof buffer and change the ID of the negated constraint (nextId) to ~nextId, which can be used for elaborating RUP to RUP with hints.
  /// @param proof Object to access the proof logging.
  virtual inline uint64_t getOutputConstraintID(bool onlyCore)
  {
    if (onlyCore)
    {
      return constraint->header.in_to_out_id[constraint->getSomeId()];
    }
    else
    {
      return constraint->header.in_to_out_id.begin()->second;
    }
  }

  inline void addLitToConflict(std::unordered_map<Lit, BigInt> &conflict_terms, BigInt &final_reason_coeff, Lit lit)
  {
    // Try same polarity as reason side literal first.
    auto normal_conflict_map_entry = conflict_terms.find(lit);
    if (normal_conflict_map_entry != conflict_terms.end())
    {
      normal_conflict_map_entry->second += final_reason_coeff;
    }
    else
    {
      // Try next the negated polarity as reason side literal.
      auto negated_conflict_map_entry = conflict_terms.find(~lit);
      if (negated_conflict_map_entry != conflict_terms.end())
      {
        if (negated_conflict_map_entry->second <= final_reason_coeff)
        {
          if (negated_conflict_map_entry->second < final_reason_coeff)
          {
            conflict_terms[lit] = final_reason_coeff - negated_conflict_map_entry->second;
          }
          conflict_terms.erase(negated_conflict_map_entry);
        }
        else
        {
          negated_conflict_map_entry->second -= final_reason_coeff;
        }
      }
      else
      {
        conflict_terms[lit] = final_reason_coeff;
      }
    }
  }

  /// @brief Resolve the conflict constraint and reason constraint with each other using the round-to-one procedure from RoundingSAT. The result of the resolution is stored in `conflict_terms`.
  virtual void resolveInto(std::unordered_map<Lit, BigInt> &conflict_terms, Lit propagated_lit, BigInt target_coeff, ProofContext &proof, Assignment &assignment, bool onlyCore)
  {
    logConstraintID(proof, onlyCore);

    BigInt divisor;
    for (auto term : constraint->terms)
    {
      if (term.lit == propagated_lit)
      {
        divisor = term.coeff;
      }
    }
    if (divisor == 1)
    {
      for (auto reason_term : constraint->terms)
      {
        BigInt final_reason_coeff = reason_term.coeff * target_coeff;
        addLitToConflict(conflict_terms, final_reason_coeff, reason_term.lit);
      }
    }
    else
    {
      for (auto reason_term : constraint->terms)
      {
        // Literal needs to be weaken if it is not negated and not the propagated literal
        if ((reason_term.lit != propagated_lit && assignment[reason_term.lit] != State::False && reason_term.coeff % divisor != 0))
        {
          // weaken literal
          proof.buffer << " " << proof.varNameManager->getName(reason_term.lit.var()) << " w";
        }
        else
        {
          BigInt final_reason_coeff = divideAndRoundUp(reason_term.coeff, divisor) * target_coeff;
          addLitToConflict(conflict_terms, final_reason_coeff, reason_term.lit);
        }
      }
    }

    if (divisor != 1)
    {
      proof.buffer << " " << divisor << " d";
    }
    if (target_coeff != 1)
    {
      proof.buffer << " " << target_coeff << " *";
    }
    proof.buffer << " +";
  }

  virtual void *getConstraintPtr()
  {
    return constraint;
  }

  virtual ~GenericDBReason() {}
};

class PropagationMaster;

class Propagator
{
protected:
  size_t qhead = 0;

public:
  PropagationMaster &propMaster;

  Propagator(PropagationMaster &_propMaster);

  virtual void propagate() = 0;
  virtual void increaseNumVarsTo(size_t) = 0;
  virtual size_t get_mem_usage() = 0;
  virtual void reset(size_t pos)
  {
    if (qhead > pos)
      qhead = pos;
  };
  virtual ~Propagator() {};
};

using PropagatorPtr = std::unique_ptr<Propagator>;

class PropagationMaster
{
private:
  Assignment assignment;
  Assignment phase;
  std::vector<Lit> trail;
  ReasonPtr conflictReason;
  Lit conflicting_solution_lit;

  PropState current;

  bool trailUnchanged = true;

  // it seems that marking constraints with setIsReason,
  // unsetIsReason in enqueue and undoOne are quite expensive. If we
  // now that the current propagation is temporary (because it is in
  // an AutoReset block) there is no need to setIsReason because it
  // will be unset eventually.
  bool isTemporary = false;

  friend class AutoReset;

  std::vector<Propagator *> knownPropagators;
  std::vector<Propagator *> activePropagators;

public:
  std::vector<ReasonPtr> reasons;
  std::chrono::duration<double> timeCleanupTrail = std::chrono::seconds(0);

  const Assignment &getAssignment() { return assignment; }
  const Assignment &getPhase() { return phase; }
  const std::vector<Lit> &getTrail() { return trail; }
  const std::vector<ReasonPtr> *getReasons() { return &reasons; }
  const ReasonPtr &getConflictReasons() { return conflictReason; }
  const Lit getConflictingSolutionLit() { return conflicting_solution_lit; }
  const void setConflictingSolutionLit(Lit lit) { conflicting_solution_lit = lit; }

  PropagationMaster(size_t nVars)
      : assignment(nVars), phase(nVars)
  {
    trail.reserve(nVars);
  }

  void addPropagator(Propagator &propagator)
  {
    knownPropagators.push_back(&propagator);
  }

  void activatePropagator(Propagator &propagator)
  {
    activePropagators.push_back(&propagator);
  }

  void deactivatePropagator(Propagator &propagator)
  {
    activePropagators.erase(std::find(activePropagators.begin(),
                                      activePropagators.end(), &propagator));
  }

  size_t get_mem_usage()
  {
    size_t result = 0;
    for (auto &prop : knownPropagators)
    {
      result += prop->get_mem_usage();
    }

    return result + phase.get_mem_usage() + assignment.get_mem_usage();
  }

  void increaseNumVarsTo(size_t nVars)
  {
    assignment.resize(nVars);
    phase.resize(nVars);
    for (Propagator *propagator : knownPropagators)
    {
      propagator->increaseNumVarsTo(nVars);
    }
  }

  void analyze(ProofContext &proof, bool toAnnotatedRUP, bool onlyCore)
  {
    if (conflictReason)
    {
      auto itReason = reasons.rbegin();
      auto itPropagatedLit = trail.rbegin();

      if (toAnnotatedRUP)
      {
        std::vector<uint64_t> usedOutputIDs;
        usedOutputIDs.push_back(conflictReason->getOutputConstraintID(onlyCore));
        void *prev_constraint_ptr = conflictReason->getConstraintPtr();

        // Get the falsified literals of the conflict as relevant propagations for the conflict.
        Assignment relevant_assignment((this->assignment.value.size() / 2) - 1);
        for (auto lit : conflictReason->getLits())
        {
          if (this->assignment[lit] == State::False)
          {
            relevant_assignment.assign(lit.negated());
          }
        }

        while (itPropagatedLit != trail.rend())
        {
          // Check if last assignment is relevant.
          if (relevant_assignment[*itPropagatedLit] == State::True)
          {
            // Check if constraint propagated multiple times in a row and remove duplicates.
            if ((*itReason)->getConstraintPtr() != prev_constraint_ptr)
            {
              usedOutputIDs.push_back((*itReason)->getOutputConstraintID(onlyCore));
              prev_constraint_ptr = (*itReason)->getConstraintPtr();
            }
            // Add falsified literals in reason as relevant propagations for conflict.
            for (auto lit : (*itReason)->getLits())
            {
              if (this->assignment[lit] == State::False)
              {
                relevant_assignment.assign(lit.negated());
              }
            }
          }

          // Go back in the trail.
          ++itPropagatedLit;
          ++itReason;
        }

        // Print output IDs used to reach conflict in reverse order.
        for (auto itOutputID = usedOutputIDs.rbegin(); itOutputID != usedOutputIDs.rend(); itOutputID++)
        {
          if (*itOutputID == proof.nextIdNoInc())
            proof.buffer << " ~";
          else
            proof.buffer << " " << *itOutputID;
        }
      }
      else
      {
        Assignment assignment(this->assignment);
        conflictReason->logConstraintID(proof, onlyCore);

        // We need to keep track of the conflict side constraint to do conflict analysis to elaborate to cutting planes.
        auto conflict_terms_map = conflictReason->getTerms();

        while (itPropagatedLit != trail.rend() && itReason != reasons.rend())
        {
          auto conflict_coeff = conflict_terms_map.find(~(*itPropagatedLit));

          if (conflict_coeff != conflict_terms_map.end())
          {
            (*itReason)->resolveInto(conflict_terms_map, *itPropagatedLit, conflict_coeff->second, proof, assignment, onlyCore);
          }

          assignment.unassign(*itPropagatedLit);
          ++itPropagatedLit;
          ++itReason;
        }
      }
    }
  }

  void conflict(ReasonPtr &&reason)
  {
    if (!current.conflict)
    {
      assert(conflictReason == nullptr);
      current.conflict = true;
      trailUnchanged = false;
      // if a conflict is already set we can not overwrite the
      // previous conflict reason, because otherwise rup and sat
      // checks can cause to overwrite the conflict reason, and
      // will not reset it, so that isTrailClean() will fail and
      // the verifier might get stuck in an unsatisfiable state
      // causing incorrectly accepted proofs.
      if (reason != nullptr)
      {
        conflictReason = std::move(reason);
        conflictReason->setIsReason();
      }
    }
  }

  bool isConflicting() { return current.conflict; }

  void enqueue(Lit lit, ReasonPtr &&reason)
  {
    // std::cout << "Enqueueing: " << lit << std::endl;
    trailUnchanged = false;
    assignment.assign(lit);
    phase.assign(lit);
    trail.push_back(lit);
    current.trailSize = trail.size();
    reasons.emplace_back(std::move(reason));
    if (!isTemporary && reasons.back().get() != nullptr)
    {
      reasons.back()->setIsReason();
    }
  }

  bool isTrailClean()
  {
    if (conflictReason && conflictReason->isMarkedForDeletion())
    {
      return false;
    }

    for (ReasonPtr &reason : reasons)
    {
      if (reason && reason->isMarkedForDeletion())
      {
        return false;
      }
    }

    return true;
  }

  void cleanupTrail()
  {
    Timer timer(timeCleanupTrail);

    std::vector<ReasonPtr> oldReasons;
    std::swap(oldReasons, reasons);
    std::vector<Lit> oldTrail;
    std::swap(oldTrail, trail);

    for (Lit lit : oldTrail)
    {
      assignment.unassign(lit);
    }

    PropState emptyTrail;
    reset(emptyTrail);

    for (size_t i = 0; i < oldTrail.size(); ++i)
    {
      ReasonPtr &reason = oldReasons[i];
      if (reason.get() != nullptr)
      {
        // deleted constraints are not supposed to propagate
        // so this will make sure that no deleted constraint
        // is left on the cleaned up trail.
        reason->rePropagate();
      }
      else
      {
        // nullptr reason can happen when we have decisions,
        // e.g., when checking if an assignment is
        // satisfiable.
        enqueue(oldTrail[i], nullptr);
      }
    }

    assert(isTrailClean());
  }

  void propagate()
  {
    trailUnchanged = false;
    while (!trailUnchanged && !isConflicting())
    {
      trailUnchanged = true;
      for (Propagator *propagator : activePropagators)
      {
        propagator->propagate();
        if (!trailUnchanged)
        {
          break;
        }
      }
    }
    current.qhead = trail.size();
  }

  void undoOne()
  {
    assignment.unassign(trail.back());
    trail.pop_back();
    if (!isTemporary && reasons.back().get() != nullptr)
    {
      reasons.back()->unsetIsReason();
    }
    reasons.pop_back();
  }

  void reset(PropState resetState)
  {
    // std::cout << "Reset to trail pos " << resetState.qhead << std::endl;
    for (Propagator *propagator : knownPropagators)
    {
      propagator->reset(resetState.qhead);
    }

    while (trail.size() > resetState.trailSize)
    {
      undoOne();
    }

    if (!resetState.conflict)
    {
      conflictReason = nullptr;
    }

    current = resetState;
  }
};

class Clause;

class ClausePropagator : public Propagator
{
public:
  using WatchedType = WatchInfo<Clause>;
  using WatchList = std::vector<WatchedType>;

  LitIndexedVec<WatchList> watchlist;

  ClausePropagator(PropagationMaster &_propMaster, size_t nVars)
      : Propagator(_propMaster), watchlist(2 * (nVars + 1)) {}

  void clear()
  {
    reset(0);
    for (WatchList &wl : watchlist)
    {
      wl.clear();
    }
  }

  void watch(Lit lit, WatchedType &w)
  {
    // std::cout << "adding watch  " << lit << std::endl;
    watchlist[lit].push_back(w);
  }

  size_t get_mem_usage()
  {
    size_t result = 0;
    for (auto &wl : watchlist)
    {
      result += sizeof(WatchedType) * wl.capacity();
    }

    return result + sizeof(WatchList) * watchlist.capacity();
  }

  virtual void increaseNumVarsTo(size_t nVars)
  {
    watchlist.resize(2 * (nVars + 1));
  }

  virtual void propagate();
};

using ClauseReason = GenericDBReason<Clause, ClausePropagator>;

class Clause
{
private:
  using TElement = Term<void>;
  using TVec = InlineVec<TElement>;

  friend ClauseHandler;
  friend std::ostream &operator<<(std::ostream &os, const Clause &v);

public:
  using TTerm = TElement;
  size_t propagationSearchStart = 2;

  // common constraint interface:
  DBConstraintHeader header;
  static constexpr int32_t degree = 1;
  TVec terms;

private:
  // size_t size() const {
  //     return terms.size();
  // }

  Clause(size_t size) : terms(size) {}

  Clause(const Clause &other) : terms(other.terms.size())
  {
    using namespace std;
    copy(other.terms.begin(), other.terms.end(), this->terms.begin());
  }

  Clause(std::vector<Lit> &_terms) : terms(_terms.size())
  {
    std::copy(_terms.begin(), _terms.end(), this->terms.begin());
  }

  template <typename T>
  Clause(const FixedSizeInequality<T> &other) : terms(other.terms.size())
  {
    for (size_t i = 0; i < other.terms.size(); ++i)
    {
      terms[i].lit = other.terms[i].lit;
    }
  }

  template <typename TermIter>
  Clause(size_t size, TermIter begin, TermIter end) : terms(size)
  {
    std::copy(begin, end, terms.begin());
  }

  void checkPosition(size_t pos, int &numFound, size_t (&watcher)[2],
                     const Assignment &assignment)
  {
    Lit &lit = this->terms[pos].lit;
    switch (assignment[lit])
    {
    case State::True:
      if (pos == 1)
      {
        watcher[0] = 0;
        watcher[1] = 1;
      }
      else
      {
        watcher[0] = pos;
        watcher[1] = 1;
      }
      numFound = 2;
      break;
    case State::Unassigned:
      watcher[numFound] = pos;
      numFound += 1;
      break;
    case State::False:
      break;
    }
  }

public:
  /// @brief Get the output proof constraint ID of the constraint that corresponds to `in_id` of the input proof.
  /// @param in_id The input proof constraint ID is NOT used for `Clause`.
  /// @return If `in_id` has no output proof constraint associated with it, then `0` is returned as an error. Otherwise, the output proof constraint ID for this constraint is returned.
  uint64_t inline getOutId(uint64_t in_id)
  {
    auto out_id = header.in_to_out_id.find(in_id);
    if (out_id == header.in_to_out_id.end())
    {
      return NO_OUTPUT_ID_FOUND;
    }
    else
    {
      return out_id->second;
    }
  }

  void inline setOutId(uint64_t _, uint64_t out_id)
  {
    header.in_to_out_id[getSomeId()] = out_id;
  }

  void inline removeOutId(uint64_t in_id)
  {
    header.in_to_out_id.erase(in_id);
  }

  void inline setSomeId(uint64_t id)
  {
    header.someId = id;
  }

  uint64_t inline getSomeId()
  {
    return header.someId;
  }

  void clearWatches(ClausePropagator &prop)
  {
    const size_t nWatcher = 2;
    if (terms.size() >= nWatcher)
    {
      for (size_t i = 0; i < nWatcher; i++)
      {
        auto &ws = prop.watchlist[terms[i].lit];
        ws.erase(
            std::remove_if(ws.begin(), ws.end(),
                           [this](auto &watch)
                           { return watch.ineq == this; }),
            ws.end());
      }
    }
  }

  bool isPropagatingAt0() { return terms.size() <= 1; }

  void initWatch(ClausePropagator &prop)
  {
    for (Term<void> term : terms)
    {
      assert(static_cast<size_t>(term.lit.var()) <
             prop.propMaster.getAssignment().value.size());
      assert(static_cast<size_t>(term.lit) < prop.watchlist.size());
    }
    // std::cout << "initializing " << *this << std::endl;
    updateWatch(prop, Lit::Undef(), true);
  }

  bool updateWatch(ClausePropagator &prop, Lit falsifiedLit = Lit::Undef(),
                   bool initial = false)
  {
    bool keep = true;

    if (header.isMarkedForDeletion)
    {
      keep = false;
      return keep;
    }

    assert(falsifiedLit == Lit::Undef() ||
           (this->terms.size() < 2 || (this->terms[0].lit == falsifiedLit ||
                                       this->terms[1].lit == falsifiedLit)));

    int numFound = 0;
    // watcher will contain the position of the terms to be watched
    size_t watcher[2];

    const Assignment &assignment = prop.propMaster.getAssignment();

    if (this->terms.size() >= 2)
    {
      if (this->terms[1].lit == falsifiedLit)
      {
        std::swap(this->terms[0], this->terms[1]);
      }

      checkPosition(0, numFound, watcher, assignment);
      if (numFound < 2)
      {
        checkPosition(1, numFound, watcher, assignment);
      }

      size_t pos = this->propagationSearchStart;
      for (size_t i = 0; i < this->terms.size() - 2 && numFound < 2; i++)
      {
        checkPosition(pos, numFound, watcher, assignment);
        pos += 1;
        if (pos == this->terms.size())
        {
          pos = 2;
        }
      }
      this->propagationSearchStart = pos;
    }
    else if (this->terms.size() == 1)
    {
      checkPosition(0, numFound, watcher, assignment);
    }

    // LOG(debug) << "numFound: "<< numFound << EOM;
    if (numFound == 1)
    {
      Lit lit = this->terms[watcher[0]].lit;
      if (assignment[lit] == State::Unassigned)
      {
        prop.propMaster.enqueue(lit, ClauseReason::aquire(*this, prop));
      }
    }

    if (numFound == 0)
    {
      prop.propMaster.conflict(ClauseReason::aquire(*this, prop));
    }

    if (this->terms.size() >= 2)
    {
      if (numFound == 0)
      {
        watcher[0] = 0;
        watcher[1] = 1;
      }
      else if (numFound == 1)
      {
        if (watcher[0] == 1)
        {
          watcher[0] = 0;
          watcher[1] = 1;
        }
        else
        {
          watcher[1] = 1;
        }
      }
      else if (watcher[0] == 1)
      {
        std::swap(watcher[0], watcher[1]);
      }

      if (initial || numFound > 0)
      {
        for (size_t i = 0; i < 2; i++)
        {
          assert(watcher[i] < terms.size());

          if (initial ||
              (i == 0 && watcher[i] >= 2 && falsifiedLit != Lit::Undef()))
          {
            // was not a watched literal / needs to be readded
            keep = false;
            // assert(watcher[0] < watcher[1] || watcher[1] == 1);
            assert(initial || (terms[i].lit == falsifiedLit));
            std::swap(terms[i], terms[watcher[i]]);
            WatchInfo<Clause> watchInfo;
            watchInfo.ineq = this;
            watchInfo.other = terms[watcher[(i + 1) % 2]].lit;
            prop.watch(terms[i].lit, watchInfo);
          }
        }
      }

      if (!initial && numFound == 2 && watcher[1] >= 2)
      {
        // Let us start the search where we found the second
        // non falsified literal.
        this->propagationSearchStart = watcher[1];
      }
    }
    return keep;
  }
};

inline std::ostream &operator<<(std::ostream &os, const Clause &v)
{
  for (Term<void> term : v.terms)
  {
    os << term << " ";
  };
  os << ">= 1";
  return os;
}

template <typename T>
class IneqPropagator;

template <typename T>
using IneqReason = GenericDBReason<FixedSizeInequality<T>, IneqPropagator<T>>;

template <typename T>
class FixedSizeInequality
{
private:
  uint32_t watchSize = 0;
  bool enoughWatches = false;

  using TElement = Term<T>;
  using TVec = std::conditional_t<std::is_trivially_destructible<T>::value,
                                  InlineVec<TElement>, std::vector<TElement>>;

public:
  T maxCoeff = 0;

  using TTerm = TElement;

  // common constraint interface:
  DBConstraintHeader header;
  T degree;
  TVec terms;

private:
  friend FixedSizeInequalityHandler<T>;
  friend std::hash<FixedSizeInequality<T>>;

  size_t size() const { return terms.size(); }

  FixedSizeInequality(size_t size) : terms(size) {}

  FixedSizeInequality(const FixedSizeInequality<T> &other)
      : terms(other.terms.size())
  {
    using namespace std;
    copy(other.terms.begin(), other.terms.end(), this->terms.begin());
    this->degree = other.degree;
  }

  FixedSizeInequality(std::vector<Term<T>> &_terms, T _degree)
      : degree(_degree), terms(_terms.size())
  {
    std::copy(_terms.begin(), _terms.end(), this->terms.begin());
  }

  template <typename TermIter, typename TInt>
  FixedSizeInequality(size_t size, TermIter begin, TermIter end, TInt _degree)
      : degree(_degree), terms(size)
  {
    std::copy(begin, end, this->terms.begin());
  }

  // FixedSizeInequality(Clause& clause)
  //     : degree(1)
  //     , terms(clause.terms.size())
  // {
  //     std::copy(clause.terms.begin(),clause.terms.end(),
  //     this->terms.begin());
  //     // Term<T> term;
  //     // term.coeff = 1;
  //     // Term<T>* it = this->terms.begin();
  //     // for (Term<void> term: clause.terms) {
  //     //     term.lit = lit;
  //     //     *it = term;
  //     //     ++it;
  //     // }
  // }

  void computeMaxCoeff()
  {
    // We sort here, because we will need a sorted list for
    // computing the watchsize.
    std::sort(terms.begin(), terms.end(), orderByCoeff<Term<T>>);
    maxCoeff = terms[terms.size() - 1].coeff;
  }

  void computeWatchSize()
  {
    if (terms.size() == 0)
    {
      return;
    }

    computeMaxCoeff();

    // we propagate lit[i] if slack < coeff[i] so we
    // need to make sure that if no watch is falsified we
    // have always slack() >= maxCoeff
    T value = -this->degree;

    size_t i = 0;
    for (; i < terms.size(); i++)
    {
      assert(terms[i].coeff <= maxCoeff);
      value += terms[i].coeff;
      if (value >= maxCoeff)
      {
        i++;
        break;
      }
    }

    watchSize = i;
    if (value >= maxCoeff)
    {
      enoughWatches = true;
    }
  }

public:
  /// @brief Get the output proof constraint ID of the constraint that corresponds to `in_id` of the input proof.
  /// @param in_id The input proof constraint ID.
  /// @return If `in_id` has no output proof constraint associated with it, then `0` is returned as an error. Otherwise, the output proof constraint ID for this constraint is returned.
  uint64_t inline getOutId(uint64_t in_id)
  {
    auto out_id = header.in_to_out_id.find(in_id);
    if (out_id == header.in_to_out_id.end())
    {
      return NO_OUTPUT_ID_FOUND;
    }
    else
    {
      return out_id->second;
    }
  }

  void inline setOutId(uint64_t _, uint64_t out_id)
  {
    header.in_to_out_id[getSomeId()] = out_id;
  }

  void inline removeOutId(uint64_t in_id)
  {
    header.in_to_out_id.erase(in_id);
  }

  void inline setSomeId(uint64_t id)
  {
    header.someId = id;
  }

  uint64_t inline getSomeId()
  {
    return header.someId;
  }

  bool isPropagatingAt0()
  {
    if (terms.size() == 0)
    {
      return degree > 0;
    }

    if (maxCoeff == 0)
    {
      computeMaxCoeff();
    }

    T value = -this->degree;

    size_t i = 0;
    for (; i < terms.size(); i++)
    {
      value += terms[i].coeff;
      if (value >= maxCoeff)
      {
        break;
      }
    }

    return value < maxCoeff;
  }

  void clearWatches(IneqPropagator<T> &prop)
  {
    for (size_t i = 0; i < this->watchSize; i++)
    {
      auto &ws = prop.watchlist[terms[i].lit];
      ws.erase(
          std::remove_if(ws.begin(), ws.end(),
                         [this](auto &watch)
                         { return watch.ineq == this; }),
          ws.end());
    }
  }

  void swapWatch(IneqPropagator<T> &prop, size_t &i, size_t &j, bool &keepWatch,
                 Lit &blockingLiteral, Lit &falsifiedLit, bool init)
  {
    const Lit old = terms[i].lit;
    const Lit nw = terms[j].lit;
    if (old != falsifiedLit && !init)
    {
      auto &wl = prop.watchlist[old];
      int found = 0;
      for (size_t i = 0; i < wl.size(); ++i)
      {
        auto &w = wl[i];
        if (w.ineq == this)
        {
          std::swap(w, wl.back());
          wl.pop_back();
          // break;
          found += 1;
        }
      }
      assert(found == 1);
    }
    else
    {
      keepWatch = false;
    }

    std::swap(terms[i], terms[j]);

    WatchInfo<FixedSizeInequality<T>> w;
    w.ineq = this;
    w.other = blockingLiteral;
    prop.watch(nw, w);
  }

  void initWatch(IneqPropagator<T> &prop)
  {
    this->template fixWatch<true>(prop);
  }

  bool updateWatch(IneqPropagator<T> &prop, Lit falsifiedLit = Lit::Undef())
  {
    return this->template fixWatch<false>(prop, falsifiedLit);
  }

  // returns if the watch is kept
  template <bool autoInit>
  bool fixWatch(IneqPropagator<T> &prop, Lit falsifiedLit = Lit::Undef())
  {
    // bool isSat = false;
    bool keepWatch = true;

    if (header.isMarkedForDeletion)
    {
      keepWatch = false;
      return keepWatch;
    }
    // prop.visit += 1;

    // std::cout << "updateWatch: " << *this << std::endl;
    const bool init = autoInit && (watchSize == 0);
    if (init)
    {
      computeWatchSize();
    }
    assert(watchSize != 0 || terms.size() == 0 || degree <= 0);

    // use static variable to avoid reinitialisation for BigInts,
    // prevents parallel or recursive execution!
    static T slack;
    // We will only compute slack if a propagation / conflict can
    // potentially occur, that is if we do not have enough
    // watches, or at least one watch is falsified and can not be
    // replaced. This is intended to improve performance on large
    // coefficients, where computing the slack can be time
    // consuming.
    bool computeSlack = !enoughWatches;
    if (computeSlack)
    {
      slack = -this->degree;
    }

    size_t j = this->watchSize;
    size_t k = this->watchSize;

    const auto &value = prop.propMaster.getAssignment().value;

    // if (this->watchSize == 2 && this->degree == 1) {
    //     if (!init && falsifiedLit != Lit::Undef() && terms[0].lit !=
    //     falsifiedLit && terms[1].lit != falsifiedLit) {
    //         return;
    //     } else  {
    //         for (int i : {0,1}) {
    //             if (value[terms[i].lit] == State::True) {
    //                 WatchInfo<T> w;
    //                 w.ineq = this;
    //                 // w.other = terms[i].lit;
    //                 prop.watch(falsifiedLit, w);
    //                 return;
    //             }
    //         }
    //     }
    // }

    Lit blockingLiteral = Lit::Undef();
    if (terms.size() >= 2)
    {
      if (std::abs(terms[1].coeff) >= degree)
      {
        blockingLiteral = terms[1].lit;
      }
    }

    for (size_t i = 0; i < this->watchSize; i++)
    {
      if (value[terms[i].lit] == State::False)
      {
        if (init)
        {
          for (; k < terms.size(); k++)
          {
            // idea from Jan: initialize watches based on phase
            if (prop.propMaster.getPhase()[terms[k].lit] == State::True &&
                value[terms[k].lit] != State::False)
            {
              swapWatch(prop, i, k, keepWatch, blockingLiteral, falsifiedLit,
                        init);
              if (computeSlack)
              {
                slack += terms[i].coeff;
              }
              k++;
              goto found_new_watch;
            }
          }
        }
        for (; j < terms.size(); j++)
        {
          if (value[terms[j].lit] != State::False)
          {
            swapWatch(prop, i, j, keepWatch, blockingLiteral, falsifiedLit,
                      init);
            if (computeSlack)
            {
              slack += terms[i].coeff;
            }
            j++;
            goto found_new_watch;
          }
        }

        if (!computeSlack)
        {
          computeSlack = true;
          slack = -this->degree;
          for (size_t l = 0; l < i; ++l)
          {
            slack += terms[l].coeff;
          }
        }

        if (init)
        {
          WatchInfo<FixedSizeInequality<T>> w;
          w.ineq = this;
          prop.watch(terms[i].lit, w);
        }

      found_new_watch:;
      }
      else
      {
        if (computeSlack)
        {
          slack += terms[i].coeff;
        }

        if (init)
        {
          WatchInfo<FixedSizeInequality<T>> w;
          w.ineq = this;
          prop.watch(terms[i].lit, w);
        }
      }

      if (blockingLiteral == Lit::Undef() &&
          std::abs(terms[i].coeff) >= degree)
      {
        blockingLiteral = terms[i].lit;
      }
    }

    // for (size_t i = 0; i < size(); i++) {
    //     if (value[terms[i].lit] == State::True) {
    //         isSat = true;
    //         break;
    //     }
    // }

    // if (isSat) {
    //     prop.visit_sat += 1;
    // }

    if (computeSlack)
    {
      if (slack < 0)
      {
        prop.propMaster.conflict(IneqReason<T>::aquire(*this, prop));
        // prop.visit_required += 1;
      }
      else if (slack < maxCoeff)
      {
        for (size_t i = 0; i < this->watchSize; i++)
        {
          if (terms[i].coeff > slack &&
              value[terms[i].lit] == State::Unassigned)
          {
            prop.propMaster.enqueue(terms[i].lit,
                                    IneqReason<T>::aquire(*this, prop));
            // prop.visit_required += 1;
          }
        }
      }
    }

    return keepWatch;
  }
};

template <typename T>
inline std::ostream &operator<<(std::ostream &os,
                                const FixedSizeInequality<T> &v)
{
  for (const Term<T> &term : v.terms)
  {
    os << term << " ";
  };
  os << " >= " << v.degree;
  return os;
}

template <typename T>
inline std::ostream &operator<<(std::ostream &os, const IneqPropagator<T> &v)
{
  os << "IneqPropagator State:\n";
  for (size_t i = 1; i < v.watchlist.size() / 2; ++i)
  {
    Var var(i);
    for (bool sign : {false, true})
    {
      Lit lit1(var, sign);
      if (v.watchlist[lit1].size() == 0)
      {
        continue;
      }
      os << "\t" << lit1 << ":\n";
      for (size_t j = 0; j < v.watchlist[lit1].size(); ++j)
      {
        os << "\t\t" << *v.watchlist[lit1][j].ineq << "\n";
      }
    }
  }
  return os;
}

inline std::ostream &operator<<(std::ostream &os, const ClausePropagator &v)
{
  os << "ClausePropagator State:\n";
  for (size_t i = 1; i < v.watchlist.size() / 2; ++i)
  {
    Var var(i);
    for (bool sign : {false, true})
    {
      Lit lit1(var, sign);
      if (v.watchlist[lit1].size() == 0)
      {
        continue;
      }
      os << "\t" << lit1 << ":\n";
      for (size_t j = 0; j < v.watchlist[lit1].size(); ++j)
      {
        os << "\t\t" << *v.watchlist[lit1][j].ineq << "\n";
      }
    }
  }
  return os;
}

template <typename TConstraint>
class ConstraintHandler
{
private:
  void *malloc(size_t size)
  {
    capacity = size;
    size_t extra =
        extra_size_requirement<typename TConstraint::TVec,
                               typename TConstraint::TElement>::value(size);
    size_t memSize = sizeof(TConstraint) + extra;
    // return std::aligned_alloc(64, memSize);
    return std::malloc(memSize);
  }

public:
  using TStoredConstraint = TConstraint;

  TConstraint *ineq = nullptr;
  size_t capacity = 0;

  void free()
  {
    if (ineq != nullptr)
    {
      ineq->~TConstraint();
      std::free(ineq);
      ineq = nullptr;
    }
  }

  ConstraintHandler(size_t size)
  {
    void *addr = malloc(size);
    ineq = new (addr) TConstraint(size);
  }

  ConstraintHandler(ConstraintHandler<TConstraint> &&other) noexcept
  {
    ineq = other.ineq;
    other.ineq = nullptr;
  }

  ConstraintHandler(const TConstraint &copyFrom)
  {
    void *addr = malloc(copyFrom.terms.size());
    ineq = new (addr) TConstraint(copyFrom);
  }

  ConstraintHandler(const ConstraintHandler<TConstraint> &other)
      : ConstraintHandler(*other.ineq) {}
  ConstraintHandler() {}

  TConstraint &operator*() { return *ineq; }

  TConstraint *operator->() { return ineq; }

  ConstraintHandler &operator=(ConstraintHandler &&other)
  {
    this->free();
    ineq = other.ineq;
    other.ineq = nullptr;
    return *this;
  }

  template <typename... Args>
  ConstraintHandler(size_t size, Args &&...args)
  {
    void *addr = malloc(size);
    ineq = new (addr) TConstraint(std::forward<Args>(args)...);
  }

  template <typename... Args>
  void replace_new(size_t size, Args &&...args)
  {
    void *addr;
    if (capacity < size || ineq == nullptr)
    {
      size = std::max(2 * capacity, size);
      free();
      addr = malloc(size);
    }
    else
    {
      ineq->~TConstraint();
      addr = ineq;
    }

    ineq = new (addr) TConstraint(std::forward<Args>(args)...);
  }

  // ConstraintHandler(std::vector<Term<T>>& terms, T degree) {
  //     void* addr = malloc(terms.size());
  //     ineq = new (addr) TConstraint(terms, degree);
  // }

  // void resize(size_t size) {
  //     if (ineq != nullptr && size == capacity) {
  //         return;
  //     }
  //     free();
  //     void* addr = malloc(size);
  //     ineq = new (addr) TConstraint(size);
  // }

  ~ConstraintHandler() { free(); }
};

namespace InplaceIneqOps
{
  /* This contains a set of methods that can operate on classes that have a
  member term of any Term<> type and a member degree of an integer type.

  When adding special treatment for some types try to use
  overloading instead of template specialization as g++ does not
  allow partial function specialization.
  */

  struct saturate
  {
    bool operator()(Clause &ineq) { return true; }

    /* returns true if the resulting constraint is normalized */
    template <typename TIneq>
    bool operator()(TIneq &ineq)
    {
      for (auto &term : ineq.terms)
      {
        using namespace std;
        term.coeff = min(term.coeff, ineq.degree);
      }
      return ineq.degree > 0 || ineq.terms.size() == 0;
    }
  };

  struct divide
  {
    template <typename TInt>
    void operator()(Clause &ineq, TInt divisor) {}

    template <typename TIneq, typename TInt>
    void operator()(TIneq &ineq, TInt divisor)
    {
      ineq.degree = divideAndRoundUp(ineq.degree, divisor);
      for (auto &term : ineq.terms)
      {
        term.coeff = divideAndRoundUp(term.coeff, divisor);
      }
    }
  };

  struct negate
  {
    void operator()(Clause &ineq)
    {
      _assert_(false && "Clause can not be negated inplace.");
    }

    template <typename TIneq>
    void operator()(TIneq &ineq)
    {
      // static_assert(!std::is_same<TIneq, Clause>(), "Can not do inplace
      // negation with clauses!");

      ineq.degree = -ineq.degree + 1;
      for (auto &term : ineq.terms)
      {
        ineq.degree += term.coeff;
        term.lit = ~term.lit;
      }
    }
  };

  struct isContradiction
  {
    bool operator()(Clause &ineq) { return ineq.terms.size() == 0; }

    template <typename TIneq>
    bool operator()(TIneq &ineq)
    {
      using TInt = decltype(ineq.degree);
      TInt slack = -ineq.degree;
      for (auto &term : ineq.terms)
      {
        slack += term.coeff;
        if (slack >= 0)
        {
          return false;
        }
      }
      return (slack < 0);
    }
  };

  struct isTrivial
  {
    template <typename TIneq>
    bool operator()(TIneq &ineq)
    {
      return ineq.degree <= 0;
    }
  };

  struct equals
  {
    template <typename TIneqA, typename TIneqB>
    bool operator()(TIneqA &ineqA, TIneqB &ineqB)
    {
      using TermA = typename TIneqA::TTerm;
      using TermB = typename TIneqB::TTerm;

      if ((ineqA.terms.size() != ineqB.terms.size()) ||
          (ineqA.degree != ineqB.degree))
      {
        return false;
      }

      std::vector<TermA> mine(ineqA.terms.begin(), ineqA.terms.end());
      sort(mine.begin(), mine.end(), orderByVar<TermA>);
      std::vector<TermB> theirs(ineqB.terms.begin(), ineqB.terms.end());
      sort(theirs.begin(), theirs.end(), orderByVar<TermB>);

      return std::equal(mine.begin(), mine.end(), theirs.begin());
    }
  };

  struct hash
  {
    template <typename TIneq>
    std::size_t operator()(TIneq &ineq)
    {
      using TTerm = typename TIneq::TTerm;
      std::vector<TTerm> mine(ineq.terms.begin(), ineq.terms.end());
      sort(mine.begin(), mine.end(), orderByVar<TTerm>);

      std::size_t seed = 0;

      for (const TTerm &term : mine)
      {
        hash_combine(seed, term.coeff);
        hash_combine(seed, term.lit);
      }

      hash_combine(seed, ineq.degree);
      return seed;
    }
  };

  struct implies
  {
    /// @brief Check if constraint `a` syntactically implies constraint `b`.
    /// @tparam TIneqA Type of constraint `a`.
    /// @tparam TIneqB Type of constraint `b`.
    /// @param a Constraint `a` is the constraint that `b` is implied from.
    /// @param b Constraint `b` that is checked to be implied.
    /// @return True if `a` syntactically implies `b`.
    template <typename TIneqA, typename TIneqB>
    bool operator()(TIneqA &a, TIneqB &b)
    {
      using TTerm = typename TIneqB::TTerm;
      // Create efficient lookup for constraint `b`
      std::unordered_map<size_t, TTerm> lookup;
      for (auto &term : b.terms)
      {
        lookup.insert(std::make_pair(term.lit.var(), term));
      }

      // IntSumSafe weakenCost = 0;
      BigInt weakenCost = 0;
      for (auto &term : a.terms)
      {
        using namespace std;
        size_t var = term.lit.var();

        auto search = lookup.find(var);
        if (search == lookup.end())
        {
          // Literal from `a` not in `b`; increment cost by coefficient of `a`, as this has to be weakened
          weakenCost += term.coeff;
        }
        else
        {
          // Literal from `a` in `b`
          auto theirs = search->second;
          if (term.lit != theirs.lit)
          {
            // Literal in `a` is negated in `b`; increment cost by coefficient of `a`, as this has to be weakened
            weakenCost += term.coeff;
          }
          else if (term.coeff > theirs.coeff)
          {
            // Literal in `a` is in `b`; increment cost by difference between coefficient in `a` and `b`, as the coefficient in `a` has to be weakened to the coefficient of `b`
            weakenCost += term.coeff;
            weakenCost -= theirs.coeff;
          }
        }
      }
      // The difference between the degree of `a` and `b` defines the budget.
      weakenCost -= a.degree;
      weakenCost += b.degree;

      return weakenCost <= 0;
    }
  };

  struct implies_strong
  {
    /// @brief Check if constraint `a` syntactically implies constraint `b`. This function uses a stronger implication check that is not supported in the kernel format.
    /// @tparam TIneqA Type of constraint `a`.
    /// @tparam TIneqB Type of constraint `b`.
    /// @param a Constraint `a` is the constraint that `b` is implied from.
    /// @param b Constraint `b` that is checked to be implied.
    /// @param proof Proof context to translate stronger implication check to kernel format.
    /// @return True if `a` syntactically implies `b`.
    template <typename TIneqA, typename TIneqB>
    bool operator()(TIneqA &a, TIneqB &b, ProofContext &proof)
    {
      using TTerm = typename TIneqB::TTerm;
      // Create efficient lookup for constraint `b`
      std::unordered_map<size_t, TTerm> lookup;
      for (auto &term : b.terms)
      {
        lookup.insert(std::make_pair(term.lit.var(), term));
      }

      // IntSumSafe weakenCost = 0;
      BigInt weakenCost = 0;
      for (auto &term : a.terms)
      {
        using namespace std;
        Var var = term.lit.var();

        auto search = lookup.find(static_cast<size_t>(var));
        if (search == lookup.end())
        {
          // Literal from `a` not in `b`; increment cost by coefficient of `a`, as this has to be weakened
          weakenCost += term.coeff;
          if (proof.is_logging_proof)
          {
            proof.buffer << " " << proof.varNameManager->getName(var) << " w";
          }
        }
        else
        {
          // Literal from `a` in `b`
          auto theirs = search->second;
          if (term.lit != theirs.lit)
          {
            // Literal in `a` is negated in `b`; increment cost by coefficient of `a`, as this has to be weakened.
            weakenCost += term.coeff;
            // Proof log that literal is weakened and added to `a` with coefficient in `b`.
            if (proof.is_logging_proof)
            {
              proof.buffer << " ";
              if (!term.lit.isNegated())
              {
                proof.buffer << "~";
              }
              proof.buffer << proof.varNameManager->getName(var) << " * +";
            }
          }
          else if (term.coeff > theirs.coeff)
          {
            // Literal in `a` is in `b`
            if (theirs.coeff < b.degree)
            {
              // This is the stronger check: Only weaken if target coefficient is not saturated.
              // The idea is that after all the literals in `a` are weakened, the degree should be equal to the target degree in `b`, hence if the coefficient of the literal in `b` is at least the degree of `b`, then saturation can be applied after weakening of `a` to get to the right coefficient.
              // Otherwise, the coefficient of the literal in `a` needs to be weakened to the coefficient in `b`.
              BigInt diff = term.coeff - theirs.coeff;
              weakenCost += diff;
              // Proof log the partial weakening.
              if (proof.is_logging_proof)
              {
                proof.buffer << " ";
                if (!term.lit.isNegated())
                {
                  proof.buffer << "~";
                }
                proof.buffer << proof.varNameManager->getName(var) << " " << diff << " * +";
              }
            }
          }
          else if (term.coeff < theirs.coeff && proof.is_logging_proof)
          {
            // Add literal to constraint `a` with difference between coefficient in `a` and `b`.
            BigInt diff;

            if (theirs.coeff > b.degree)
            {
              if (term.coeff >= b.degree)
                continue;
              diff = b.degree - term.coeff;
            }
            else
              diff = theirs.coeff - term.coeff;

            proof.buffer
                << " ";
            if (term.lit.isNegated())
            {
              proof.buffer << "~";
            }
            proof.buffer << proof.varNameManager->getName(var) << " " << diff << " * +";
          }
          lookup.erase(static_cast<size_t>(var));
        }
      }
      // The difference between the degree of `a` and `b` defines the budget.
      weakenCost -= a.degree;
      weakenCost += b.degree;

      // Get RHS of constraint to the correct value by adding a variable and its negation with the difference between the current and the correct RHS, which is `weakenCost`.
      if (proof.is_logging_proof)
      {
        if (weakenCost != 0)
        {
          if (lookup.begin() != lookup.end())
          {
            proof.buffer << " " << proof.varNameManager->getName(static_cast<Var>(lookup.begin()->first)) << " " << -weakenCost << " * + ~" << proof.varNameManager->getName(static_cast<Var>(lookup.begin()->first)) << " " << -weakenCost << " * +";
          }
        }
        bool saturated = false;
        for (TTerm term : b.terms)
        {
          if (term.coeff > b.degree && lookup.find(static_cast<size_t>(term.lit.var())) == lookup.end())
          {
            if (!saturated)
            {
              saturated = true;
              proof.buffer << " s";
            }
            proof.buffer << " ";
            if (term.lit.isNegated())
              proof.buffer << "~";
            proof.buffer << proof.varNameManager->getName(term.lit.var()) << " " << term.coeff - b.degree << " * +";
          }
        }
        for (auto const &key_value_pair : lookup)
        {
          TTerm term = key_value_pair.second;
          proof.buffer << " ";
          if (term.lit.isNegated())
            proof.buffer << "~";
          proof.buffer << proof.varNameManager->getName(term.lit.var()) << " " << term.coeff << " * +";
        }
      }

      return weakenCost <= 0;
    }
  };

  template <typename T>
  struct compute_slack
  {
    /// @brief Compute the slack of the constraint `constraint`.
    /// @tparam TIneq Type of `constraint`.
    /// @tparam T Type for coefficients and slack.
    /// @param constraint A constraint.
    /// @param assignment (Partial) assignment with respect to which the slack is computed.
    /// @return The slack of `constraint`.
    template <typename TIneq>
    T operator()(TIneq &constraint, Assignment &assignment)
    {
      // initialize slack
      T slack = -constraint.degree;

      for (auto const &term : constraint.terms)
      {
        // Add coefficient of non-falsified literals to slack.
        if (assignment[term.lit] != State::False)
        {
          slack += term.coeff;
        }
      }

      return slack;
    }
  };

  template <typename T>
  struct get_max_coeff
  {
    template <typename TIneq>
    T operator()(TIneq &ineq)
    {
      T maxCoeff = 0;

      for (auto &term : ineq.terms)
      {
        if (term.coeff > maxCoeff)
        {
          maxCoeff = term.coeff;
        }
      }

      return maxCoeff;
    }
  };

  template <typename T>
  struct propagate_constraint
  {
    /// @brief Compute the slack of the constraint `constraint`.
    /// @tparam TIneq Type of `constraint`.
    /// @tparam T Type for coefficients and slack.
    /// @param constraint A constraint.
    /// @param assignment (Partial) assignment with respect to which the slack is computed.
    /// @param slack Slack of the current constraint under the assignment.
    /// @return The slack of `constraint`.
    template <typename TIneq>
    bool operator()(TIneq &constraint, Assignment &assignment, T &slack)
    {
      bool did_propagate = false;
      for (auto const &term : constraint.terms)
      {
        // Propagate literal if coefficient smaller slack and variable unassigned in assignment.
        if (slack < term.coeff && assignment[term.lit] == State::Unassigned)
        {
          assignment.assign(term.lit);
          did_propagate = true;
        }
      }
      return did_propagate;
    }
  };

  struct substitute
  {
    template <typename TIneq>
    void operator()(TIneq &ineq, Substitution &sub)
    {
      // warning: after this operation the constraint is no longer
      // normalized, load to FatInequality to normalize
      for (auto &term : ineq.terms)
      {
        auto it = sub.map.find(term.lit);
        if (it != sub.map.end())
        {
          term.lit = it->second;
        }
      }
    }
  };

  struct print
  {
    template <typename TIneq>
    std::ostream &operator()(TIneq &ineq, std::function<std::string(int)> varName,
                             std::ostream &out)
    {
      std::vector<size_t> idx(ineq.terms.size());
      std::iota(idx.begin(), idx.end(), 0);

      std::vector<std::string> names;
      names.reserve(ineq.terms.size());

      for (auto &term : ineq.terms)
      {
        names.push_back(varName(term.lit.var()));
      }

      std::sort(idx.begin(), idx.end(), [&names](size_t i1, size_t i2)
                { return numLexOrder()(names[i1], names[i2]); });

      for (size_t i = 0; i < idx.size(); ++i)
      {
        auto &term = ineq.terms[idx[i]];
        out << term.coeff << " ";
        if (term.lit.isNegated())
        {
          out << "~";
        }
        assert(names[idx[i]] == varName(term.lit.var()));
        out << names[idx[i]] << " ";
      }
      out << ">= " << ineq.degree;
      return out;
    }
  };

  template <typename TargetTIneq>
  struct makeHandler
  {
    template <typename TIneq>
    ConstraintHandler<TargetTIneq> operator()(TIneq &ineq)
    {
      return ConstraintHandler<TargetTIneq>(ineq.terms.size(), ineq);
    }
  };

} // namespace InplaceIneqOps

template <typename T>
class IneqPropagator : public Propagator
{
public:
  typedef WatchInfo<FixedSizeInequality<T>> WatchedType;
  typedef std::vector<WatchedType> WatchList;

  LitIndexedVec<WatchList> watchlist;

  IneqPropagator(PropagationMaster &_propMaster, size_t _nVars)
      : Propagator(_propMaster), watchlist(2 * (_nVars + 1))
  {
    // for (auto& ws: watchlist) {
    //     ws.reserve(50);
    // }
  }

  void clear()
  {
    reset(0);
    for (WatchList &wl : watchlist)
    {
      wl.clear();
    }
  }

  void watch(Lit lit, WatchedType &w) { watchlist[lit].push_back(w); }

  size_t get_mem_usage()
  {
    size_t result = 0;
    for (auto &wl : watchlist)
    {
      result += sizeof(WatchedType) * wl.capacity();
    }

    return result + sizeof(WatchList) * watchlist.capacity();
  }

  virtual void increaseNumVarsTo(size_t _nVars)
  {
    watchlist.resize(2 * (_nVars + 1));
  }

  virtual void propagate()
  {
    const auto &trail = propMaster.getTrail();
    // std::cout << "IneqPropagator: propagating from: " << qhead << std::endl;
    // std::cout << *this << std::endl;
    while (qhead < trail.size() && !propMaster.isConflicting())
    {
      Lit falsifiedLit = ~trail[qhead];
      // std::cout << "propagating: " << trail[qhead] << std::endl;

      WatchList &ws = watchlist[falsifiedLit];

      const WatchedType *end = ws.data() + ws.size();
      WatchedType *sat = ws.data();
      // WatchedType* it  = ws.data();
      // for (; it != end; it++) {
      //     if (assignment.value[it->other] == State::True) {
      //         std::swap(*it, *sat);
      //         ++sat;
      //     }
      // }

      WatchedType *next = &(*sat);
      WatchedType *kept = next;

      const uint lookAhead = 3;
      for (; next != end && !propMaster.isConflicting(); next++)
      {
        auto fetch = next + lookAhead;
        if (fetch < end)
        {
          __builtin_prefetch(fetch->ineq);
        }
        assert(next->other != Lit::Undef() ||
               propMaster.getAssignment().value[next->other] ==
                   State::Unassigned);

        bool keepWatch = true;
        if (propMaster.getAssignment().value[next->other] != State::True)
        {
          assert(next->ineq->header.isMarkedForDeletion == false);
          keepWatch = next->ineq->updateWatch(*this, falsifiedLit);
        }
        if (keepWatch)
        {
          *kept = *next;
          kept += 1;
        }
      }

      // in case of conflict copy remaining watches
      for (; next != end; next++)
      {
        *kept = *next;
        kept += 1;
      }

      ws.erase(ws.begin() + (kept - ws.data()), ws.end());

      qhead += 1;
    }
  }
};

/*
 * Automatically resets the trail to its previous state after the
 * scope is left. Requires that only new things were put on the
 * trail, i.e., the trail did not grow shorter.
 */
class AutoReset
{
private:
  PropagationMaster &engine;
  PropState base;

public:
  AutoReset(PropagationMaster &engine_)
      : engine(engine_), base(engine.current)
  {
    engine.isTemporary = true;
  }

  ~AutoReset()
  {
    engine.reset(base);
    engine.isTemporary = false;
  }
};

template <typename T>
class PropagatorGroup
{
private:
  std::array<std::list<Inequality<T> *>, 4> lists;
  bool _isActive = false;

public:
  PropagationMaster &propMaster;
  std::vector<Inequality<T> *> propagatingAt0;

  enum class State
  {
    unhandled,
    unattached,
    unregistered,
    handled
  };

  std::list<Inequality<T> *> &get(State state)
  {
    return lists[static_cast<int>(state)];
  }

  typedef std::unordered_set<Inequality<T> *> OccursList;
  LitIndexedVec<OccursList> occurs;

  IneqPropagator<T> ineqPropagator;
  IneqPropagator<int32_t> ineq32Propagator;
  ClausePropagator clausePropagator;

  PropagatorGroup(PropagationMaster &_propMaster, size_t _nVars)
      : propMaster(_propMaster), occurs(2 * (_nVars + 1)),
        ineqPropagator(_propMaster, _nVars),
        ineq32Propagator(_propMaster, _nVars),
        clausePropagator(_propMaster, _nVars)

  {
  }

  void clear()
  {
    propagatingAt0.clear();

    for (auto &list : lists)
    {
      list.clear();
    }

    for (OccursList &ol : occurs)
    {
      ol.clear();
    }

    clausePropagator.clear();
    ineq32Propagator.clear();
    ineqPropagator.clear();
  }

  size_t get_mem_usage()
  {
    // memory of propagators is counted in propmaster
    return sizeof(OccursList) * occurs.capacity();
  }

  void increaseNumVarsTo(size_t nVars) { occurs.resize(2 * (nVars + 1)); }

  bool isActive() { return _isActive; }

  void activatePropagators()
  {
    if (!_isActive)
    {
      _isActive = true;
      propMaster.activatePropagator(clausePropagator);
      propMaster.activatePropagator(ineq32Propagator);
      propMaster.activatePropagator(ineqPropagator);
    }
  }

  void deactivatePropagators()
  {
    if (_isActive)
    {
      _isActive = false;
      propMaster.deactivatePropagator(clausePropagator);
      propMaster.deactivatePropagator(ineq32Propagator);
      propMaster.deactivatePropagator(ineqPropagator);
    }
  }

  void doPropagationsAt0()
  {
    for (Inequality<T> *ineq : this->propagatingAt0)
    {
      ineq->updateWatch(*this);
    }
  }

  /// @brief Attach constraints that need to be attached since the last attaching of constraints.
  /// @return True if propagation after attaching the constraints is required.
  bool attachUnattached()
  {
    bool propagationRequired = false;
    for (State state : {State::unhandled, State::unattached})
    {
      for (Inequality<T> *ineq : get(state))
      {
        ineq->wasAttached = true;
        ineq->initWatch(*this);
        ineq->_groupState =
            (state == State::unhandled) ? State::unregistered : State::handled;

        if (ineq->isPropagatingAt0())
        {
          // We want to avoid duplicates in the `propagationAt0` vector. Only add constraint to `propagatingAt0` if they are not added yet.
          auto it = std::find(this->propagatingAt0.rbegin(), this->propagatingAt0.rend(), ineq);
          if (it == this->propagatingAt0.rend())
          {
            this->propagatingAt0.push_back(ineq);
            // We need to do the propagations at level 0.
            propagationRequired = true;
          }
        }
      }
    }

    get(State::unregistered)
        .splice(get(State::unregistered).end(), get(State::unhandled));
    get(State::handled)
        .splice(get(State::handled).end(), get(State::unattached));

    return propagationRequired;
  }

  void registerOccurences()
  {
    for (State state : {State::unhandled, State::unregistered})
    {
      for (Inequality<T> *ineq : get(state))
      {
        ineq->registerOccurence(*this);
        ineq->_groupState =
            (state == State::unhandled) ? State::unattached : State::handled;
      }
    }

    get(State::unattached)
        .splice(get(State::unattached).end(), get(State::unhandled));
    get(State::handled)
        .splice(get(State::handled).end(), get(State::unregistered));
  }

  void add(Inequality<T> &ineq)
  {
    get(State::unhandled).push_front(&ineq);
    ineq._groupIter = get(State::unhandled).begin();
    ineq._groupState = State::unhandled;
  }

  void remove(Inequality<T> &ineq)
  {
    if (ineq.isPropagatingAt0())
    {
      auto it =
          std::find(propagatingAt0.rbegin(), propagatingAt0.rend(), &ineq);
      if (it != propagatingAt0.rend())
      {
        std::swap(*it, propagatingAt0.back());
        propagatingAt0.pop_back();
      }
    }

    get(ineq._groupState).erase(ineq._groupIter);
    if (ineq._groupState == State::unregistered ||
        ineq._groupState == State::handled)
    {
      // we need to clear the watches now, a lazy removal is not
      // possible, because the constraint is not necessarily
      // deleted, but may be reattached to a different set.
      ineq.clearWatches(*this);
    }

    ineq.unRegisterOccurence(*this);
  }

  std::unordered_set<Inequality<T> *> computeEffected(Substitution &sub)
  {
    registerOccurences();
    std::unordered_set<Inequality<T> *> unique;
    for (auto it : sub.map)
    {
      Lit from = it.first;
      Lit to = it.second;

      // constraints are normalized, if a literal is set to
      // one then this is the same as weakening, hence we do
      // not need to add it to the set of effected
      // constraints
      if (to != Substitution::one())
      {
        unique.insert(occurs[from].begin(), occurs[to].end());
      }
    }
    return unique;
  }

  /// @brief Check if the variable `var` occurs in a constraint in the database.
  /// @param var Variable to check.
  /// @return Return `true` iff variable occurs in a constraint in the database.
  inline bool isVarOccurring(uint var)
  {
    Lit lit = Lit(var);
    return occurs[lit].size() > 0 ||
           occurs[lit.negated()].size() > 0;
  }

  struct initWatch
  {
    void operator()(FixedSizeInequality<T> &constraint,
                    PropagatorGroup<T> &engine)
    {
      constraint.initWatch(engine.ineqPropagator);
    }

    void operator()(FixedSizeInequality<int32_t> &constraint,
                    PropagatorGroup<T> &engine)
    {
      constraint.initWatch(engine.ineq32Propagator);
    }

    void operator()(Clause &constraint, PropagatorGroup<T> &engine)
    {
      constraint.initWatch(engine.clausePropagator);
    }
  };

  struct clearWatch
  {
    void operator()(FixedSizeInequality<T> &constraint,
                    PropagatorGroup<T> &engine)
    {
      constraint.clearWatches(engine.ineqPropagator);
    }

    void operator()(FixedSizeInequality<int32_t> &constraint,
                    PropagatorGroup<T> &engine)
    {
      constraint.clearWatches(engine.ineq32Propagator);
    }

    void operator()(Clause &constraint, PropagatorGroup<T> &engine)
    {
      constraint.clearWatches(engine.clausePropagator);
    }
  };

  struct updateWatches
  {
    void operator()(FixedSizeInequality<T> &constraint,
                    PropagatorGroup<T> &engine)
    {
      constraint.updateWatch(engine.ineqPropagator);
    }

    void operator()(FixedSizeInequality<int32_t> &constraint,
                    PropagatorGroup<T> &engine)
    {
      constraint.updateWatch(engine.ineq32Propagator);
    }

    void operator()(Clause &constraint, PropagatorGroup<T> &engine)
    {
      constraint.updateWatch(engine.clausePropagator);
    }
  };

  struct addOccurence
  {
    template <typename TIneq>
    void operator()(TIneq &ineq, PropagatorGroup<T> &engine, Inequality<T> &w)
    {
      for (auto &term : ineq.terms)
      {
        engine.occurs[term.lit].emplace(&w);
      }
    }
  };

  struct rmOccurence
  {
    template <typename TIneq>
    void operator()(TIneq &ineq, PropagatorGroup<T> &engine, Inequality<T> &w)
    {
      for (auto &term : ineq.terms)
      {
        engine.occurs[term.lit].erase(&w);
      }
    }
  };
};

template <typename T>
class PropEngine
{
private:
  typedef Inequality<T> Ineq;
  std::unordered_set<Ineq *, PointedHash<Ineq>, PointedEq<Ineq>>
      constraintLookup;

  size_t nVars;
  bool updateWatch = true;
  size_t dbMem = 0;
  size_t cumDbMem = 0;
  size_t maxDbMem = 0;

  PropagationMaster propMaster;
  // we want to keep an instance of the tmpPropagator to avoid rapid
  // reallocation
  IneqPropagator<T> tmpPropagator;
  // we want to keep an instance of the negated constraint to avoid
  // rapid reallocation
  FixedSizeInequalityHandler<T> negated;
  bool hasDetached = false;

public:
  PropagatorGroup<T> core;
  PropagatorGroup<T> derived;

  std::chrono::duration<double> timeEffected = std::chrono::seconds(0);
  std::chrono::duration<double> timeFind = std::chrono::seconds(0);
  std::chrono::duration<double> timeInitProp = std::chrono::seconds(0);
  std::chrono::duration<double> timePropagate = std::chrono::seconds(0);
  std::chrono::duration<double> timeRUP = std::chrono::seconds(0);

  long long visit = 0;
  long long visit_sat = 0;
  long long visit_required = 0;
  long long lookup_requests = 0;

  PropEngine(size_t _nVars)
      : nVars(_nVars), propMaster(_nVars), tmpPropagator(propMaster, _nVars),
        core(propMaster, _nVars), derived(propMaster, _nVars), timeEffected(0),
        timeFind(0), timeInitProp(0), timePropagate(0), timeRUP(0)
  {
    derived.activatePropagators();
    core.activatePropagators();
  }

  size_t get_mem_usage()
  {
    return core.get_mem_usage() + derived.get_mem_usage() +
           propMaster.get_mem_usage();
  }

  void printStats()
  {
    std::cout << "c statistic: used database memory: " << std::fixed
              << std::setprecision(3)
              << static_cast<float>(dbMem) / 1024 / 1024 / 1024 << " GB"
              << std::endl;

    std::cout << "c statistic: cumulative database memory: " << std::fixed
              << std::setprecision(3)
              << static_cast<float>(cumDbMem) / 1024 / 1024 / 1024 << " GB"
              << std::endl;

    std::cout << "c statistic: maximal used database memory: " << std::fixed
              << std::setprecision(3)
              << static_cast<float>(maxDbMem) / 1024 / 1024 / 1024 << " GB"
              << std::endl;

    std::cout << "c statistic: number variables:" << nVars << std::endl;
    std::cout << "c statistic: variable memory use estimate:"
              << static_cast<float>(nVars) * 324 / 1024 / 1024 / 1024 << " GB"
              << std::endl;
    std::cout << "c statistic: variable memory use:"
              << static_cast<float>(get_mem_usage()) / 1024 / 1024 / 1024
              << " GB" << std::endl;

    std::cout << "c statistic: visit: " << visit << std::endl;
    std::cout << "c statistic: visit_sat: " << visit_sat << std::endl;
    std::cout << "c statistic: visit_required: " << visit_required << std::endl;

    std::cout << "c statistic: time effected: " << std::fixed
              << std::setprecision(2) << timeEffected.count() << std::endl;

    std::cout << "c statistic: time find: " << std::fixed
              << std::setprecision(2) << timeFind.count() << std::endl;

    std::cout << "c statistic: time initpropagation: " << std::fixed
              << std::setprecision(2) << timeInitProp.count() << std::endl;

    std::cout << "c statistic: time cleanupTrail: " << std::fixed
              << std::setprecision(2) << propMaster.timeCleanupTrail.count()
              << std::endl;

    std::cout << "c statistic: time propagate: " << std::fixed
              << std::setprecision(2) << timePropagate.count() << std::endl;

    std::cout << "c statistic: time rup: " << std::fixed << std::setprecision(2)
              << timeRUP.count() << std::endl;

    std::cout << "c statistic: hashColisions: " << hashColision << std::endl;
    std::cout << "c statistic: lookup_requests: " << lookup_requests
              << std::endl;
  }

  void increaseNumVarsTo(size_t _nVars)
  {
    assert(nVars <= _nVars);
    if (nVars < _nVars)
    {
      this->nVars = _nVars;
      propMaster.increaseNumVarsTo(_nVars);
      core.increaseNumVarsTo(_nVars);
      derived.increaseNumVarsTo(_nVars);
    }
  }

  void propagate()
  {
    Timer timer(timePropagate);
    propMaster.propagate();
  }

  std::pair<std::vector<int>, std::vector<int>>
  propagate4sat(std::vector<int> &lits)
  {
    // Update the occurrence lists with recent additions and deletions.
    core.registerOccurences();
    derived.registerOccurences();

    // Set up data structures to return.
    std::vector<int> propagated;
    std::vector<int> missing;

    for (int lit : lits)
    {
      Lit l(lit);
      auto val = propMaster.getAssignment().value[l];

      if (val == State::Unassigned)
      {
        propMaster.enqueue(l, nullptr);
      }
      else if (val == State::False)
      {
        propMaster.conflict(nullptr);
        propMaster.setConflictingSolutionLit(l);
        missing.push_back(0);
        return std::make_pair<>(lits, missing);
      }
    }

    // Check if we already have a complete assignment.
    bool all_assigned = true;
    for (uint var = 1; var <= nVars; var++)
    {
      auto val = propMaster.getAssignment().value[Lit(var)];
      if (val == State::Unassigned)
      {
        // Only report variable as missing when it occurs in constraints.
        if (isVarOccurring(var))
        {
          all_assigned = false;
          break;
        }
      }
      else if (val == State::True)
      {
        propagated.push_back(var);
      }
      else
      {
        propagated.push_back(-var);
      }
    }
    if (all_assigned)
    {
      bool all_satisfied = true;
      for (Ineq *constraint : constraintLookup)
      {
        if (!constraint->isSAT(propMaster.getAssignment()))
        {
          all_satisfied = false;
          break;
        }
      }
      if (all_satisfied)
      {
        return std::make_pair<>(propagated, missing);
      }
    }

    propagate();

    propagated.clear();

    if (propMaster.isConflicting())
    {
      missing.push_back(0);
    }

    for (uint var = 1; var <= nVars; var++)
    {
      auto val = propMaster.getAssignment().value[Lit(var)];
      if (val == State::Unassigned)
      {
        // Only report variable as missing when it occurs in constraints.
        if (isVarOccurring(var))
        {
          missing.push_back(var);
        }
      }
      else if (val == State::True)
      {
        propagated.push_back(var);
      }
      else
      {
        propagated.push_back(-var);
      }
    }

    return std::make_pair<>(propagated, missing);
  }

  std::pair<std::vector<int>, std::vector<int>>
  checkSat(std::vector<int> &lits)
  {
    initPropagation();
    propagate();
    AutoReset reset(this->propMaster);
    return propagate4sat(lits);
  }

  void printSolutionPropagationTrace(std::vector<int> &lits, VariableNameManager &var_name_manager, bool use_color)
  {
    initPropagation();
    propagate();
    propagate4sat(lits);

    auto propagatedLits = propMaster.getTrail();
    auto itPropagatedLit = propagatedLits.begin();
    auto propagationReasons = propMaster.getReasons();
    auto itReason = propagationReasons->begin();
    auto conflictReason = &propMaster.getConflictReasons();

    std::vector<std::pair<int, uint64_t>> trace;
    while (itPropagatedLit != propagatedLits.end() && itReason != propagationReasons->end())
    {
      // Print propagated literal
      if (use_color)
        std::cout << "\u001b[95;1m";

      if (itPropagatedLit->isNegated())
        std::cout << "    ~";
      else
        std::cout << "    ";

      std::cout << var_name_manager.getName(itPropagatedLit->var());
      if (use_color)
        std::cout << "\u001b[0m";

      std::cout << "  (";

      if ((*itReason) == nullptr)
      {
        // The literal is added to the trail by the solution.
        if (use_color)
          std::cout << "\u001b[33;1m";
        std::cout << "by given solution";
      }
      else
      {
        // Print constraint
        if (use_color)
          std::cout << "\u001b[34;1m";

        auto terms = (*itReason)->getTerms();
        printConstraint(var_name_manager, terms, (*itReason)->getDegree());
      }
      if (use_color)
        std::cout << "\u001b[0m";

      std::cout << ")";
      std::cout << "\n";
      ++itPropagatedLit;
      ++itReason;
    }

    if ((*conflictReason) == nullptr)
    {
      if (use_color)
        std::cout << "\u001b[31;1m";

      std::cout << "current propagation conflicts with solution: ";
      if (use_color)
        std::cout << "\u001b[0m";

      if (use_color)
        std::cout << "\u001b[34;1m";

      auto lit = propMaster.getConflictingSolutionLit();
      if (lit.isNegated())
        std::cout << "~";

      std::cout << var_name_manager.getName(lit.var());
      if (use_color)
        std::cout << "\u001b[0m";
    }
    else
    {
      if (use_color)
        std::cout << "\u001b[31;1m";

      std::cout << "conflict constraint: ";
      if (use_color)
        std::cout << "\u001b[0m";

      // Print constraint
      if (use_color)
        std::cout << "\u001b[34;1m";

      auto terms = (*conflictReason)->getTerms();
      printConstraint(var_name_manager, terms, (*conflictReason)->getDegree());
      if (use_color)
        std::cout << "\u001b[0m";
    }

    std::cout << "\n"
              << std::endl;
  }

  Inequality<T> *attach(Inequality<T> *toAttach, uint64_t id)
  {
    Inequality<T> *ineq;
    toAttach->contract();
    {
      Timer timer(timeFind);
      ineq = *constraintLookup.insert(toAttach).first;
      lookup_requests += 1;
    }

    ineq->attachCount += 1;
    ineq->derived_ids.insert(id);

    if (!ineq->isAttached)
    {
      ineq->isAttached = true;
      ineq->freeze();
      dbMem += ineq->mem();
      cumDbMem += ineq->mem();
      maxDbMem = std::max(dbMem, maxDbMem);

      // All constraints are attached to derived by default.
      derived.add(*ineq);
    }

    // If `someId` is not set, set it.
    if (ineq->getSomeId() == DEFAULT_CONSTRAINT_ID)
    {
      ineq->setSomeId(id);
    }
    return ineq;
  }

  void moveAllToCore()
  {
    using State = typename PropagatorGroup<T>::State;
    for (State state : {State::unhandled, State::unattached,
                        State::unregistered, State::handled})
    {
      for (Inequality<T> *ineq : derived.get(state))
      {
        while (ineq->derived_ids.size() > 0)
        {
          uint64_t id = *ineq->derived_ids.begin();
          ineq->core_ids.insert(id);
          ineq->derived_ids.erase(id);
        }
        core.add(*ineq);
      }
    }
    derived.clear();
  }

  void moveMultipleToCore(std::vector<Inequality<T> *> ineqs, std::vector<uint64_t> ids)
  {
    assert(ineqs.size() == ids.size());

    for (uint64_t idx = 0; idx < ineqs.size(); idx++)
    {
      if (ineqs[idx] != nullptr)
      {
        moveToCore(*ineqs[idx], ids[idx]);
      }
    }
  }

  void moveToCore(Inequality<T> &ineq, uint64_t id)
  {
    // Assert that constraint ID is either in core or derived set.
    assert(ineq.core_ids.find(id) != ineq.core_ids.end() || ineq.derived_ids.find(id) != ineq.derived_ids.end());

    // Check if constraint ID is not already in the set of `core_ids`.
    if (ineq.core_ids.find(id) == ineq.core_ids.end())
    {
      // Move constraint from derived to core propagator if no ID of the constraint was in the core before.
      if (ineq.core_ids.size() == 0)
      {
        derived.remove(ineq);
        core.add(ineq);
      }

      // Remove constraint from derived
      ineq.derived_ids.erase(id);

      // Add constraint to core
      ineq.core_ids.insert(id);
    }

    // It is always safe to set the current id that is moved to the core as `someId`.
    ineq.setSomeId(id);
  }

  void moveToDerived(Inequality<T> &ineq, uint64_t id)
  {
    // Assert that constraint ID is either in core or derived set.
    assert(ineq.core_ids.find(id) != ineq.core_ids.end() || ineq.derived_ids.find(id) != ineq.derived_ids.end());

    // Check if constraint ID is not already in the set of `derived_ids`.
    if (ineq.derived_ids.find(id) == ineq.derived_ids.end())
    {
      // Remove constraint ID from core.
      ineq.core_ids.erase(id);

      // Add constraint ID to derived.
      ineq.derived_ids.insert(id);

      // Move constraint from core propagator to derived propagator if no core IDs are remaining.
      if (ineq.core_ids.size() == 0)
      {
        core.remove(ineq);
        derived.add(ineq);
      }
    }
  }

  /// @brief  Initializes the propagation. Clears the propagation trail if constraints have been detached since last propagation.
  void initPropagation(bool coreOnly = false)
  {
    Timer timer(timeInitProp);
    bool propagationRequired = false;

    if (coreOnly && derived.isActive())
    { // deactivate derived constraint propagator
      derived.deactivatePropagators();
      PropState emptyTrail;
      propMaster.reset(emptyTrail);
      core.doPropagationsAt0();
    }
    else if (hasDetached && !propMaster.isTrailClean())
    { // Cleanup trail if constraints have been detached since last propagation.
      propMaster.cleanupTrail();

      core.doPropagationsAt0();
      if (!coreOnly)
      {
        derived.doPropagationsAt0();
      }
    }

    if (!coreOnly && !derived.isActive())
    { // reenable derived constraint propagator
      derived.activatePropagators();
      derived.doPropagationsAt0();
      propagationRequired = true;
    }

    propagationRequired |= core.attachUnattached();
    if (!coreOnly)
    {
      propagationRequired |= derived.attachUnattached();
    }

    if (propagationRequired)
    {
      propMaster.propagate();
    }

    if (hasDetached)
    {
      MasterJunkyard::get().clearAll();
      hasDetached = false;
    }
  }

  int attachCount(Inequality<T> *ineq)
  {
    Inequality<T> *tmp;
    assert((tmp = find(ineq), tmp == nullptr || tmp == ineq));
    return ineq->attachCount;
  }

  std::vector<uint64_t> getDeletions(Inequality<T> *ineq)
  {
    std::vector<uint64_t> result;

    if (ineq && ineq->attachCount > 0)
    {
      ineq->attachCount -= 1;
      if (ineq->attachCount == 0)
      {
        std::copy(ineq->core_ids.begin(), ineq->core_ids.end(),
                  std::back_inserter(result));
        std::copy(ineq->derived_ids.begin(), ineq->derived_ids.end(),
                  std::back_inserter(result));
      }
    }

    return result;
  }

  bool detach(Inequality<T> *ineq, uint64_t id)
  {
    bool erased = false;

    if (ineq != nullptr)
    {
      // We later need to know if the constraint was a core constraint.
      bool isCoreConstraint = ineq->isCoreConstraint();
      // Delete ID from constraint object.
      size_t was_erased = ineq->core_ids.erase(id);
      was_erased += ineq->derived_ids.erase(id);
      ineq->removeOutId(id);
      if (was_erased == 0)
      {
        return false;
      }

      // If current `someId` was deleted, but we still have valid IDs, then a new `someId` has to be found.
      if (ineq->getSomeId() == id)
      {
        if (ineq->core_ids.size() > 0)
        {
          ineq->setSomeId(*ineq->core_ids.begin());
        }
        else if (ineq->derived_ids.size() > 0)
        {
          ineq->setSomeId(*ineq->derived_ids.begin());
        }
      }

      // Move constraint to derived propagator if constraint has no more core IDs stored, but there are still derived IDs
      if (ineq->isAttached && isCoreConstraint && ineq->core_ids.size() == 0 && ineq->derived_ids.size() > 0)
      {
        core.remove(*ineq);
        derived.add(*ineq);
      }

      // Delete the constraint fully if it was attached and no more valid IDs of this constraint exist.
      if (ineq->isAttached && ineq->core_ids.size() == 0 && ineq->derived_ids.size() == 0)
      {
        erased = true;
        ineq->markedForDeletion();

        ineq->isAttached = false;
        constraintLookup.erase(ineq);
        lookup_requests += 1;

        dbMem -= ineq->mem();

        if (isCoreConstraint)
        {
          core.remove(*ineq);
        }
        else
        {
          derived.remove(*ineq);
        }

        if (ineq->isReason())
        {
          hasDetached = true;
        }
      }
    }

    return erased;
  }

  std::vector<int> propagatedLits(bool onlyCore)
  {
    initPropagation(onlyCore);
    propagate();

    std::vector<int> assignment;
    for (uint var = 1; var <= nVars; var++)
    {
      State val = propMaster.getAssignment().value[Lit(var)];
      int lit = var;
      if (val != State::Unassigned)
      {
        if (val == State::True)
        {
          assignment.push_back(lit);
        }
        else
        {
          assignment.push_back(-lit);
        }
      }
    }
    return assignment;
  }

  inline void printConstraint(VariableNameManager &var_name_manager, std::unordered_map<Lit, BigInt> &terms, BigInt degree)
  {
    for (auto term : terms)
    {
      std::cout << term.second << " ";
      if (term.first.isNegated())
      {
        std::cout << "~";
      }
      std::cout << var_name_manager.getName(term.first.var()) << " ";
    }
    std::cout << ">= " << degree;
  }

  void printPropagationTrace(VariableNameManager &var_name_manager, bool use_color, bool onlyCore)
  {
    initPropagation(onlyCore);
    propagate();

    auto propagatedLits = propMaster.getTrail();
    auto itPropagatedLit = propagatedLits.begin();
    auto propagationReasons = propMaster.getReasons();
    auto itReason = propagationReasons->begin();

    std::vector<std::pair<int, uint64_t>> trace;
    while (itPropagatedLit != propagatedLits.end() && itReason != propagationReasons->end())
    {
      // Print propagated literal
      if (use_color)
      {
        std::cout << "\u001b[95;1m";
      }
      if (itPropagatedLit->isNegated())
      {
        std::cout << "    ~";
      }
      else
      {
        std::cout << "    ";
      }
      std::cout << var_name_manager.getName(itPropagatedLit->var());
      if (use_color)
      {
        std::cout << "\u001b[0m";
      }
      std::cout << "  (";

      // Print constraint
      if (use_color)
      {
        std::cout << "\u001b[34;1m";
      }
      auto terms = (*itReason)->getTerms();
      printConstraint(var_name_manager, terms, (*itReason)->getDegree());
      if (use_color)
      {
        std::cout << "\u001b[0m";
      }

      std::cout << ")";
      std::cout << "\n";
      ++itPropagatedLit;
      ++itReason;
    }
    std::cout << std::endl;
  }

  void addIfNeccessary(std::vector<InequalityPtr<T>> &result,
                       Inequality<T> *ineq, Substitution &sub, bool only_core_subproof,
                       ProofContext &proof, bool gen_proof, bool onlyCore)
  {
    InequalityPtr<T> rhs = ineq->copy();
    rhs->substitute(sub);
    rhs->freeze();
    // we want to keep the id, as they will be used for subgoals
    rhs->copyId(*ineq);

    if (rhs->isTrivial() || ((!only_core_subproof || ineq->isCoreConstraint()) && ineq->implies(*rhs)))
    {
      return;
    }

    Inequality<T> *found = find(rhs.get(), only_core_subproof);
    if (found)
    {
      return;
    }
    else
    {
      result.emplace_back(std::move(rhs));
    }
  }

  std::vector<InequalityPtr<T>> computeEffected(Substitution &sub,
                                                ProofContext &proof,
                                                bool only_core_subproof, bool onlyCore = false, bool gen_proof = false)
  {
    Timer timer(timeEffected);

    std::vector<InequalityPtr<T>> result;
    for (Inequality<T> *ineq : core.computeEffected(sub))
    {
      addIfNeccessary(result, ineq, sub, only_core_subproof, proof, gen_proof, onlyCore);
    }

    if (!onlyCore)
    {
      for (Inequality<T> *ineq : derived.computeEffected(sub))
      {
        addIfNeccessary(result, ineq, sub, only_core_subproof, proof, gen_proof, onlyCore);
      }
    }

    return result;
  }

  Inequality<T> *find(Inequality<T> *ineq, bool onlyCore = false)
  {
    Timer timer(timeFind);
    ineq->contract();
    lookup_requests += 1;

    auto result = constraintLookup.find(ineq);
    // Constraint is not found if constraint is not in lookup.
    if (result == constraintLookup.end())
    {
      return nullptr;
    }
    else
    {
      // Constraint is not found if constraints should only be searched in core set and if the found constraint is not a core constraint.
      if (onlyCore && !(*result)->isCoreConstraint())
      {
        return nullptr;
      }
      else
      {
        return *result;
      }
    }
  }

  std::pair<Ineq *, bool> checkOutputConstraints(std::vector<Ineq *> &outputConstraints)
  {
    // Initialize hashset to track occurrences of output constraints in core.
    std::unordered_map<Ineq *, uint32_t, PointedHash<Ineq>, PointedEq<Ineq>> constraintCheckMap;
    for (Ineq *constraint : constraintLookup)
    {
      if (constraint->isCoreConstraint())
      {
        constraintCheckMap[constraint] = 0;
      }
    }

    // Check if output constraint is in core.
    for (Ineq *constraint : outputConstraints)
    {
      auto result = constraintCheckMap.find(constraint);
      if (result == constraintCheckMap.end())
      {
        return std::make_pair(constraint, false);
      }
      result->second++;
    }

    // Check that we have all core constraints in the output at least once.
    for (std::pair<Ineq *, uint32_t> constraintPair : constraintCheckMap)
    {
      if (constraintPair.second == 0)
      {
        return std::make_pair(constraintPair.first, true);
      }
    }

    return std::make_pair(nullptr, true);
  }

  Ineq *checkConstraintImpliedByDB(Ineq &target_constraint, bool only_core = false)
  {
    for (Ineq *constraint : constraintLookup)
    {
      // We check a constraint if `only_core` is not enabled, or if it is enabled and the constraint is a core constraint.
      if (!only_core || constraint->isCoreConstraint())
      {
        if (constraint->implies(target_constraint))
        {
          return constraint;
        }
      }
    }

    return nullptr;
  }

  inline bool isVarOccurring(uint var)
  {
    return core.isVarOccurring(var) || derived.isVarOccurring(var);
  }

  void get_unit_propagation_hints(bool onlyCore, ProofContext &proof)
  {
    auto itReason = propMaster.reasons.rbegin();

    std::vector<uint64_t> usedOutputIDs;
    void *prev_constraint_ptr = nullptr;

    while (itReason != propMaster.reasons.rend())
    {
      // Check if constraint propagated multiple times in a row and remove duplicates.
      if ((*itReason)->getConstraintPtr() != prev_constraint_ptr)
      {
        usedOutputIDs.push_back((*itReason)->getOutputConstraintID(onlyCore));
        prev_constraint_ptr = (*itReason)->getConstraintPtr();
      }

      // Go back in the trail.
      ++itReason;
    }

    // Print output IDs used to reach conflict in reverse order.
    for (auto itOutputID = usedOutputIDs.rbegin(); itOutputID != usedOutputIDs.rend(); itOutputID++)
    {
      if (*itOutputID == proof.nextIdNoInc())
        proof.buffer << " ~";
      else
        proof.buffer << " " << *itOutputID;
    }
  }

  struct rupCheckWithHints
  {
    template <typename TIneq>
    bool operator()(TIneq &redundant, PropEngine<T> &engine, bool onlyCore, ProofContext &proof, std::vector<uint64_t> &hints, std::vector<Ineq *> &hintConstraints)
    {
      Timer timer(engine.timeRUP);
      Assignment assignment = Assignment(engine.nVars);
      FixedSizeInequalityHandler<T> &negated = engine.negated;
      negated.replace_new(redundant.terms.size(), redundant.terms.size(),
                          redundant.terms.begin(), redundant.terms.end(),
                          redundant.degree);
      InplaceIneqOps::negate()(*negated.ineq);

      bool propagation_happened = true;
      while (propagation_happened)
      {
        propagation_happened = false;
        auto itConstraints = hintConstraints.begin();
        for (auto hint : hints)
        {
          if (hint == 0)
          {
            T slack = InplaceIneqOps::compute_slack<T>()(*negated.ineq, assignment);
            // Check for conflict.
            if (slack < 0)
            {
              if (proof.is_logging_proof)
                proof.buffer << " ~";
              return true;
            }
            // Propagate constraint.
            if (InplaceIneqOps::propagate_constraint<T>()(*negated.ineq, assignment, slack))
            {
              propagation_happened = true;
              if (proof.is_logging_proof)
                proof.buffer << " ~";
            }
          }
          else
          {
            T slack = (*itConstraints)->compute_slack(assignment);
            // Check for conflict.
            if (slack < 0)
            {
              if (proof.is_logging_proof)
                proof.buffer << " " << (*itConstraints)->getOutId(hint);
              return true;
            }
            // Propagate constraint.
            if ((*itConstraints)->propagate_constraint(assignment, slack))
            {
              propagation_happened = true;
              if (proof.is_logging_proof)
                proof.buffer << " " << (*itConstraints)->getOutId(hint);
            }

            // Advance constraint pointer.
            itConstraints++;
          }
        }
      }
      return false;
    }
  };

  struct rupCheck
  {
    template <typename TIneq>
    bool operator()(TIneq &redundant, PropEngine<T> &engine,
                    bool onlyCore, ProofContext &proof, bool toAnnotatedRUP)
    {
      Timer timer(engine.timeRUP);
      // The propagation initialization takes care of propagating units after they are introduced and reusing the trail if it is still valid. Maybe we should still propagate from time to time at decision level 0 to get all propagations stored that are always valid.
      engine.initPropagation(onlyCore);

      // Fully propagating is too time consuming in some cases
      // in connection with redundancy checks. Consider that we
      // want to check F, \neg C, \neg D |- \bot in a redundancy
      // check. Propagating F, \neg C might take quite some time
      // but together with \neg D we arrive at conflict quickly.
      //
      // Every `LEVEL_ZERO_PROPAGATION_INTERVAL` RUP checks will propagate from level 0 to store propagations that are done without the RUP constraint.
      static size_t formula_propagation_counter = LEVEL_ZERO_PROPAGATION_INTERVAL;
      formula_propagation_counter -= 1;
      if (formula_propagation_counter <= 0)
      {
        engine.propagate();
        formula_propagation_counter = LEVEL_ZERO_PROPAGATION_INTERVAL;
      }

      // Propagation without `redundant` constraint already leads to contradiction.
      if (engine.propMaster.isConflicting())
      {
        if (proof.is_logging_proof)
        {
          engine.propMaster.analyze(proof, toAnnotatedRUP, onlyCore);
        }
        return true;
      }

      // Add negation of `redundant` constraint to propagation engine.
      FixedSizeInequalityHandler<T> &negated = engine.negated;
      negated.replace_new(redundant.terms.size(), redundant.terms.size(),
                          redundant.terms.begin(), redundant.terms.end(),
                          redundant.degree);
      negated->setOutId(negated->getSomeId(), proof.nextIdNoInc());

      {
        AutoReset reset(engine.propMaster);
        InplaceIneqOps::negate()(*negated.ineq);

        engine.propMaster.activatePropagator(engine.tmpPropagator);
        negated->initWatch(engine.tmpPropagator);

        engine.propagate();
        if (engine.propMaster.isConflicting())
        {
          if (proof.is_logging_proof)
          {
            engine.propMaster.analyze(proof, toAnnotatedRUP, onlyCore);
          }
        }

        // Clear `negated` constraint from propagation engine. `AutoReset` takes care of resetting the trail to the trail before propagation.
        negated->clearWatches(engine.tmpPropagator);
        engine.propMaster.deactivatePropagator(engine.tmpPropagator);

        return engine.propMaster.isConflicting();
      }
    }
  };
};

struct isSAT
{
  template <typename TIneq>
  bool operator()(const TIneq &constraint, const Assignment &assignment)
  {
    using T = typename TIneq::TTerm::coeff_type;

    T slack = -constraint.degree;
    for (const auto &term : constraint.terms)
    {
      if (assignment[term.lit] == State::True)
      {
        slack += term.coeff;
        if (slack >= 0)
        {
          return true;
        }
      }
    }
    return slack >= 0;
  }
};

/**
 * stores constraint in (literal) normalized form, the sign of the
 * literal is stored in the coefficient
 */
template <typename T>
class FatInequality
{
private:
  VarIndexedVec<T> coeffs;
  VarIndexedVec<uint8_t> used;
  std::vector<Var> usedList;
  T degree;
  bool bussy;

  void setCoeff(Var var, T value)
  {
    if (value != 0)
    {
      use(var);
      coeffs[var] = value;
    }
  }

  void use(Var var)
  {
    if (this->coeffs.size() <= var)
    {
      this->coeffs.resize(var.value + 1, 0);
      this->used.resize(var.value + 1, false);
    }

    if (!used[var])
    {
      used[var] = true;
      usedList.push_back(var);
    }
  }

public:
  FatInequality()
  {
    degree = 0;
    bussy = false;
  }

  template <typename TConstraint>
  void load(TConstraint &ineq)
  {
    assert(!bussy && "critical error: I am used twice!");
    bussy = true;

    this->degree = ineq.degree;
    Term<T> myTerm;
    for (auto &term : ineq.terms)
    {
      if (term.coeff < 0)
      {
        myTerm.lit = ~term.lit;
        myTerm.coeff = -term.coeff;
        this->degree += myTerm.coeff;
      }
      else
      {
        myTerm.lit = term.lit;
        myTerm.coeff = std::move(term.coeff);
      }
      addLhs(myTerm);
      // T coeff = cpsign(term.coeff, term.lit);
      // using namespace std;

      // setCoeff(term.lit.var(), coeff);
    }

    if (this->coeffs.size() > 0)
    {
      Var one = Substitution::one().var();
      T &coeff = this->coeffs[one];
      if (coeff > 0)
      {
        this->degree -= coeff;
      }
      this->coeffs[one] = 0;
    }
  }

  void unload(FixedSizeInequality<T> &ineq)
  {
    bussy = false;

    auto pos = ineq.terms.begin();
    for (Var var : this->usedList)
    {
      T &coeff = this->coeffs[var];
      Lit lit(var, coeff < 0);
      using namespace std;
      coeff = abs(coeff);

      if (coeff > 0)
      {
        assert(var != Substitution::one().var());
        assert(pos != ineq.terms.end());
        pos->coeff = std::move(coeff);
        pos->lit = lit;
        pos += 1;
      }

      this->coeffs[var] = 0;
      this->used[var] = false;
    }
    assert(pos == ineq.terms.end());
    this->usedList.clear();

    ineq.degree = this->degree;
    this->degree = 0;

    // Normalized only has non-negative degree, hence negative degree should be brought up to 0
    if (ineq.degree < 0)
    {
      ineq.degree = 0;
    }
  }

  size_t size()
  {
    size_t result = 0;
    for (Var var : this->usedList)
    {
      if (this->coeffs[var] != 0)
      {
        result += 1;
      }
    }
    return result;
  }

  void weaken(Var var)
  {
    this->degree -= abs(this->coeffs[var]);
    this->coeffs[var] = 0;
  }

  void multiply(T factor)
  {
    for (Var var : this->usedList)
    {
      this->coeffs[var] *= factor;
    }
    this->degree *= factor;
  }

  /* requires positive coefficients */
  template <typename IntType>
  void addLhs(const Term<IntType> &term)
  {
    using namespace std;
    T b = cpsign(term.coeff, term.lit);
    Var var = term.lit.var();
    this->use(var);
    T a = this->coeffs[var];
    if (a == 0)
    {
      this->coeffs[var] = b;
    }
    else
    {
      this->coeffs[var] = a + b;
      T cancellation = max<T>(0, max(abs(a), abs(b)) - abs(this->coeffs[var]));
      this->degree -= cancellation;
    }
  }

  template <typename TConstraint>
  void add(const TConstraint &other)
  {
    for (const auto &term : other.terms)
    {
      addLhs(term);
    }

    this->degree += other.degree;
  }

  struct callLoad
  {
    template <typename TConstraint, typename Calee>
    void operator()(TConstraint &constraint, Calee &callee)
    {
      callee.load(constraint);
    }
  };

  struct callAdd
  {
    template <typename TConstraint>
    void operator()(TConstraint &constraint, FatInequality<T> &callee)
    {
      callee.add(constraint);
    }
  };
};

using IneqLarge = FixedSizeInequality<int32_t>;
using IneqBig = FixedSizeInequality<BigInt>;

enum class TypeId
{
  Clause,
  IneqLarge,
  IneqBig
};

template <typename T>
struct type_id;

template <>
struct type_id<Clause>
{
  static constexpr TypeId value = TypeId::Clause;
};

template <>
struct type_id<IneqLarge>
{
  static constexpr TypeId value = TypeId::IneqLarge;
};

template <>
struct type_id<IneqBig>
{
  static constexpr TypeId value = TypeId::IneqBig;
};

class BaseHandle;
using HandlePtr = std::unique_ptr<BaseHandle>;

enum class CoeffBound
{
  trivial = 0,
  one = 1,
  int32 = 2,
  int64 = 3,
  unbounded = 4
};

template <typename TInt>
static CoeffBound getBound(TInt i, CoeffBound base = CoeffBound::trivial)
{
#ifdef __APPLE__
  // This is a fix to make VeriPB compile on MacOS. On MacOS `int64_t` is `long long` and gmp does not implement comparison between `BigInt` and `long long`. So for MacOS the `CoeffBound` of 64 bits is skipped.
  std::array<int32_t, 4> bound = {0, 1, std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()};
#else
  std::array<int64_t, 4> bound = {0, 1, std::numeric_limits<int32_t>::max(), std::numeric_limits<int64_t>::max()};
#endif
  uint group = static_cast<uint>(base);
  while (group < bound.size() && i > bound[group])
  {
    group += 1;
  }
#ifdef __APPLE__
  // Skipping `CoeffBound` of 64 bits if more than 32 bits are needed for MacOS.
  if (group == 3)
  {
    group = 4;
  }
#endif
  return static_cast<CoeffBound>(group);
};

class BaseHandle
{
public:
  const TypeId typeId;
  BaseHandle(TypeId _typeId) : typeId(_typeId) {}
  virtual HandlePtr copy() = 0;
  virtual void toJunkyard() = 0;
  virtual bool isReason() = 0;
  virtual uint64_t getOutId(uint64_t) = 0;
  virtual void setOutId(uint64_t, uint64_t) = 0;
  virtual void removeOutId(uint64_t) = 0;
  virtual void setSomeId(uint64_t) = 0;
  virtual uint64_t getSomeId() = 0;
  virtual bool allCoeffOne() = 0;
  virtual CoeffBound getBoundDegree() = 0;
  virtual bool isPropagatingAt0() = 0;
  virtual void markedForDeletion() = 0;
  virtual size_t mem() = 0;
  virtual ~BaseHandle() = default;
};

template <typename TConstraint>
struct Handle : public BaseHandle
{
  ConstraintHandler<TConstraint> manager;

  Handle(ConstraintHandler<TConstraint> &&_manager)
      : BaseHandle(type_id<TConstraint>::value), manager(std::move(_manager)) {}

  virtual HandlePtr copy()
  {
    return std::make_unique<Handle<TConstraint>>(
        ConstraintHandler<TConstraint>(manager));
  };

  TConstraint &get()
  {
    assert(manager.ineq != nullptr);
    return *manager.ineq;
  }

  virtual void toJunkyard()
  {
    Junkyard<ConstraintHandler<TConstraint>>::get().add(std::move(manager));
  }

  virtual bool isReason() { return get().header.isReason; }

  /// @brief Get the output proof constraint ID of the constraint that corresponds to `in_id` of the input proof.
  /// @param in_id Constraint ID of the constraint in the input proof.
  /// @return If `in_id` has no output proof constraint associated with it, then `0` is returned as an error. Otherwise, the output proof constraint ID for this constraint is returned.
  virtual inline uint64_t getOutId(uint64_t in_id)
  {
    auto out_id = get().header.in_to_out_id.find(in_id);
    if (out_id == get().header.in_to_out_id.end())
    {
      return NO_OUTPUT_ID_FOUND;
    }
    else
    {
      return out_id->second;
    }
  }

  virtual inline void setOutId(uint64_t in_id, uint64_t out_id)
  {
    get().header.in_to_out_id[in_id] = out_id;
  }

  virtual inline void removeOutId(uint64_t in_id)
  {
    get().header.in_to_out_id.erase(in_id);
  }

  virtual inline void setSomeId(uint64_t id)
  {
    get().header.someId = id;
  }

  virtual inline uint64_t getSomeId()
  {
    return get().header.someId;
  }

  virtual bool allCoeffOne()
  {
    if (std::is_same<TConstraint, Clause>::value)
    {
      return true;
    }

    for (auto &term : get().terms)
    {
      assert(term.coeff >= 0);
      if (term.coeff != 1)
        return false;
    }
    return true;
  }

  virtual CoeffBound getBoundDegree() { return getBound(get().degree); }

  virtual void markedForDeletion() { get().header.isMarkedForDeletion = true; }

  virtual bool isPropagatingAt0() { return get().isPropagatingAt0(); }

  virtual size_t mem()
  {
    return sizeof(TConstraint) +
           get().terms.size() * sizeof(typename TConstraint::TTerm);
  }

  virtual ~Handle() = default;
};

template <typename TConstraintHandler>
static HandlePtr make_handle(TConstraintHandler &&handler)
{
  using TConstraint = typename TConstraintHandler::TStoredConstraint;
  return HandlePtr(new Handle<TConstraint>(std::move(handler)));
}

constexpr int8_t combineTypeId(TypeId idA, TypeId idB)
{
  return static_cast<int8_t>(idA) << 4 | static_cast<int8_t>(idB);
};

// result_of_t requires full parameters, while we do not know if
// method will be called with a Clause, the return type should be
// identical, no matter which constraint is called, so lets just
// use the return type that is defined for a clause
using AnyIneq = Clause;

struct unpacked
{
  template <typename method, typename... Args>
  static std::result_of_t<method && (AnyIneq &, Args &&...)>
  call(method m, BaseHandle *a, Args &&...args)
  {
    // we could totally use virtual function calls here, but as we need
    // the infrastructure anyway to call methods on pairs of
    // inequalities, why not use it always and save(?) some typing.
    switch (a->typeId)
    {
    case TypeId::Clause:
      return m(static_cast<Handle<Clause> *>(a)->get(),
               std::forward<Args>(args)...);
      break;
    case TypeId::IneqLarge:
      return m(static_cast<Handle<IneqLarge> *>(a)->get(),
               std::forward<Args>(args)...);
      break;
    case TypeId::IneqBig:
      return m(static_cast<Handle<IneqBig> *>(a)->get(),
               std::forward<Args>(args)...);
      break;
    default:
      unreachible("Internal error.");
    }
  }

  struct doubleUnpack
  {
    template <typename UnpackedB, typename method, typename... Args>
    std::result_of_t<method && (AnyIneq &, AnyIneq &, Args &&...args)>
    operator()(UnpackedB &b, BaseHandle *a, method m, Args &&...args)
    {
      return call(m, a, b, std::forward<Args>(args)...);
    }
  };

  template <typename method, typename... Args>
  static std::result_of_t<doubleUnpack &&
                          (AnyIneq &, BaseHandle *a, method m, Args &&...args)>
  call2(method m, BaseHandle *a, BaseHandle *b, Args &&...args)
  {
    return call(doubleUnpack(), b, a, m, std::forward<Args>(args)...);
  }
};

// template<typename method>
// auto callUnpacked(method m, BaseHandle* a, BaseHandle* b) {
//     int8_t test = combineTypeId(a->typeId, b->typeId);
//     switch (test) {
//         case combineTypeId(TypeId::Clause, TypeId::Clause):
//             return m(static_cast<Handle<Clause>*>(a)->get(),
//             static_cast<Handle<Clause>*>(b)->get()); break;
//         case combineTypeId(TypeId::Clause, TypeId::IneqLarge):
//             return m(static_cast<Handle<Clause>*>(a)->get(),
//             static_cast<Handle<IneqLarge>*>(b)->get()); break;
//         case combineTypeId(TypeId::IneqLarge, TypeId::Clause):
//             return m(static_cast<Handle<IneqLarge>*>(a)->get(),
//             static_cast<Handle<Clause>*>(b)->get()); break;
//         case combineTypeId(TypeId::IneqLarge, TypeId::IneqLarge):
//             return m(static_cast<Handle<IneqLarge>*>(a)->get(),
//             static_cast<Handle<IneqLarge>*>(b)->get()); break;
//         default:
//             assert(false);
//             throw "unreachable";
//     }
// }

template <typename T>
using FatInequalityPtr = std::unique_ptr<FatInequality<T>>;

/**
 * stores constraint in (literal) normalized form
 *
 * The inequality can have three states: normal, expanded, frozen it
 * switches automatically between normal and expanded as needed. Once
 * the inequality is frozen it can not switch back and all operations
 * that would modify the inequality are disallowed.
 */
template <typename T>
class Inequality
{
private:
  bool loaded = false;
  bool frozen = false;
  FatInequalityPtr<T> expanded;

  HandlePtr handle;

  static std::vector<FatInequalityPtr<T>> pool;
  const bool useClauses = true;

  friend std::hash<Inequality<T>>;

public:
  bool isAttached = false;
  bool wasAttached = false;
  std::unordered_set<uint64_t> derived_ids;
  std::unordered_set<uint64_t> core_ids;
  uint attachCount = 0;

  // used exclusively by PropagatorGroup
  typename std::list<Inequality *>::iterator _groupIter;
  typename PropagatorGroup<T>::State _groupState;

  Inequality(Inequality &other) : handle(other.handle->copy())
  {
    assert(other.loaded == false);
  }

  template <typename TIneq>
  Inequality(ConstraintHandler<TIneq> &&handler)
      : handle(make_handle(std::move(handler))) {}

  Inequality(std::vector<Term<T>> &terms_, T degree_)
      : Inequality(
            FixedSizeInequalityHandler<T>(terms_.size(), terms_, degree_))
  {
    normalize();
  }

  Inequality(std::vector<Term<T>> &&terms_, T degree_)
      : Inequality(
            FixedSizeInequalityHandler<T>(terms_.size(), terms_, degree_))
  {
    normalize();
  }

  Inequality(std::vector<T> &coeffs, std::vector<int> &lits, T degree_)
      : Inequality(Term<T>::toTerms(coeffs, lits), degree_) {}

  Inequality(std::vector<T> &&coeffs, std::vector<int> &&lits, T degree_)
      : Inequality(Term<T>::toTerms(coeffs, lits), degree_) {}

  ~Inequality()
  {
    contract();

    if (wasAttached)
    {
      assert(handle);
      handle->toJunkyard();
    }
  }

  /// @brief Check if the constraint is a core constraint.
  /// @return Returns true iff constraints has at least one valid core constraint ID.
  bool isCoreConstraint()
  {
    return core_ids.size() >= 1;
  }

  /// @brief Get some core ID of the constraint, if it exists.
  /// @return Some core ID of the constraint. If it does not exist, then return `DEFAULT_CONSTRAINT_ID`.
  uint64_t getSomeCoreId()
  {
    if (core_ids.begin() != core_ids.end())
    {
      return *core_ids.begin();
    }
    else
    {
      return DEFAULT_CONSTRAINT_ID;
    }
  }

  bool isCoreId(uint64_t id)
  {
    if (core_ids.find(id) != core_ids.end())
    { // `id` is in `core_ids`
      return true;
    }
    else
    {
      return false;
    }
  }

  bool isDerivedId(uint64_t id)
  {
    if (derived_ids.find(id) != derived_ids.end())
    { // `id` is in `derived_ids`

      return true;
    }
    else
    {
      return false;
    }
  }

  void copyId(Inequality<T> &other)
  {
    setSomeId(other.getSomeId());
    setOutId(getSomeId(), other.getOutId(getSomeId()));
  }

  void normalize()
  {
    expand();
    contract();
  }

  bool isReason() { return handle->isReason(); }

  void freeze()
  {
    if (!frozen)
    {
      contract();

      if (handle->typeId != TypeId::Clause)
      {
        bool allCoeffOne = handle->allCoeffOne();
        CoeffBound degreeBound = handle->getBoundDegree();
        if (allCoeffOne && degreeBound == CoeffBound::one)
        {
          ClauseHandler clauseManager = unpacked::call(
              InplaceIneqOps::makeHandler<Clause>(), handle.get());
          HandlePtr clauseHandle = make_handle(std::move(clauseManager));
          std::swap(clauseHandle, handle);
        }
      }

      frozen = true;
    }
  }

  void clearWatches(PropagatorGroup<T> &prop)
  {
    assert(frozen);
    unpacked::call(typename PropagatorGroup<T>::clearWatch(), handle.get(),
                   prop);
  }

  void markedForDeletion()
  {
    assert(frozen);
    handle->markedForDeletion();
  }

  bool isPropagatingAt0() { return handle->isPropagatingAt0(); }

  void initWatch(PropagatorGroup<T> &prop)
  {
    assert(frozen && "Call freeze() first.");
    unpacked::call(typename PropagatorGroup<T>::initWatch(), handle.get(),
                   prop);
  }

  void updateWatch(PropagatorGroup<T> &prop)
  {
    assert(frozen && "Call freeze() first.");
    unpacked::call(typename PropagatorGroup<T>::updateWatches(), handle.get(),
                   prop);
  }

  Inequality &saturate()
  {
    assert(!frozen);
    contract();

    bool isNormalized =
        unpacked::call(InplaceIneqOps::saturate(), handle.get());

    if (!isNormalized)
    {
      // nasty hack to shrink the constraint as this should not
      // happen frequently anyway.
      expand();
      contract();
    }

    return *this;
  }

  Inequality &divide(T divisor)
  {
    assert(!frozen);
    contract();
    unpacked::call(InplaceIneqOps::divide(), handle.get(), divisor);
    return *this;
  }

  Inequality &multiply(T factor)
  {
    assert(!frozen);
    // todo this is lazy for making sure we don't have a clause.
    expand();
    expanded->multiply(factor);
    return *this;
  }

  Inequality &add(Inequality &other)
  {
    assert(!frozen);
    expand();
    other.contract();
    unpacked::call(typename FatInequality<T>::callAdd(), other.handle.get(),
                   *expanded);
    return *this;
  }

  void expand()
  {
    assert(!frozen);
    if (!loaded)
    {
      loaded = true;
      if (pool.size() > 0)
      {
        expanded = std::move(pool.back());
        pool.pop_back();
      }
      else
      {
        expanded = std::make_unique<FatInequality<T>>();
      }

      unpacked::call(typename FatInequality<T>::callLoad(), handle.get(),
                     *expanded);
      handle = nullptr;
    }
  }

  void contract()
  {
    if (loaded)
    {
      assert(!frozen);
      FixedSizeInequalityHandler<T> manager(expanded->size());
      expanded->unload(*manager.ineq);
      handle =
          HandlePtr(new Handle<FixedSizeInequality<T>>(std::move(manager)));
      pool.push_back(std::move(expanded));
      expanded = nullptr;
      loaded = false;
    }
  }

  bool eq(Inequality &other) { return *this == other; }

  bool operator==(Inequality<T> &other)
  {
    contract();
    other.contract();
    return unpacked::call2(InplaceIneqOps::equals(), handle.get(),
                           other.handle.get());
  }

  bool operator==(const Inequality<T> &other) const
  {
    assert(!this->loaded);
    assert(!other.loaded);
    return unpacked::call2(InplaceIneqOps::equals(), handle.get(),
                           other.handle.get());
  }

  bool operator!=(Inequality<T> &other) { return !(*this == other); }

  std::string toString(std::function<std::string(int)> varName)
  {
    contract();
    std::stringstream s;
    unpacked::call(InplaceIneqOps::print(), handle.get(), varName, s);
    return s.str();
  }

  std::string repr()
  {
    return toString([](int i)
                    {
      std::stringstream s;
      s << "x" << i;
      return s.str(); });
  }

  bool implies(Inequality &other)
  {
    this->contract();
    other.contract();

    return unpacked::call2(InplaceIneqOps::implies(), handle.get(),
                           other.handle.get());
  }

  bool implies_strong(Inequality &other, ProofContext &proof)
  {
    this->contract();
    other.contract();

    return unpacked::call2(InplaceIneqOps::implies_strong(), handle.get(),
                           other.handle.get(), proof);
  }

  T compute_slack(Assignment &assignment)
  {
    this->contract();
    return unpacked::call(InplaceIneqOps::compute_slack<T>(), handle.get(), assignment);
  }

  T get_max_coeff()
  {
    this->contract();
    return unpacked::call(InplaceIneqOps::get_max_coeff<T>(), handle.get());
  }

  bool propagate_constraint(Assignment &assignment, T &slack)
  {
    this->contract();
    return unpacked::call(InplaceIneqOps::propagate_constraint<T>(), handle.get(), assignment, slack);
  }

  bool isSAT(const Assignment &assignment)
  {
    this->contract();
    return unpacked::call(::isSAT(), handle.get(), assignment);
  }

  bool rupCheckWithHints(PropEngine<T> &propEngine, bool onlyCore, ProofContext &proof, std::vector<uint64_t> &hints, std::vector<Inequality *> &hintConstraints)
  {
    this->contract();
    return unpacked::call(typename PropEngine<T>::rupCheckWithHints(), handle.get(),
                          propEngine, onlyCore, proof, hints, hintConstraints);
  }

  bool rupCheck(PropEngine<T> &propEngine, bool onlyCore, ProofContext &proof, bool toAnnotatedRUP)
  {
    this->contract();
    return unpacked::call(typename PropEngine<T>::rupCheck(), handle.get(),
                          propEngine, onlyCore, proof, toAnnotatedRUP);
  }

  Inequality &substitute(Substitution &sub)
  {
    assert(!this->frozen);
    this->contract();
    unpacked::call(InplaceIneqOps::substitute(), handle.get(), sub);
    // need to normalize, we do so by expanding the constraint
    this->expand();
    this->contract();
    return *this;
  }

  Inequality &negated()
  {
    assert(!frozen);
    // todo this is lazy for making sure we don't have a clause.
    expand();
    contract();
    unpacked::call(InplaceIneqOps::negate(), handle.get());
    return *this;
  }

  InequalityPtr<T> copy()
  {
    contract();
    return std::make_unique<Inequality>(*this);
  }

  Inequality &weaken(int _var)
  {
    assert(!frozen);
    assert(_var >= 0);
    Var var(_var);
    expand();
    expanded->weaken(var);
    return *this;
  }

  bool isContradiction()
  {
    contract();
    return unpacked::call(InplaceIneqOps::isContradiction(), handle.get());
  }

  bool isTrivial()
  {
    contract();
    return unpacked::call(InplaceIneqOps::isTrivial(), handle.get());
  }

  size_t mem()
  {
    contract();
    return handle->mem();
  }

  void registerOccurence(PropagatorGroup<T> &prop)
  {
    assert(frozen);
    return unpacked::call(typename PropagatorGroup<T>::addOccurence(),
                          handle.get(), prop, *this);
  }

  void unRegisterOccurence(PropagatorGroup<T> &prop)
  {
    assert(frozen);
    return unpacked::call(typename PropagatorGroup<T>::rmOccurence(),
                          handle.get(), prop, *this);
  }

  uint64_t inline getOutId(uint64_t in_id)
  {
    assert(frozen);
    return handle->getOutId(in_id);
  }

  void inline setOutId(uint64_t in_id, uint64_t out_id)
  {
    assert(frozen);
    handle->setOutId(in_id, out_id);
  }

  void inline removeOutId(uint64_t in_id)
  {
    assert(frozen);
    handle->removeOutId(in_id);
  }

  void inline setSomeId(uint64_t id)
  {
    assert(frozen);
    handle->setSomeId(id);
  }

  uint64_t inline getSomeId()
  {
    assert(frozen);
    return handle->getSomeId();
  }
};

namespace std
{
  template <typename T>
  struct hash<Inequality<T>>
  {
    std::size_t operator()(const Inequality<T> &ineq) const
    {
      return unpacked::call(InplaceIneqOps::hash(), ineq.handle.get());
    }
  };
} // namespace std

// we need to initialize the static template member manually;
template <typename T>
std::vector<FatInequalityPtr<T>> Inequality<T>::pool;

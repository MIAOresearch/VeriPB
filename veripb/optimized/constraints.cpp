#ifdef PY_BINDINGS
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/iostream.h>
#include <pybind11/functional.h>
namespace py = pybind11;
#endif

#include "constraints.hpp"

Propagator::Propagator(PropagationMaster &_propMaster)
    : propMaster(_propMaster)
{
    propMaster.addPropagator(*this);
}

long divideAndRoundUp(long value, BigInt divisor)
{
    BigInt result = ((value + divisor - 1) / divisor);
    return result.get_si();
}

void ClausePropagator::propagate()
{
    // std::cout << "ClausePropagator: propagating from: " << qhead << std::endl;
    // std::cout << *this << std::endl;
    const auto &trail = propMaster.getTrail();
    const Assignment &assignment = propMaster.getAssignment();
    while (qhead < trail.size() and !propMaster.isConflicting())
    {
        Lit falsifiedLit = ~trail[qhead];
        // std::cout << "propagating: " << trail[qhead] << std::endl;

        WatchList &ws = watchlist[falsifiedLit];

        const WatchedType *end = ws.data() + ws.size();
        WatchedType *sat = ws.data();
        // WatchedType* it  = ws.data();
        // for (; it != end; it++) {
        //     if (assignment.value[it->other] == State::True) {
        //         std::swap(*it, *sat);
        //         ++sat;
        //     }
        // }

        WatchedType *next = sat;
        WatchedType *kept = next;

        const uint lookAhead = 3;
        for (; next != end && !propMaster.isConflicting(); next++)
        {
            auto fetch = next + lookAhead;
            if (fetch < end)
            {
                __builtin_prefetch(fetch->ineq);
            }
            assert(next->other != Lit::Undef() || assignment[next->other] == State::Unassigned);

            bool keepWatch = true;
            if (assignment.value[next->other] != State::True)
            {
                assert(next->ineq->header.isMarkedForDeletion == false);
                keepWatch = next->ineq->updateWatch(*this, falsifiedLit, false);
            }
            if (keepWatch)
            {
                *kept = *next;
                kept += 1;
            }
        }

        // in case of conflict copy remaining watches
        for (; next != end; next++)
        {
            *kept = *next;
            kept += 1;
        }

        ws.erase(ws.begin() + (kept - ws.data()), ws.end());

        qhead += 1;
    }
}

int hashColision = 0;

#ifdef PY_BINDINGS
void init_constraints(py::module &m)
{
    m.doc() = "Efficient implementation for linear combinations of constraints.";
    m.def("maxId", []()
          { return std::numeric_limits<uint64_t>::max(); });

    py::class_<Substitution>(m, "Substitution")
        .def(py::init<std::vector<int> &, std::vector<int> &, std::vector<int> &>());

    py::class_<PropEngine<CoefType>>(m, "PropEngine")
        .def(py::init<size_t>())
        .def("attach", &PropEngine<CoefType>::attach)
        .def("detach", &PropEngine<CoefType>::detach)
        .def("getDeletions", &PropEngine<CoefType>::getDeletions)
        .def("attachCount", &PropEngine<CoefType>::attachCount)
        .def("checkSat", &PropEngine<CoefType>::checkSat)
        .def("propagatedLits", &PropEngine<CoefType>::propagatedLits)
        .def("printPropagationTrace", &PropEngine<CoefType>::printPropagationTrace)
        .def("printSolutionPropagationTrace", &PropEngine<CoefType>::printSolutionPropagationTrace)
        .def("increaseNumVarsTo", &PropEngine<CoefType>::increaseNumVarsTo)
        .def("printStats", &PropEngine<CoefType>::printStats)
        .def("computeEffected", &PropEngine<CoefType>::computeEffected)
        .def("find", &PropEngine<CoefType>::find)
        .def("checkOutputConstraints", &PropEngine<CoefType>::checkOutputConstraints)
        .def("checkConstraintImpliedByDB", &PropEngine<CoefType>::checkConstraintImpliedByDB)
        .def("moveToCore", &PropEngine<CoefType>::moveToCore)
        .def("moveToDerived", &PropEngine<CoefType>::moveToDerived)
        .def("moveMultipleToCore", &PropEngine<CoefType>::moveMultipleToCore)
        .def("moveAllToCore", &PropEngine<CoefType>::moveAllToCore)
        .def("get_unit_propagation_hints", &PropEngine<CoefType>::get_unit_propagation_hints);

    py::class_<Assignment>(m, "Assignment")
        .def(py::init<std::vector<int> &>())
        .def(py::init<std::vector<int> &, int>());

    auto cppIneq = py::class_<Inequality<CoefType>>(m, "CppInequality")
                       .def(py::init<std::vector<CoefType> &, std::vector<int> &, CoefType>())
                       .def("getSomeId", &Inequality<CoefType>::getSomeId)
                       .def("isCoreConstraint", &Inequality<CoefType>::isCoreConstraint)
                       .def("isCoreId", &Inequality<CoefType>::isCoreId)
                       .def("isDerivedId", &Inequality<CoefType>::isDerivedId)
                       .def("setOutId", &Inequality<CoefType>::setOutId)
                       .def("getOutId", &Inequality<CoefType>::getOutId)
                       .def("removeOutId", &Inequality<CoefType>::removeOutId)
                       .def("saturate", &Inequality<CoefType>::saturate)
                       .def("divide", &Inequality<CoefType>::divide)
                       .def("multiply", &Inequality<CoefType>::multiply)
                       .def("add", &Inequality<CoefType>::add)
                       .def("contract", &Inequality<CoefType>::contract)
                       .def("copy", &Inequality<CoefType>::copy)
                       .def("copyId", &Inequality<CoefType>::copyId)
                       .def("implies", &Inequality<CoefType>::implies)
                       .def("implies_strong", &Inequality<CoefType>::implies_strong)
                       .def("expand", &Inequality<CoefType>::expand)
                       .def("negated", &Inequality<CoefType>::negated)
                       .def("rupCheck", &Inequality<CoefType>::rupCheck)
                       .def("rupCheckWithHints", &Inequality<CoefType>::rupCheckWithHints)
                       .def("__eq__", &Inequality<CoefType>::eq)
                       .def("__repr__", &Inequality<CoefType>::repr)
                       .def("toString", &Inequality<CoefType>::toString)
                       .def("toOPB", &Inequality<CoefType>::repr)
                       .def("isContradiction", &Inequality<CoefType>::isContradiction)
                       .def("getSomeCoreId", &Inequality<CoefType>::getSomeCoreId)
                       .def("isSAT", &Inequality<CoefType>::isSAT)
                       .def("isTrivial", &Inequality<CoefType>::isTrivial)
                       .def("substitute", &Inequality<CoefType>::substitute)
                       .def("weaken", &Inequality<CoefType>::weaken)
                       .def("get_max_coeff", &Inequality<CoefType>::get_max_coeff);
    cppIneq.attr("__hash__") = py::none();

    py::class_<ProofContext>(m, "ProofContext")
        .def(py::init<uint64_t, bool>())
        .def("setVariableNameManager", &ProofContext::setVariableNameManager)
        .def("getVariableNameManager", &ProofContext::getVariableNameManager)
        .def_readwrite("lastId", &ProofContext::lastId)
        .def("getBuffer", &ProofContext::getBuffer)
        .def("resetBuffer", &ProofContext::resetBuffer);
}
#endif

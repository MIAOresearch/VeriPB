import logging

from veripb.constraints import Term
from veripb.parser import MaybeWordParser
from veripb.timed_function import TimedFunction
from veripb.rules_register import register_rule
from veripb.optimized.constraints import Assignment
from veripb.printing import printRUPTrace
from veripb.autoproving import TemporaryAttach

from veripb import InvalidProof


class Rule():
    @staticmethod
    def getParser(context):
        """
        Returns:
            A parser that creates this rule from the string following the Id
        """
        raise NotImplementedError()

    @classmethod
    def parse(cls, line, context):
        return cls.getParser(context).parse(line.rstrip())

    def compute(self, antecedents, context=None):
        """
        Performs the action of the rule, e.g. compute new constraints or perform
        checks on the current proof.

        Args:
            antecedents (list): Antecedents in the order given by antecedentIDs

        Returns:
            computed constraints or empty list
        """
        raise NotImplementedError()

    def numConstraints(self):
        """
        Returns:
            number of constraints that will be produced by this rule
        """
        raise NotImplementedError()

    def antecedentIDs(self):
        """
        Return a list of indices of used antecednts
        """
        raise NotImplementedError()

    def isGoal(self):
        return False

    def deleteConstraints(self):
        """
        Return a list of constraintId's that can be removed from the database
        """
        return []

    def allowedRules(self, context, currentRules):
        return currentRules

    def newParseContext(self, parseContext):
        parseContext.rules = self.allowedRules(
            parseContext.context, parseContext.rules)
        return parseContext

    def justify(self, antecedents, derived, context):
        for constraint, idx in derived:
            if constraint is not None:
                # context.proof.print("fail")
                # return
                context.proof.print(
                    "a", context.ineqFactory.toString(constraint), ";")
                constraint.setOutId(idx, context.proof.nextId())

    def __str__(self):
        return type(self).__name__

    def isFormula(self):
        return False

    def isOutputProofDeletionPrinted(self):
        return True

    def ignoreDeletedAntecedents(self):
        """
        Return true if the rule is operating over a range of indices and deleted constraints should be ignored.
        """
        return False

    def isAddToCore(self):
        """
        Return true if the constraints added by this rule are added to the core set instead of the derived set.
        """
        return False


class EmptyRule(Rule):
    def compute(self, antecedents, context):
        return []

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        return []


class DummyRule(Rule):
    def compute(self, antecedents, context):
        return []

    def numConstraints(self):
        return 1

    def antecedentIDs(self):
        return []


@register_rule
class HeaderRule(Rule):
    Ids = ["pseudo-Boolean"]

    def compute(self, antecedents, context):
        if context.major == 1:
            self._numConstraints = 0
            return []
        else:
            self._numConstraints = len(context.formula)
            return context.formula

    def numConstraints(self):
        return self._numConstraints

    def antecedentIDs(self):
        return []

    def isFormula(self):
        return True

    def justify(self, antecedents, derived, context):
        pass


class EqualityCheckFailed(InvalidProof):
    def __init__(self, expected, got):
        self.expected = expected
        self.got = got


class ReverseUnitPropagationFailed(InvalidProof):
    pass


def idOrFind(words, context):
    type_key = next(words)
    which = []
    if type_key == "id":
        while True:
            try:
                token = next(words)
                if token == ";":
                    break
                else:
                    which.append(context.to_constraint_id(token))
            except:
                break

        if which and which[-1] == 0:
            which = which[:-1]

        if 0 in which:
            raise InvalidProof("Can not access constraint with index 0.")
    elif type_key == "find" or type_key == "spec":
        if type_key == "spec":
            type_key = "find"
        cppWordIter = words.wordIter.getNative()
        ineq = context.ineqFactory.parse(cppWordIter, allowMultiple=False)

        which = context.propEngine.find(ineq, False)
        if which is None:
            raise ValueError("Can not find constraint %s." % (
                context.ineqFactory.toString(ineq)))
        else:
            which = [which.getSomeId()]

    elif type_key == "range":
        which = list(map(context.to_constraint_id, words))
        if len(which) != 2:
            raise InvalidProof("Expected exactly two arguments.")
        which = list(range(which[0], which[1]))

    else:
        raise InvalidProof(
            "Expected identifier type ('id', 'range', 'find' or 'spec').")

    return (which, type_key)


@register_rule
class Assumption(Rule):
    Ids = ["a"]

    @classmethod
    def parse(cls, words, context):
        cppWordIter = words.wordIter.getNative()
        ineq = context.ineqFactory.parse(cppWordIter, allowMultiple=False)

        return cls(ineq)

    def __init__(self, constraint):
        self.constraint = constraint

    def numConstraints(self):
        return 1

    def antecedentIDs(self):
        return []

    def __eq__(self, other):
        return self.constraint == other.constraint and self.Id == other.Id

    def isGoal(self):
        return False

    @TimedFunction.time("Assumption.compute")
    def compute(self, antecedents, context):
        context.propEngine.increaseNumVarsTo(context.ineqFactory.numVars())
        context.usesAssumptions = True
        return [self.constraint]


@register_rule
class ReverseUnitPropagation(Rule):
    Ids = ["u", "rup"]

    @classmethod
    def parse(cls, words, context):
        cppWordIter = words.wordIter.getNative()
        ineq = context.ineqFactory.parse(cppWordIter, allowMultiple=False)
        nextToken = next(words, None)
        if nextToken is None:
            # No hints given, so we can directly create object.
            return cls(ineq)
        words.undo()

        # Check if correct hint separator is used.
        # if hint_separator != ":":
        #     raise InvalidProof(
        #         "Expected separator ':' between constraint and hints. Got '%s' instead." % hint_separator)
        # Parse the hints as a list of integers.
        hints = [0]
        for hint in words:
            try:
                hints.append(context.to_constraint_id(hint))
            except:
                if hint == "~":
                    # Encountered the negated constraint, which is internally denoted by 0.
                    hints.append(0)
                else:
                    raise InvalidProof("Invalid RUP hint given: " + hint)
        return cls(ineq, hints)

    def __init__(self, constraint, hints=None):
        self.constraint = constraint
        self.hints = hints

    def numConstraints(self):
        return 1

    def antecedentIDs(self):
        if self.hints is None:
            return "all"
        else:
            return [hint for hint in self.hints if hint != 0]

    def __eq__(self, other):
        return self.constraint == other.constraint

    def isGoal(self):
        return False

    @TimedFunction.time("ReverseUnitPropagation.compute")
    def compute(self, antecedents, context):
        context.propEngine.increaseNumVarsTo(context.ineqFactory.numVars())
        if self.hints is None:
            success = self.constraint.rupCheck(
                context.propEngine, context.only_core_subproof, context.proof.getProofContext(), context.verifierSettings.toAnnotatedRUP)
        else:
            antecedents = list(antecedents)
            hintConstraints = [antecedent[0] for antecedent in antecedents]
            antecedents_idx = 0
            for hint_idx, hint in enumerate(self.hints):
                if hint != 0:
                    if hint < 0:
                        self.hints[hint_idx] = antecedents[antecedents_idx][1]
                    antecedents_idx += 1

            success = self.constraint.rupCheckWithHints(
                context.propEngine, context.only_core_subproof, context.proof.getProofContext(), self.hints, hintConstraints)

        if success:
            return [self.constraint]
        else:
            if context.verifierSettings.traceFailed and self.hints is None:
                with TemporaryAttach(context.propEngine) as temporary:
                    negated = self.constraint.copy().negated()
                    temporary.attach(negated)
                    printRUPTrace(context, context.only_core_subproof)
            raise ReverseUnitPropagationFailed(
                "Failed to show '%s' by reverse unit propagation." % (
                    context.ineqFactory.toString(self.constraint)))

    def justify(self, antecedents, derived, context):
        assert (len(derived) <= 1)

        for constraint, idx in derived:
            if constraint is not None:
                if context.verifierSettings.toAnnotatedRUP:
                    context.proof.print("rup", context.ineqFactory.toString(
                        self.constraint), ";", context.proof.getBuffer())
                else:
                    # consume Id for redundance step
                    redId = context.proof.nextId()
                    context.proof.print("red", context.ineqFactory.toString(
                        self.constraint), "; ; begin")
                    polId = context.proof.nextId()
                    context.proof.print("\tpol", context.proof.getBuffer())
                    context.proof.print("end", polId)

                # Set output constraint ID for the constraint that was just derived.
                derivedId = context.proof.nextId()
                constraint.setOutId(idx, derivedId)


class CompareToConstraint(Rule):
    @classmethod
    def parse(cls, words, context=None):
        # Parse constraint first.
        cppWordIter = words.wordIter.getNative()
        ineq = context.ineqFactory.parse(cppWordIter, allowMultiple=False)

        # Check if optional hint is given
        hint = next(words, None)
        if hint is not None:
            try:
                hint = context.to_constraint_id(hint)
            except:
                raise InvalidProof(
                    "Expected constraint ID as hint, but got instead: " + hint)

        return cls(hint, ineq)

    def __init__(self, constraintId, constraint):
        self.constraintId = constraintId
        self.constraint = constraint
        self.foundConstraintId = None
        self.foundConstraint = None

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        if self.constraintId:
            return [self.constraintId]
        else:
            # If there is no constraint ID given as hint, then we have to go through all the constraints in the database.
            return "all"

    def __eq__(self, other):
        return self.constraintId == other.constraintId \
            and self.constraint == other.constraint

    def isGoal(self):
        return True


@register_rule
class ConstraintEquals(CompareToConstraint):
    Ids = ["e"]

    @TimedFunction.time("ConstraintEquals.compute")
    def compute(self, antecedents, context=None):
        if self.constraintId:
            # Constraint ID given as hint.
            antecedent = list(antecedents)[0]
            if context.only_core_subproof and not antecedent[0].isCoreId(antecedent[1]):
                raise InvalidProof(
                    "Constraint ID %d is not a core constraint ID and in this subproof only core constraints can be used for the equality check." % self.constraintId)
            if self.constraint != antecedent[0]:
                raise EqualityCheckFailed(context.ineqFactory.toString(
                    self.constraint), context.ineqFactory.toString(antecedent[0]))
        else:
            # No constraint ID given as hint.
            self.foundConstraint = context.propEngine.find(
                self.constraint, context.only_core_subproof)
            if not self.foundConstraint:
                raise InvalidProof(
                    "Equality check failed for constraint %s with any constraint in the database." % context.ineqFactory.toString(self.constraint))

        return []


@register_rule
class ConstraintEqualsAndAdd(ConstraintEquals):
    Ids = ["ea"]

    def compute(self, antecedents, context=None):
        super().compute(antecedents, context)
        return [self.constraint]

    def numConstraints(self):
        return 1

    def justify(self, antecedents, derived, context):
        if self.constraintId:
            antecedent, antecedent_id = list(antecedents)[0]
            antecedent_out_id = antecedent.getOutId(antecedent_id)
        else:
            antecedent_out_id = self.foundConstraint.getOutId(
                self.foundConstraint.getSomeId())

        derived_constraint, derived_idx = list(derived)[0]
        context.proof.print("pol", antecedent_out_id)
        derived_constraint.setOutId(derived_idx, context.proof.nextId())


class ImpliesCheckFailed(InvalidProof):
    def __init__(self, expected, got):
        self.expected = expected
        self.got = got


@register_rule
class ConstraintImplies(CompareToConstraint):
    Ids = ["i"]

    def compute(self, antecedents, context=None):
        if self.constraintId:
            # Constraint ID given as hint.
            antecedent = list(antecedents)[0]
            if context.only_core_subproof and not antecedent[0].isCoreId(antecedent[1]):
                raise InvalidProof(
                    "Constraint ID %d is not a core constraint ID and in this subproof only core constraints can be used for the equality check." % self.constraintId)
            if not antecedent[0].implies_strong(self.constraint, context.proof.getProofContext()):
                raise ImpliesCheckFailed(context.ineqFactory.toString(
                    self.constraint), context.ineqFactory.toString(antecedent[0]))
        else:
            # No constraint ID given as hint.
            self.foundConstraint = context.propEngine.find(
                self.constraint, context.only_core_subproof)
            if self.foundConstraint:
                # Constraint exactly found in database
                self.foundConstraintId = self.foundConstraint.getSomeId()
                context.proof.resetBuffer()
            else:
                # Check implication for all constraints in database
                success = False
                if context.only_core_subproof:
                    # Only core constraints can be used.
                    for ineqId, ineq in antecedents:
                        if not ineq.isCoreId(ineqId):
                            continue
                        if ineq.implies_strong(self.constraint, context.proof.getProofContext()):
                            self.foundConstraintId = ineqId
                            self.foundConstraint = ineq
                            success = True
                            break
                else:
                    # All constraints can be used.
                    for ineqId, ineq in antecedents:
                        if ineq.implies_strong(self.constraint, context.proof.getProofContext()):
                            self.foundConstraintId = ineqId
                            self.foundConstraint = ineq
                            success = True
                            break
                if not success:
                    raise InvalidProof(
                        "Implication check failed for constraint %s from any constraint in the database." % context.ineqFactory.toString(self.constraint))

        return []


@register_rule
class ConstraintImpliesGetImplied(ConstraintImplies):
    Ids = ["ia"]

    @TimedFunction.time("ConstraintImpliesGetImplied.compute")
    def compute(self, antecedents, context=None):
        super().compute(antecedents, context)
        return [self.constraint]

    def numConstraints(self):
        return 1

    def justify(self, antecedents, derived, context):
        if self.constraintId:
            antecedent, antecedent_id = list(antecedents)[0]
        else:
            antecedent, antecedent_id = self.foundConstraint, self.foundConstraintId

        derived_constraint, derived_idx = list(derived)[0]
        context.proof.print("pol", antecedent.getOutId(
            antecedent_id), context.proof.getBuffer())
        derived_constraint.setOutId(derived_idx, context.proof.nextId())


class ContradictionCheckFailed(InvalidProof):
    def __str__(self):
        result = "Constraint is not a contradiction. "
        result += super().__str__()
        return result


class SolutionCheckFailed(InvalidProof):
    def __str__(self):
        result = "Provided assignment is contradicting or does not propagate to solution. "
        result += super().__str__()
        return result


def checkSolution(context, assignment, original_formula=None):
    propagated, missingAssignments = context.propEngine.checkSat(assignment)
    # We only test for missing assignments or conflict if the assignment is not tested against the original formula.
    if len(missingAssignments) > 0 and original_formula is None:
        if missingAssignments[0] == 0:
            if context.verifierSettings.traceFailed:
                print("Propagation trace for the conflicting solution:")
                context.propEngine.printSolutionPropagationTrace(assignment,
                                                                 context.ineqFactory.varNameMgr, context.verifierSettings.useColor)
            raise SolutionCheckFailed("(assignment propagates to conflict)")
        elif missingAssignments[0] != 0:
            missing = ", ".join((context.ineqFactory.num2Name(x)
                                for x in missingAssignments))
            error = " (unassigned variables: %s)" % (missing)
            raise SolutionCheckFailed(error)

    if original_formula:
        assignment_object = Assignment(
            propagated, context.ineqFactory.numVars())
        for c in original_formula:
            if not c.isSAT(assignment_object):
                error = "Original constraint %s not satisfied!" % (
                    context.ineqFactory.toString(c))
                raise SolutionCheckFailed(error)

    return propagated


def parse_assignment(context, words):
    def lit2int(name):
        if name[0] == "~":
            return -context.ineqFactory.name2Num(name[1:])
        else:
            return context.ineqFactory.name2Num(name)

    result = list(map(lit2int, words))

    context.propEngine.increaseNumVarsTo(context.ineqFactory.numVars())
    # Should we produce an error or at least a warning if a variable is defined that was not used before?
    # if nVars < context.ineqFactory.numVars():
    #     raise InvalidProof("Variable " + context.ineqFactory.num2Name(nVars + 1) + " not defined.")
    return result


@register_rule
class Solution(Rule):
    Ids = ["v", "solx"]

    @classmethod
    def parse(cls, line, context):
        with MaybeWordParser(line) as words:
            result = parse_assignment(context, words)

        return cls(result)

    def __init__(self, partialAssignment):
        self.partialAssignment = partialAssignment

    @TimedFunction.time("Solution.compute")
    def compute(self, antecedents, context):
        self.assignment = checkSolution(context,
                                        self.partialAssignment)
        if context.deletion_checked:
            context.recorded_solution = True

        return [context.ineqFactory.fromTerms([Term(1, -lit) for lit in self.assignment], 1)]

    def numConstraints(self):
        return 1

    def antecedentIDs(self):
        return []

    def isGoal(self):
        return True

    def deleteConstraints(self):
        return []

    def isAddToCore(self):
        return True

    def justify(self, antecedents, derived, context):
        context.proof.print("solx", " ".join((context.ineqFactory.int2lit(x)
                                              for x in self.assignment)))
        for constraint, idx in derived:
            constraint.setOutId(idx, context.proof.nextId())


@register_rule
class Solution(Rule):
    Ids = ["sol"]

    @classmethod
    def parse(cls, line, context):
        with MaybeWordParser(line) as words:
            result = parse_assignment(context, words)

        return cls(result)

    def __init__(self, partialAssignment):
        self.partialAssignment = partialAssignment

    @TimedFunction.time("Solution.compute")
    def compute(self, antecedents, context):
        self.assignment = checkSolution(context,
                                        self.partialAssignment)
        if context.deletion_checked:
            context.recorded_solution = True

            if context.objective is not None:
                objValue = calculateObjectiveValue(self.assignment, context)

                if context.best_objective_value == None:
                    context.best_objective_value = objValue
                else:
                    if context.best_objective_value > objValue:
                        context.best_objective_value = objValue

        return []

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        return []

    def isGoal(self):
        return True

    def deleteConstraints(self):
        return []

    def justify(self, antecedents, derived, context):
        context.proof.print("sol", " ".join((context.ineqFactory.int2lit(x)
                                             for x in self.assignment)))


@register_rule
class OriginalSolution(Rule):
    Ids = ["ov"]

    @classmethod
    def parse(cls, line, context):
        if context.major == 2:
            raise InvalidProof(
                "The rule 'ov' is not available in version 2 of the proof format and onwards.")
        with MaybeWordParser(line) as words:
            result = parse_assignment(context, words)

        return cls(Assignment(result))

    def __init__(self, assignment):
        self.assignment = assignment

    @TimedFunction.time("Solution.compute")
    def compute(self, antecedents, context):
        for c in context.formula:
            if not c.isSAT(self.assignment):
                error = "Constraint %s not satisfied!" % (
                    context.ineqFactory.toString(c))
                raise SolutionCheckFailed(error)

        return []

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        return []

    def isGoal(self):
        return True

    def deleteConstraints(self):
        return []


class ObjectiveNotFullyAssigned(InvalidProof):
    def __str__(self):
        result = "Provided assignment does not propagate to assignment of all variables in the objective."
        result += super().__str__()
        return result


@register_rule
class ObjectiveBound(Rule):
    Ids = ["o", "soli"]

    @classmethod
    def parse(cls, line, context):
        if context.objective is None:
            raise InvalidProof(
                "The rule 'soli' can only be used for optimization problems, but there was no objective specified in formula file.")
        with MaybeWordParser(line) as words:
            result = parse_assignment(context, words)

        return cls(result)

    def __init__(self, partialAssignment):
        self.partialAssignment = partialAssignment

    @TimedFunction.time("ObjectiveBound.compute")
    def compute(self, antecedents, context):
        self.assignment = checkSolution(context,
                                        self.partialAssignment)
        objValue = calculateObjectiveValue(self.assignment, context)

        if context.deletion_checked:
            context.recorded_solution = True

            if context.best_objective_value == None:
                context.best_objective_value = objValue
            else:
                if context.best_objective_value > objValue:
                    context.best_objective_value = objValue

        # obj <= objvalue - 1
        lowerBound = context.ineqFactory.fromTerms(
            [Term(-coeff, lit) for lit, coeff in context.objective.items()], -(objValue - 1 - context.objectiveConstant))
        return [lowerBound]

    def numConstraints(self):
        return 1

    def antecedentIDs(self):
        return []

    def isGoal(self):
        return True

    def deleteConstraints(self):
        return []

    def isAddToCore(self):
        return True

    def justify(self, antecedents, derived, context):
        context.proof.print("soli", " ".join((context.ineqFactory.int2lit(x)
                                              for x in self.assignment)))
        for constraint, idx in derived:
            constraint.setOutId(idx, context.proof.nextId())


@register_rule
class IsContradiction(Rule):
    Ids = ["c"]

    @classmethod
    def parse(cls, line, context):
        if context.major == 2:
            raise InvalidProof(
                "The proof rule 'c' is not supported in version 2 and onwards. Use 'end <ID>' to end subproof or corresponding 'conclusion' to end proof.")

        with MaybeWordParser(line) as words:
            which = list(map(int, words))

            if (which[-1] == 0):
                which = which[:-1]

            if len(which) != 1:
                raise ValueError("Expected exactly one constraintId.")

        return cls(which[0])

    def __init__(self, constraintId):
        self.constraintId = constraintId

    @TimedFunction.time("IsContradiction.compute")
    def compute(self, antecedents, context=None):
        antecedent = list(antecedents)[0]
        if not antecedent[0].isContradiction():
            raise ContradictionCheckFailed()
        else:
            context.containsContradiction = True
            if context.proof:
                context.proof.print("c", antecedent[0].getOutId(antecedent[1]))

        return []

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        return [self.constraintId]

    def __eq__(self, other):
        return self.constraintId == other.constraintId

    def isGoal(self):
        return True


NONE = "NONE"
UNSAT = "UNSAT"
SAT = "SAT"
BOUNDS = "BOUNDS"
INF = "INF"


def checkUpperBound(upper_bound, assignment, context):
    if assignment is not None:
        assignment = checkSolution(context,
                                   assignment, context.formula)
        objValue = calculateObjectiveValue(
            assignment, context, True)
        if objValue != upper_bound:
            raise InvalidProof(
                "Upper bound does not match objective value of given assignment.")
    else:
        if context.recorded_solution:
            if context.best_objective_value != upper_bound:
                raise InvalidProof(
                    "Upper bound does not match best recorded solution.")
        else:
            raise InvalidProof(
                "Claimed upper bound better than 'INF' but no solution recorded or given as hint.")


def checkContradiction(constraint, constraint_id, context):
    if constraint:
        if not constraint.isContradiction():
            raise ContradictionCheckFailed()
        else:
            context.containsContradiction = True
        return constraint.getOutId(constraint_id)
    else:
        contradiction_constraint = context.ineqFactory.fromTerms([], 1)
        # 1. Check if contradiction exists in exactly the right form in the DB.
        found_constraint = context.propEngine.find(
            contradiction_constraint, False)
        if found_constraint is None:
            # 2. Check if any constraint in the DB implies contradiction.
            found_constraint = context.propEngine.checkConstraintImpliedByDB(
                contradiction_constraint, False)
            if found_constraint is None:
                raise ContradictionCheckFailed()

        # At this point we know that we found a constraint that implies contradiction.
        context.containsContradiction = True
        return found_constraint.getOutId(found_constraint.getSomeId())


def checkLowerBound(lower_bound, hint, hint_id, context):
    lower_bound_constraint = context.ineqFactory.fromTerms(
        [Term(coeff, lit) for lit, coeff in context.objective.items()], lower_bound - context.objectiveConstant)
    if hint:
        implication_checked = hint.implies(lower_bound_constraint)
        if not implication_checked:
            raise InvalidProof(
                "Constraint ID does not imply lower bound for conclusion BOUNDS.")
        return hint.getOutId(hint_id)

    else:
        # 1. Check if lower bound constraint exists in exactly the right form in the DB.
        found_constraint = context.propEngine.find(
            lower_bound_constraint, False)
        if found_constraint is None:
            # 2. Check if any constraint in the DB implies the lower bound constrain.
            found_constraint = context.propEngine.checkConstraintImpliedByDB(
                lower_bound_constraint, False)
            if found_constraint is None:
                raise InvalidProof(
                    "Lower bound constraint is not implied by any constraint in the database. If the lower bound follows by RUP, you have to add a RUP line to the proof that derives the lower bounding constraint.")

        # At this point we know that we found a constraint that implies contradiction.
        return found_constraint.getOutId(found_constraint.getSomeId())


def calculateObjectiveValue(assignment, context, use_original_objective=False):
    numFoundValues = 0
    if use_original_objective:
        objValue = context.originalObjectiveConstant
        objective_terms = context.originalObjectiveTerms
    else:
        objValue = context.objectiveConstant
        objective_terms = context.objective

    for lit in assignment:
        if lit in objective_terms:
            numFoundValues += 1
            objValue += objective_terms[lit]
        if -lit in objective_terms:
            numFoundValues += 1

    if len(objective_terms) != numFoundValues:
        raise ObjectiveNotFullyAssigned()

    return objValue


@register_rule
class ConclusionRule(Rule):
    Ids = ["conclusion"]

    @classmethod
    def parse(cls, line, context):
        if context.has_conclusion:
            raise InvalidProof(
                "Multiple conclusions detected. 'conclusion' is only allowed once.")
        if not context.has_output:
            raise InvalidProof(
                "The proof must contain 'output' before 'conclusion'. If there is no output, use 'output NONE'.")
        context.has_conclusion = True

        with MaybeWordParser(line) as words:
            conclusion_type = next(words, None)

            if conclusion_type == None:
                raise ValueError("No conclusion type given.")

            constraint_id = None
            assignment = None
            lower_bound = None
            upper_bound = None

            if conclusion_type == NONE:
                pass

            elif conclusion_type == UNSAT:
                if context.objective is not None:
                    raise InvalidProof(
                        "The conclusion 'UNSAT' cannot be used for optimization problems (i.e. formula file contains objective).")
                separator = next(words, None)
                if separator == ":":
                    constraint_id = next(words, None)
                    if constraint_id == None:
                        raise ValueError(
                            "Expected one constraint ID.")
                elif separator == None:
                    pass
                else:
                    raise ValueError(
                        "Expected either separator ':' between conclusion type UNSAT and constraint ID or no further parameter.")

            elif conclusion_type == SAT:
                if context.objective is not None:
                    raise InvalidProof(
                        "The conclusion 'SAT' cannot be used for optimization problems (i.e. formula file contains objective).")
                separator = next(words, None)
                if separator == ":":
                    assignment = parse_assignment(context, words)
                else:
                    if separator == None:
                        if context.recorded_solution == False:
                            raise ValueError(
                                "No solution supporting the conclusion SAT has been recorded.")
                    else:
                        raise ValueError(
                            "Expected separator ':' between conclusion type SAT and assignment.")

            elif conclusion_type == BOUNDS:
                if context.objective is None:
                    raise InvalidProof(
                        "The conclusion 'BOUNDS' cannot be used for optimization problems (i.e. formula file contains objective).")
                lower_bound = next(words, None)
                if lower_bound == None:
                    raise ValueError(
                        "Expected lower bound as first parameter of conclusion BOUNDS.")
                separator = next(words, None)
                if separator == None:
                    raise ValueError(
                        "Expected separator ':' or upper bound.")
                if separator == ":":
                    constraint_id = next(words)
                    if constraint_id == None:
                        raise ValueError(
                            "Expected constraint ID that establishes lower bound.")
                    upper_bound = next(words, None)
                else:
                    upper_bound = separator
                if upper_bound == None:
                    raise ValueError(
                        "Expected upper bound as parameter of conclusion BOUNDS.")
                if upper_bound != "INF":
                    separator = next(words, None)
                    if separator == ":":
                        assignment = parse_assignment(context, words)
                else:
                    separator = next(words, None)
                    if separator != None:
                        raise InvalidProof(
                            "Upper bound 'INF' and solution specified at the same time. If there is a solution, then upper bound cannot be 'INF'")

            else:
                raise ValueError(
                    "Conclusion type {} is not supported.".format(conclusion_type))

        return cls(conclusion_type, constraint_id, assignment, lower_bound, upper_bound, context)

    def __init__(self, conclusion, constraintId, assignment, lower_bound, upper_bound, context):
        if constraintId:
            self.constraintId = context.to_constraint_id(constraintId)
        else:
            self.constraintId = constraintId

        if lower_bound and lower_bound != INF:
            self.lower_bound = int(lower_bound)
        else:
            self.lower_bound = lower_bound

        if upper_bound and upper_bound != INF:
            self.upper_bound = int(upper_bound)
        else:
            self.upper_bound = upper_bound

        self.assignment = assignment
        self.conclusion = conclusion
        self.hint_out_id = None

    # @TimedFunction.time("ConclusionRule.compute")
    def compute(self, antecedents, context=None):
        antecedents = list(antecedents)
        if len(antecedents) == 1:
            antecedent = antecedents[0][0]
            antecedent_id = antecedents[0][1]
        else:
            antecedent = None
            antecedent_id = None

        if self.conclusion == UNSAT:
            self.hint_out_id = checkContradiction(
                antecedent, antecedent_id, context)
            # Print conclusion verified statement.
            print("s VERIFIED UNSATISFIABLE")

        elif self.conclusion == SAT:
            if self.assignment is not None:
                self.assignment = checkSolution(context,
                                                self.assignment, context.formula)
            else:
                if context.recorded_solution == False:
                    raise InvalidProof(
                        "Conclusion invalid! Conclusion 'SAT' can only be used if there has been solution logged before or a solution is given as a hint using 'conclusion SAT : <assignment>'.")
            # Print conclusion verified statement.
            print("s VERIFIED SATISFIABLE")

        elif self.conclusion == BOUNDS:
            # Check upper bound
            if self.upper_bound == INF:
                if context.recorded_solution:
                    raise InvalidProof(
                        "Upper bound is not 'INF', as at least one solution has been recorded.")
            else:
                checkUpperBound(self.upper_bound, self.assignment, context)

            # Check lower bound
            if self.lower_bound == INF:
                # Sanity check that upper bound cannot be claimed for infeasible problem
                if self.upper_bound != INF:
                    raise InvalidProof(
                        "The lower bound claims that the problem is infeasible, but the upper bound claims that there is a solution with objective value {}, which is impossible.".format(self.upper_bound))
                self.hint_out_id = checkContradiction(
                    antecedent, antecedent_id, context)
            else:
                # Sanity check that lower bound <= upper bound
                if self.upper_bound != INF and self.lower_bound > self.upper_bound:
                    raise InvalidProof(
                        "Claimed lower bound is bigger than claimed upper bound.")
                # Sanity check that lower bound <= best objective value
                if self.upper_bound != INF and context.recorded_solution and self.lower_bound > context.best_objective_value:
                    raise InvalidProof(
                        "Claimed lower bound is bigger than best objective value for all recorded solutions.")
                self.hint_out_id = checkLowerBound(
                    self.lower_bound, antecedent, antecedent_id, context)
            # Print conclusion verified statement.
            print("s VERIFIED BOUNDS", self.lower_bound,
                  "<= obj <=", self.upper_bound)
        elif self.conclusion == NONE:
            # Print conclusion verified statement.
            print("s VERIFIED NO CONCLUSION")

        return []

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        if self.constraintId:
            return [self.constraintId]
        else:
            return []

    def __eq__(self, other):
        return self.constraintId == other.constraintId

    def isGoal(self):
        return True

    def justify(self, antecedents, derived, context):
        antecedents = list(antecedents)
        if len(antecedents) == 1:
            antecedent = antecedents[0]
        else:
            antecedent = None
        if self.conclusion == UNSAT:
            context.proof.print("conclusion UNSAT :", self.hint_out_id)

        elif self.conclusion == NONE:
            context.proof.print("conclusion NONE")

        elif self.conclusion == SAT:
            context.proof.print("conclusion SAT", end=" ")
            if context.recorded_solution == False:
                context.proof.print(":", " ".join(
                    (context.ineqFactory.int2lit(x) for x in self.assignment)))
            else:
                context.proof.print()

        elif self.conclusion == BOUNDS:
            context.proof.print("conclusion BOUNDS", self.lower_bound,
                                ":", self.hint_out_id, self.upper_bound, end=" ")
            if self.upper_bound != INF and self.assignment is not None:
                context.proof.print(":", " ".join((context.ineqFactory.int2lit(x)
                                                   for x in self.assignment)))
            else:
                context.proof.print()

        else:
            # If the conclusion is something not known then return undefined
            context.proof.print("conclusion UNDEFINED")


@register_rule
class EndProofRule(Rule):
    Ids = ["end"]

    @classmethod
    def parse(cls, line, context):
        if context.is_finished:
            raise InvalidProof(
                "Multiple proof ends detected. 'end pseudo-Boolean proof' is only allowed once.")
        if not context.has_output or not context.has_conclusion:
            raise InvalidProof(
                "The proof must contain 'output' and 'conclusion' before 'end pseudo-Boolean proof'. If there is no output, use 'output NONE', and if there is no conclusion, use 'conclusion NONE'.")
        context.is_finished = True

        with MaybeWordParser(line) as words:
            which = list(map(str, words))

            if len(which) != 2 or which[0] != "pseudo-Boolean" or which[1] != "proof":
                raise ValueError(
                    "Incorrect end of proof. End of proof should be 'end pseudo-Boolean proof'.")

        return cls()

    @TimedFunction.time("ConclusionRule.compute")
    def compute(self, antecedents, context):
        return []

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        return []

    def isGoal(self):
        return True

    def justify(self, antecedents, derived, context):
        context.proof.print("end pseudo-Boolean proof")


@register_rule
class ReversePolishNotation(Rule):
    Ids = ["p", "pol"]

    @classmethod
    def parse(cls, line, context):
        stackSize = 0

        def f(word):
            nonlocal stackSize

            if word in ["+", "*", "d", "w"]:
                stackSize -= 1
            elif word == "r":
                stackSize -= 2
            elif word in ["s", ";"]:
                stackSize += 0
            else:
                if context.ineqFactory.isLit(word):
                    lit = context.ineqFactory.lit2int(word)
                    word = ("l", lit)
                    stackSize += 1
                else:
                    try:
                        word = context.to_constraint_id(word)
                        stackSize += 1
                    except ValueError:
                        raise ValueError(
                            "Expected integer, literal or one of +, *, d, s, r.")
                    # else:
                    #     if word == 0:
                    #         raise ValueError("Got 0, which should only be used to terminate sequence.")

            if stackSize <= 0:
                raise ValueError(
                    "Trying to pop from empty stack in reverse polish notation.")

            return word

        with MaybeWordParser(line) as words:
            sequence = list(map(f, words))

            if sequence and sequence[-1] == 0:
                sequence.pop()
                stackSize -= 1
            if sequence and sequence[-1] == ";":
                sequence.pop()

            if stackSize != 1:
                raise ValueError(
                    "Stack should contain exactly one element at end of polish notation, but the stack contained {} elements.".format(stackSize))
        return cls(sequence)

    class AntecedentIterator():
        """
        Iterator that only returns the contained constraint numbers
        (antecedents) and skips everything else.
        """

        def __init__(self, instructions):
            self.instructions = iter(instructions)

        def __iter__(self):
            return self

        def __next__(self):
            while (True):
                current = next(self.instructions)
                if isinstance(current, int):
                    return current
                if current in ["*", "d", "w"]:
                    # consume one more, remember that we swaped the right operand and operator
                    next(self.instructions)

    def __init__(self, instructions):
        for i, x in enumerate(instructions):
            # the last operand of multiplication and division always
            # needs to be a constant and not a constraint, so we (can) switch
            # positions, which makes it easier to distinguish constraints from
            # constants later on
            if x in ["*", "d", "w"]:
                instructions[i] = instructions[i - 1]
                instructions[i - 1] = x

        self.instructions = instructions

    @TimedFunction.time("ReversePolishNotation.compute")
    def compute(self, antecedents, context=None):
        antecedents = list(antecedents)
        stack = list()
        antecedentIt = iter(antecedents)

        it = iter(self.instructions)
        ins = next(it, None)
        while ins is not None:
            if isinstance(ins, int):
                antecedent = next(antecedentIt)
                if context.only_core_subproof and not antecedent[0].isCoreId(antecedent[1]):
                    raise InvalidProof(
                        "Reverse polish notation proof is using derived constraint ID %d in a only core constraints subproof." % antecedent[1])
                stack.append(antecedent[0].copy())
            if isinstance(ins, tuple):
                what = ins[0]
                if what == "l":
                    lit = ins[1]
                    stack.append(context.ineqFactory.litAxiom(lit))
                else:
                    assert (False)
            elif ins == "+":
                second = stack.pop()
                first = stack.pop()
                stack.append(first.add(second))
            elif ins == "*":
                constraint = stack.pop()
                factor = next(it)
                if factor < 0:
                    raise InvalidProof("Multiplication by negative number.")
                stack.append(constraint.multiply(factor))
            elif ins == "d":
                constraint = stack.pop()
                divisor = next(it)
                if divisor <= 0:
                    raise InvalidProof("Division by non positive number.")
                stack.append(constraint.divide(divisor))
            elif ins == "s":
                constraint = stack.pop()
                stack.append(constraint.saturate())
            elif ins == "w":
                nxt = next(it, None)
                if not isinstance(nxt, tuple) or nxt[0] != "l":
                    raise InvalidProof(
                        "Expected literal before 'w' instruction, got {}".format(nxt))
                lit = nxt[1]
                if lit < 0:
                    logging.warn("Weakening step ignores sign of literals.")
                    lit = abs(lit)
                constraint = stack.pop()
                stack.append(constraint.weaken(lit))

            ins = next(it, None)

        assert len(stack) == 1
        stack[0].contract()
        return stack

    def justify(self, antecedents, derived, context):
        assert (len(derived) <= 1)
        for c, idx in derived:
            if c:
                c.setOutId(idx, context.proof.nextId())

                antecedentIt = iter(antecedents)

                it = iter(self.instructions)
                ins = next(it, None)
                print("pol", end=" ", file=context.proof.file)
                while ins is not None:
                    if isinstance(ins, int):
                        antecedent = next(antecedentIt)
                        print(antecedent[0].getOutId(antecedent[1]),
                              end=" ", file=context.proof.file)
                    if isinstance(ins, tuple):
                        what = ins[0]
                        if what == "l":
                            lit = ins[1]
                            print(context.ineqFactory.int2lit(lit),
                                  end=" ", file=context.proof.file)
                        else:
                            assert (False)
                    elif ins == "+":
                        print("+", end=" ", file=context.proof.file)
                    elif ins == "*":
                        factor = next(it)
                        print(factor, "*", end=" ", file=context.proof.file)
                    elif ins == "d":
                        divisor = next(it)
                        print(divisor, "d", end=" ", file=context.proof.file)
                    elif ins == "s":
                        print("s", end=" ", file=context.proof.file)
                    elif ins == "w":
                        nxt = next(it, None)
                        assert (nxt[0] == "l")
                        lit = nxt[1]
                        print(context.ineqFactory.int2lit(abs(lit)),
                              "w", end=" ", file=context.proof.file)

                    ins = next(it, None)

                print("\n", end="", file=context.proof.file)

    def numConstraints(self):
        return 1

    def antecedentIDs(self):
        return ReversePolishNotation.AntecedentIterator(self.instructions)


@register_rule
class LoadFormula(Rule):
    Ids = ["f"]

    @classmethod
    def parse(cls, line, context):
        if getattr(context, "canLoadFormula", True) == False:
            raise InvalidProof("You are not allowed to load the formula"
                               "after using redundancy checks")
        numConstraints = len(context.formula)
        with MaybeWordParser(line) as words:
            try:
                num = int(next(words))
            except StopIteration:
                num = 0
            if num == 0:
                if context.major == 1:
                    return cls(numConstraints)
                else:
                    return cls(0)
            else:
                if num != numConstraints:
                    raise InvalidProof(
                        "Number of constraints in the formula does not match, got %i but "
                        "there are %i constraints in the formula. This rule does not consider constraint derived in the proof." % (num, numConstraints))

                if context.major == 1:
                    return cls(numConstraints)
                else:
                    return cls(0)

    def __init__(self, numConstraints):
        self._numConstraints = numConstraints

    @TimedFunction.time("LoadFormula.compute")
    def compute(self, antecedents, context=None):
        if context.major == 1:
            return context.formula
        else:
            return []

    def numConstraints(self):
        return self._numConstraints

    def antecedentIDs(self):
        return []

    def justify(self, antecedents, derived, context):
        pass

    def isFormula(self):
        return True


@register_rule
class LoadAxiom(Rule):
    Ids = ["l"]

    @classmethod
    def parse(cls, line, context):
        if getattr(context, "canLoadFormula", True) == False:
            raise ValueError("You are not allowed to load the formula"
                             "after using redundancy checks")

        if context.major == 2:
            raise InvalidProof(
                "Rule 'l' is no longer supported in proof format version 2 and onwards.")

        with MaybeWordParser(line) as words:
            num = words.nextInt()
            words.expectEnd()
            return cls(num)

    def __init__(self, axiomId):
        self._axiomId = axiomId

    @TimedFunction.time("LoadAxiom.compute")
    def compute(self, antecedents, context):
        try:
            return [context.formula[self._axiomId - 1]]
        except IndexError as e:
            raise InvalidProof("Trying to load non existing axiom.")

    def numConstraints(self):
        return 1

    def antecedentIDs(self):
        return []

    def justify(self, antecedents, constraints, context):
        # The proof will load the formula at the beginning once so
        # every constraint loaded as axiom should already have an out
        # id set and hence should not need justification.

        # maybe we should disallow the l rule
        pass


@register_rule
class MarkCore(EmptyRule):
    Ids = ["core"]

    @classmethod
    def parse(cls, words, context):
        ids, identification_type = idOrFind(words, context)

        if identification_type == "find":
            raise InvalidProof(
                "Can't move to core a constraint identified by specification ('core find' or 'core spec'), as constraintID is ambiguous and moving all constraints with this specification might break correct deletion.")

        return cls(ids)

    def __init__(self, toMark):
        self.which = toMark

    def antecedentIDs(self):
        return self.which

    def compute(self, antecedents, context):
        for ineq, id in antecedents:
            context.propEngine.moveToCore(ineq, id)
        return []

    def justify(self, antecedents, derived, context):
        context.proof.print("core id", " ".join(
            [str(constraint.getOutId(idx)) for constraint, idx in antecedents]))

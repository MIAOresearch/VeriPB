from enum import Enum
import logging

from veripb import InvalidProof, ParseError
from veripb.rules_register import register_rule
from veripb.parser import MaybeWordParser, loadFormula
from veripb.rules import Rule
from veripb.timed_function import TimedFunction
from veripb.rules_objective import checkObjective


from veripb.optimized.parsing import parseOpb, parseCnf, parseWcnf

DERIVABLE = "DERIVABLE"
EQUISATISFIABLE = "EQUISATISFIABLE"
EQUIOPTIMAL = "EQUIOPTIMAL"
EQUIENUMERABLE = "EQUIENUMERABLE"
NONE = "NONE"
POSSIBLE_GUARANTEES = set([DERIVABLE, EQUISATISFIABLE,
                          EQUIOPTIMAL, EQUIENUMERABLE, NONE])
CONSTRAINTS = "CONSTRAINTS"
PERMUTATION = "PERMUTATION"
IMPLICIT = "IMPLICIT"
FILE = "FILE"
POSSIBLE_LIST_TYPES = set([CONSTRAINTS, PERMUTATION, IMPLICIT, FILE])


def checkFileOutput(context):
    if context.outputFormulaFile is None:
        raise InvalidProof(
            "Proof is using output type 'FILE', but no file has been given as command line argument (see 'veripb --help').")

    # Parse output formula file
    if context.outputCNF:
        parser = parseCnf
    elif context.outputWCNF:
        parser = parseWcnf
    else:
        parser = parseOpb

    try:
        outputFormula = loadFormula(context.outputFormulaFile.name, parser,
                                    context.ineqFactory.varNameMgr)
    except ParseError as e:
        e.fileName = context.outputFormulaFile.name
        raise e

    # Check objective function. Test if both formulas agree on having an objective.
    checkObjective(
        context, outputFormula["objective"], outputFormula["objectiveConstant"])

    failedConstraint, foundInCore = context.propEngine.checkOutputConstraints(
        outputFormula["constraints"])

    if failedConstraint is not None:
        if foundInCore:
            raise InvalidProof("The following constraint is in the core but not in the output: " +
                               context.ineqFactory.toString(failedConstraint))
        else:
            raise InvalidProof("The following constraint is in the output but not in the core: " +
                               context.ineqFactory.toString(failedConstraint))


@register_rule
class OutputRule(Rule):
    Ids = ["output"]

    @classmethod
    def parse(cls, line, context):
        if context.has_output:
            raise InvalidProof(
                "Multiple outputs detected. 'output' is only allowed once.")
        context.has_output = True

        with MaybeWordParser(line) as words:
            which = list(map(str, words))

            if which[-1] == 0:
                which = which[:-1]

            # Check for special case output `NONE`.
            if which[0] == NONE:
                if len(which) != 1:
                    raise ValueError(
                        "No further parameter required for output guarantee 'NONE'.")
                return cls(which[0])

            if which[0] not in POSSIBLE_GUARANTEES:
                raise ValueError(
                    "The output guarantee '{}' does not exist.".format(which[0]))

            # All other output guarantees require an output type.
            if len(which) != 2:
                raise ValueError(
                    "Expected a second parameter when the output guarantee is something else than 'NONE'.")
            if which[1] not in POSSIBLE_LIST_TYPES:
                raise ValueError(
                    "The output type '{}' does not exist.".format(which[1]))

            return cls(which[0], which[1])

    def __init__(self, guarantee, output_type=None):
        self.guarantee = guarantee
        self.output_type = output_type

    @TimedFunction.time("OutputRule.compute")
    def compute(self, antecedents, context=None):
        # 1. Check if guarantee is met.
        # Check for the correct problem type.
        if self.guarantee == EQUIOPTIMAL:
            if context.objective is None:
                raise InvalidProof(
                    "The output guarantee 'EQUIOPTIMAL' cannot be used for decision problems (i.e. formula file without objective). For decision problems the guarantee 'EQUISATISFIABLE' should be used instead.")
            context.output_objective_checked = False
        if self.guarantee == EQUISATISFIABLE:
            if context.objective is not None:
                raise InvalidProof(
                    "The output guarantee 'EQUISATISFIABLE' cannot be used for optimization problems (i.e. formula file contains objective). For optimization problems the guarantee 'EQUIOPTIMAL' should be used instead.")

        # Check for check enabled deletion.
        if self.guarantee == EQUIOPTIMAL or self.guarantee == EQUISATISFIABLE or self.guarantee == EQUIENUMERABLE:
            if not context.deletion_checked:
                raise InvalidProof(
                    "Deletions were unchecked. Output guarantee '{}' can only be used if deletions are checked. Use the option '--forceCheckDeletion' to fail the proof checking as soon as unchecked deletion is used.".format(self.guarantee))

        if self.guarantee == EQUIENUMERABLE:
            raise NotImplementedError(
                "Output guarantee 'EQUIENUMERABLE' has not been implemented so far.")

        # 2. Check output formula, depending on the type.
        if self.output_type is None:
            return []
        elif self.output_type == FILE:
            checkFileOutput(context)
        elif self.output_type == IMPLICIT:
            if context.outputFormulaFile is not None:
                logging.warning(
                    "Output type 'IMPLICIT' does not check against provided output problem file.")
        else:
            raise NotImplementedError(
                "Only output type 'FILE' and 'IMPLICIT'  has been implemented so far.")

        # Print verification statement.
        if self.guarantee == EQUIOPTIMAL:
            print("s VERIFIED OUTPUT EQUIOPTIMAL FOR obj <",
                  context.best_objective_value)
        elif self.guarantee == EQUISATISFIABLE:
            print("s VERIFIED OUTPUT EQUISATISFIABLE")

        return []

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        if self.output_type is None:
            return []

        if self.output_type == FILE:
            return "core"

    def justify(self, antecedents, derived, context):
        # This is moved to conclusion
        if self.output_type is None:
            context.proof.print("output", self.guarantee)
        else:
            context.proof.print("output", self.guarantee, self.output_type)

    def newParseContext(self, parseContext):
        if self.output_type != FILE:
            parseContext.parsing_output = True
        return parseContext


class LineType(Enum):
    OBJECTIVE = 0
    INEQ = 1


class CheckOutputRule(Rule):
    Ids = []

    @classmethod
    def parse(cls, words, context):
        label = next(words)
        if label == "min:":
            cppWordIter = words.wordIter.getNative()
            objective_dict, objective_constant = context.ineqFactory.parseObjective(
                cppWordIter)
            return cls(LineType.OBJECTIVE, objective_dict=objective_dict,
                       objective_constant=objective_constant)
        else:
            words.undo()
            cppWordIter = words.wordIter.getNative()
            ineq = context.ineqFactory.parse(cppWordIter, allowMultiple=False)
            return cls(LineType.INEQ, ineq=ineq)

    def __init__(self, line_type, objective_dict=None, objective_constant=None, ineq=None):
        self.line_type = line_type
        self.objective_dict = objective_dict
        self.objective_constant = objective_constant
        self.ineq = ineq

    def compute(self, antecedents, context=None):
        if self.line_type == LineType.OBJECTIVE:
            if context.objective is None:
                raise InvalidProof(
                    "Output check: Original problem did not contain objective and output problem contains objective.")
            if len(self.objective_dict) != len(context.objective):
                raise InvalidProof(
                    "Current objective and objective in output does not match up.")

            context.output_objective_checked = True
        return []

    def numConstraints(self):
        return 0

    def antecedentIDs(self):
        return []
